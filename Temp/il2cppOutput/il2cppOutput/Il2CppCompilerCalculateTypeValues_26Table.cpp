﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<System.Boolean>
struct Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD;
// System.Action`1<Vuforia.VuMarkBehaviour>
struct Action_1_t8E59E4A442B30DDF0D8C60EC54C9F8CBCE045E91;
// System.Action`1<Vuforia.VuMarkTarget>
struct Action_1_t54E72A24489A68155FB05D0D7258B36444955310;
// System.Action`1<Vuforia.VuforiaBehaviour>
struct Action_1_t2C77A04A3D5781CE2D25ADCA64743C98A7E534F9;
// System.Action`1<Vuforia.VuforiaUnity/InitError>
struct Action_1_tAA7EF8692AA276A9B2E6353FC26F374339933EFF;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<Vuforia.VuMarkBehaviour>>
struct Dictionary_2_t028B7061DF6BB16493BDAD455EDBA593439778FE;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>
struct Dictionary_2_t7A03A041C3064135DC6552629B8B07C88FC4A0CD;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>
struct Dictionary_2_tA086E5DF05B5E12AB5BD479A0A0566E77144E53F;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>
struct Dictionary_2_t8226BA474A6287F4B7854D0BB11D209617E6ECD6;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButtonBehaviour>
struct Dictionary_2_t49D49E0D72539C160B6F736C64DBC729238B3481;
// System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t46FB20A88A3801A02559059E86A26B5B78D0A86B;
// System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>
struct Dictionary_2_t44E234598935A034894846D9A90E3242F2F2D6D7;
// System.Collections.Generic.Dictionary`2<System.Type,Vuforia.TargetFinder>
struct Dictionary_2_t97145FAA8A62E3A6895B2E7A97B7BB62B2F6B972;
// System.Collections.Generic.Dictionary`2<Vuforia.PIXEL_FORMAT,Vuforia.UnmanagedImage>
struct Dictionary_2_tD36561317F15A4463B66EFAA17CF49A315A67587;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_tC8214FEC830040D37F12A482FF0284D9C2B9001E;
// System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager/TrackableIdPair>
struct LinkedList_1_tE86603FF5BEA21BD62849C673324AEE74A31E58A;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<Vuforia.DataSet>
struct List_1_t03C9CEF50E5A7C36B2BC4CD68C627FA6080A1BF9;
// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
struct List_1_tE4338C7F7D33C78CB75B44EB5CCCA0152E97497B;
// System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>
struct List_1_t1314A3DDAE2D8AD81F9403BD961C3CC0530B04A9;
// System.Collections.Generic.List`1<Vuforia.TrackableBehaviour>
struct List_1_tE21EDDA7B9D124D56D393C74E3F66347A01B8147;
// System.Collections.Generic.List`1<Vuforia.VuMarkBehaviour>
struct List_1_tC8B88347864264398BE3608B6662A0EFEB5E8450;
// System.Collections.Generic.List`1<Vuforia.VuMarkTarget>
struct List_1_tBAEACD03FC5EAAC0FEDAC89712307241E318AA69;
// System.Func`1<System.Boolean>
struct Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t3AD4B0F443BFD399C4AC2D6EE99FFE3BC0970017;
// System.Func`2<Vuforia.TrackableBehaviour,System.Boolean>
struct Func_2_tB1EDC94D5DE2903BAA30A4C2DF00098781945AA6;
// System.Func`2<Vuforia.TrackerData/TrackableResultData,System.Int32>
struct Func_2_t9505439C411FE019A9092CD2D73D7AE231ED1C15;
// System.Func`2<Vuforia.TrackerData/TrackableResultData,Vuforia.TrackableBehaviour/Status>
struct Func_2_t1AF4389885EDCAF20EE4E6FBD0CD50A435897369;
// System.Func`2<Vuforia.TrackerData/TrackableResultData,Vuforia.TrackableBehaviour/StatusInfo>
struct Func_2_tFA00CC82AB530CBB3AE283838E61F2A92D8AADEA;
// System.Func`2<Vuforia.TrackerData/TrackableResultData,Vuforia.TrackerData/TrackableResultData>
struct Func_2_tD4955204FFB5A50CA24FF4974284E149A59D6136;
// System.Func`2<Vuforia.TrackerData/VuMarkTargetResultData,System.Int32>
struct Func_2_tA8E4FF64EEF8A5BB3CD0DA7185224DA89FD65166;
// System.Func`2<Vuforia.TrackerData/VuMarkTargetResultData,Vuforia.TrackableBehaviour/Status>
struct Func_2_tD56B3E30FD7CB7B6C9BBEBFD094C2437FD6C5028;
// System.Func`2<Vuforia.TrackerData/VuMarkTargetResultData,Vuforia.TrackerData/VuMarkTargetResultData>
struct Func_2_t643EB9103FB4685428597EAAF61735FC0C319932;
// System.Func`2<Vuforia.VuMarkBehaviour,System.Boolean>
struct Func_2_t312F769A21CE39065F93E4424EBAC780E5795565;
// System.Func`3<System.String,System.Int32,System.String>
struct Func_3_t9C9BE7F16D3C7AEBACE6A589ACA52C45553AB417;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AsyncOperation
struct AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Camera[]
struct CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Shader
struct Shader_tE2731FF351B74AB4186897484FB01E000C1160CA;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.WebCamTexture
struct WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73;
// Vuforia.CameraCalibrationComparer
struct CameraCalibrationComparer_t7CF72E6611106D56BB040F32DB6D7BFF0898F7EA;
// Vuforia.CustomDataSetTargetSize
struct CustomDataSetTargetSize_t2BD3BD33FBB280F4FA853FB9E9D00ED16BCBF031;
// Vuforia.CylinderTarget
struct CylinderTarget_t83B279B1917FBC92AAAFC41CC10D9132AB461085;
// Vuforia.DataSet
struct DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644;
// Vuforia.DeviceTrackingManager
struct DeviceTrackingManager_t1D264AEA989AA45ED5A85F7FB142F38DB3EE482A;
// Vuforia.DigitalEyewearARController
struct DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4;
// Vuforia.DigitalEyewearARController/SerializableViewerParameters
struct SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9;
// Vuforia.ICameraConfiguration
struct ICameraConfiguration_tB7509A17C94E12154FA84F00D5E0A03EE3F4AF3F;
// Vuforia.IExtendedTrackingManager
struct IExtendedTrackingManager_t19EAD26ECD63C6C68F72CD8ACA00FF9C912A9C15;
// Vuforia.IHoloLensApiAbstraction
struct IHoloLensApiAbstraction_t12E3611D625B9E76CD31BE15058EE9D4A058AE24;
// Vuforia.IPlayModeEditorUtility
struct IPlayModeEditorUtility_tD0CB67AAF485BDA578DC8F4BBD8C1F3CC9202DCA;
// Vuforia.IPremiumObjectFactory
struct IPremiumObjectFactory_t3CE54AD8623E5CA2896593A774FBFFD8E84B20CD;
// Vuforia.ITargetSize
struct ITargetSize_t93FE85335C3D5B63DDB1C5EE2CE85A928F405898;
// Vuforia.IVuforiaWrapper
struct IVuforiaWrapper_t9B4B3D91B44F2ECED1836F91D6320F8F30724FE2;
// Vuforia.IWebCamTexAdaptor
struct IWebCamTexAdaptor_t3ACEA77F93B1E588C67D123F92A47E179E4A8672;
// Vuforia.IlluminationManager
struct IlluminationManager_t20764D05A4DA19AE6C1CADF24630DEFC5B59E296;
// Vuforia.ImageTarget
struct ImageTarget_t916FA6EEDF77BFCE0C8490D8E534D871354B7E1C;
// Vuforia.ImageTargetBuilder
struct ImageTargetBuilder_t5BA66A134696E24A591FA066087BAABE66F00755;
// Vuforia.LateLatchingManager
struct LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C;
// Vuforia.MultiTarget
struct MultiTarget_tE5E82497457AA47E0DEDBBE9AB5B7773B16C528D;
// Vuforia.TextureRenderer
struct TextureRenderer_t033DF107DC5C8ADC8C63277D48BA615920542D22;
// Vuforia.Trackable
struct Trackable_t2A23C572321E7D4FEAC9A1019DFA0AA144FC9B8F;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4;
// Vuforia.TrackableSource
struct TrackableSource_tD34E68F40275ADD2D4284E6CF1DA1800C8194E39;
// Vuforia.TrackerData/TrackableResultData[]
struct TrackableResultDataU5BU5D_t9F14AA7BB2B4B86B497397AA4ABC42C964D0477D;
// Vuforia.TrackerData/VuMarkTargetData[]
struct VuMarkTargetDataU5BU5D_tDEF295778F1EBA00260FE5F7CF00E0E91F5E3605;
// Vuforia.TrackerData/VuMarkTargetResultData[]
struct VuMarkTargetResultDataU5BU5D_t0AAD90D01B7B61C91A7B4DE994F065D01013FF9A;
// Vuforia.VuMarkBehaviour
struct VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4;
// Vuforia.VuMarkManager
struct VuMarkManager_tD0288834C61D3F733AD8E4E98FE9C5DED6AC56F9;
// Vuforia.VuMarkTarget
struct VuMarkTarget_t8C60386EF2743C3E5CDB5CD3BC517FAD27118B10;
// Vuforia.VuMarkTemplate
struct VuMarkTemplate_t77A35C669991A2CC63075E8FCF018DA013A799DD;
// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2;
// Vuforia.VuforiaConfiguration/DatabaseConfiguration
struct DatabaseConfiguration_t0B1E7EF9676AD0191B8AFA87BFD43EB9F141928A;
// Vuforia.VuforiaConfiguration/DeviceTrackerConfiguration
struct DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F;
// Vuforia.VuforiaConfiguration/DigitalEyewearConfiguration
struct DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18;
// Vuforia.VuforiaConfiguration/GenericVuforiaConfiguration
struct GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064;
// Vuforia.VuforiaConfiguration/SmartTerrainConfiguration
struct SmartTerrainConfiguration_t6104CF56BC1AFE13ED509BB01CBE90E7E64372C7;
// Vuforia.VuforiaConfiguration/VideoBackgroundConfiguration
struct VideoBackgroundConfiguration_tCC24E374B966B79D018C14F6807A6DDA47302F17;
// Vuforia.VuforiaConfiguration/WebCamConfiguration
struct WebCamConfiguration_t0A5BD19A86D3AA8B68FB79C1EA8A5855A6C9F321;
// Vuforia.WorldCenterTrackableBehaviour
struct WorldCenterTrackableBehaviour_tA305098FD468B65DE0B5CC6BDED91127D1306A88;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ARCONTROLLER_TBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293_H
#define ARCONTROLLER_TBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ARController
struct  ARController_tBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293  : public RuntimeObject
{
public:
	// Vuforia.VuforiaBehaviour Vuforia.ARController::mVuforiaBehaviour
	VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2 * ___mVuforiaBehaviour_0;

public:
	inline static int32_t get_offset_of_mVuforiaBehaviour_0() { return static_cast<int32_t>(offsetof(ARController_tBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293, ___mVuforiaBehaviour_0)); }
	inline VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2 * get_mVuforiaBehaviour_0() const { return ___mVuforiaBehaviour_0; }
	inline VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2 ** get_address_of_mVuforiaBehaviour_0() { return &___mVuforiaBehaviour_0; }
	inline void set_mVuforiaBehaviour_0(VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2 * value)
	{
		___mVuforiaBehaviour_0 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCONTROLLER_TBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293_H
#ifndef IMAGETARGETBUILDER_T5BA66A134696E24A591FA066087BAABE66F00755_H
#define IMAGETARGETBUILDER_T5BA66A134696E24A591FA066087BAABE66F00755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetBuilder
struct  ImageTargetBuilder_t5BA66A134696E24A591FA066087BAABE66F00755  : public RuntimeObject
{
public:
	// Vuforia.TrackableSource Vuforia.ImageTargetBuilder::mTrackableSource
	TrackableSource_tD34E68F40275ADD2D4284E6CF1DA1800C8194E39 * ___mTrackableSource_0;
	// System.Boolean Vuforia.ImageTargetBuilder::mIsScanning
	bool ___mIsScanning_1;

public:
	inline static int32_t get_offset_of_mTrackableSource_0() { return static_cast<int32_t>(offsetof(ImageTargetBuilder_t5BA66A134696E24A591FA066087BAABE66F00755, ___mTrackableSource_0)); }
	inline TrackableSource_tD34E68F40275ADD2D4284E6CF1DA1800C8194E39 * get_mTrackableSource_0() const { return ___mTrackableSource_0; }
	inline TrackableSource_tD34E68F40275ADD2D4284E6CF1DA1800C8194E39 ** get_address_of_mTrackableSource_0() { return &___mTrackableSource_0; }
	inline void set_mTrackableSource_0(TrackableSource_tD34E68F40275ADD2D4284E6CF1DA1800C8194E39 * value)
	{
		___mTrackableSource_0 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableSource_0), value);
	}

	inline static int32_t get_offset_of_mIsScanning_1() { return static_cast<int32_t>(offsetof(ImageTargetBuilder_t5BA66A134696E24A591FA066087BAABE66F00755, ___mIsScanning_1)); }
	inline bool get_mIsScanning_1() const { return ___mIsScanning_1; }
	inline bool* get_address_of_mIsScanning_1() { return &___mIsScanning_1; }
	inline void set_mIsScanning_1(bool value)
	{
		___mIsScanning_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETBUILDER_T5BA66A134696E24A591FA066087BAABE66F00755_H
#ifndef PLAYMODEEDITORUTILITY_T4C4971B1A8CE64AD2D21007CB4ABDAABD2D2951A_H
#define PLAYMODEEDITORUTILITY_T4C4971B1A8CE64AD2D21007CB4ABDAABD2D2951A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PlayModeEditorUtility
struct  PlayModeEditorUtility_t4C4971B1A8CE64AD2D21007CB4ABDAABD2D2951A  : public RuntimeObject
{
public:

public:
};

struct PlayModeEditorUtility_t4C4971B1A8CE64AD2D21007CB4ABDAABD2D2951A_StaticFields
{
public:
	// Vuforia.IPlayModeEditorUtility Vuforia.PlayModeEditorUtility::sInstance
	RuntimeObject* ___sInstance_0;

public:
	inline static int32_t get_offset_of_sInstance_0() { return static_cast<int32_t>(offsetof(PlayModeEditorUtility_t4C4971B1A8CE64AD2D21007CB4ABDAABD2D2951A_StaticFields, ___sInstance_0)); }
	inline RuntimeObject* get_sInstance_0() const { return ___sInstance_0; }
	inline RuntimeObject** get_address_of_sInstance_0() { return &___sInstance_0; }
	inline void set_sInstance_0(RuntimeObject* value)
	{
		___sInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMODEEDITORUTILITY_T4C4971B1A8CE64AD2D21007CB4ABDAABD2D2951A_H
#ifndef NULLPLAYMODEEDITORUTILITY_T06E84C91A80EB0DD0D7E5DBA780DC16CA2FBBE89_H
#define NULLPLAYMODEEDITORUTILITY_T06E84C91A80EB0DD0D7E5DBA780DC16CA2FBBE89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PlayModeEditorUtility_NullPlayModeEditorUtility
struct  NullPlayModeEditorUtility_t06E84C91A80EB0DD0D7E5DBA780DC16CA2FBBE89  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLPLAYMODEEDITORUTILITY_T06E84C91A80EB0DD0D7E5DBA780DC16CA2FBBE89_H
#ifndef U3CU3EC_TB435526A5CF5263ED816F1A1E6F72ED3E7168DB8_H
#define U3CU3EC_TB435526A5CF5263ED816F1A1E6F72ED3E7168DB8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PosixPath_<>c
struct  U3CU3Ec_tB435526A5CF5263ED816F1A1E6F72ED3E7168DB8  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tB435526A5CF5263ED816F1A1E6F72ED3E7168DB8_StaticFields
{
public:
	// Vuforia.PosixPath_<>c Vuforia.PosixPath_<>c::<>9
	U3CU3Ec_tB435526A5CF5263ED816F1A1E6F72ED3E7168DB8 * ___U3CU3E9_0;
	// System.Func`2<System.String,System.Boolean> Vuforia.PosixPath_<>c::<>9__1_0
	Func_2_t3AD4B0F443BFD399C4AC2D6EE99FFE3BC0970017 * ___U3CU3E9__1_0_1;
	// System.Func`3<System.String,System.Int32,System.String> Vuforia.PosixPath_<>c::<>9__1_1
	Func_3_t9C9BE7F16D3C7AEBACE6A589ACA52C45553AB417 * ___U3CU3E9__1_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB435526A5CF5263ED816F1A1E6F72ED3E7168DB8_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tB435526A5CF5263ED816F1A1E6F72ED3E7168DB8 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tB435526A5CF5263ED816F1A1E6F72ED3E7168DB8 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tB435526A5CF5263ED816F1A1E6F72ED3E7168DB8 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB435526A5CF5263ED816F1A1E6F72ED3E7168DB8_StaticFields, ___U3CU3E9__1_0_1)); }
	inline Func_2_t3AD4B0F443BFD399C4AC2D6EE99FFE3BC0970017 * get_U3CU3E9__1_0_1() const { return ___U3CU3E9__1_0_1; }
	inline Func_2_t3AD4B0F443BFD399C4AC2D6EE99FFE3BC0970017 ** get_address_of_U3CU3E9__1_0_1() { return &___U3CU3E9__1_0_1; }
	inline void set_U3CU3E9__1_0_1(Func_2_t3AD4B0F443BFD399C4AC2D6EE99FFE3BC0970017 * value)
	{
		___U3CU3E9__1_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB435526A5CF5263ED816F1A1E6F72ED3E7168DB8_StaticFields, ___U3CU3E9__1_1_2)); }
	inline Func_3_t9C9BE7F16D3C7AEBACE6A589ACA52C45553AB417 * get_U3CU3E9__1_1_2() const { return ___U3CU3E9__1_1_2; }
	inline Func_3_t9C9BE7F16D3C7AEBACE6A589ACA52C45553AB417 ** get_address_of_U3CU3E9__1_1_2() { return &___U3CU3E9__1_1_2; }
	inline void set_U3CU3E9__1_1_2(Func_3_t9C9BE7F16D3C7AEBACE6A589ACA52C45553AB417 * value)
	{
		___U3CU3E9__1_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TB435526A5CF5263ED816F1A1E6F72ED3E7168DB8_H
#ifndef PREMIUMOBJECTFACTORY_T1C221706BF440DC37E00859C8D0851C34773AF79_H
#define PREMIUMOBJECTFACTORY_T1C221706BF440DC37E00859C8D0851C34773AF79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PremiumObjectFactory
struct  PremiumObjectFactory_t1C221706BF440DC37E00859C8D0851C34773AF79  : public RuntimeObject
{
public:

public:
};

struct PremiumObjectFactory_t1C221706BF440DC37E00859C8D0851C34773AF79_StaticFields
{
public:
	// Vuforia.IPremiumObjectFactory Vuforia.PremiumObjectFactory::sInstance
	RuntimeObject* ___sInstance_0;

public:
	inline static int32_t get_offset_of_sInstance_0() { return static_cast<int32_t>(offsetof(PremiumObjectFactory_t1C221706BF440DC37E00859C8D0851C34773AF79_StaticFields, ___sInstance_0)); }
	inline RuntimeObject* get_sInstance_0() const { return ___sInstance_0; }
	inline RuntimeObject** get_address_of_sInstance_0() { return &___sInstance_0; }
	inline void set_sInstance_0(RuntimeObject* value)
	{
		___sInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREMIUMOBJECTFACTORY_T1C221706BF440DC37E00859C8D0851C34773AF79_H
#ifndef NULLPREMIUMOBJECTFACTORY_TCCF6718E86AE7E65FB25DDF81A0C4E75A2767032_H
#define NULLPREMIUMOBJECTFACTORY_TCCF6718E86AE7E65FB25DDF81A0C4E75A2767032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PremiumObjectFactory_NullPremiumObjectFactory
struct  NullPremiumObjectFactory_tCCF6718E86AE7E65FB25DDF81A0C4E75A2767032  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLPREMIUMOBJECTFACTORY_TCCF6718E86AE7E65FB25DDF81A0C4E75A2767032_H
#ifndef STATEMANAGER_T3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1_H
#define STATEMANAGER_T3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.StateManager
struct  StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour> Vuforia.StateManager::mTrackableBehaviours
	Dictionary_2_tA086E5DF05B5E12AB5BD479A0A0566E77144E53F * ___mTrackableBehaviours_0;
	// System.Collections.Generic.List`1<System.Int32> Vuforia.StateManager::mAutomaticallyCreatedBehaviours
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___mAutomaticallyCreatedBehaviours_1;
	// System.Collections.Generic.List`1<Vuforia.TrackableBehaviour> Vuforia.StateManager::mBehavioursMarkedForDeletion
	List_1_tE21EDDA7B9D124D56D393C74E3F66347A01B8147 * ___mBehavioursMarkedForDeletion_2;
	// System.Collections.Generic.List`1<Vuforia.TrackableBehaviour> Vuforia.StateManager::mActiveTrackableBehaviours
	List_1_tE21EDDA7B9D124D56D393C74E3F66347A01B8147 * ___mActiveTrackableBehaviours_3;
	// Vuforia.VuMarkManager Vuforia.StateManager::mVuMarkManager
	VuMarkManager_tD0288834C61D3F733AD8E4E98FE9C5DED6AC56F9 * ___mVuMarkManager_4;
	// Vuforia.DeviceTrackingManager Vuforia.StateManager::mDeviceTrackingManager
	DeviceTrackingManager_t1D264AEA989AA45ED5A85F7FB142F38DB3EE482A * ___mDeviceTrackingManager_5;
	// UnityEngine.GameObject Vuforia.StateManager::mCameraPositioningHelper
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mCameraPositioningHelper_6;
	// Vuforia.IExtendedTrackingManager Vuforia.StateManager::mExtendedTrackingManager
	RuntimeObject* ___mExtendedTrackingManager_7;
	// Vuforia.IlluminationManager Vuforia.StateManager::mIlluminationManager
	IlluminationManager_t20764D05A4DA19AE6C1CADF24630DEFC5B59E296 * ___mIlluminationManager_8;

public:
	inline static int32_t get_offset_of_mTrackableBehaviours_0() { return static_cast<int32_t>(offsetof(StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1, ___mTrackableBehaviours_0)); }
	inline Dictionary_2_tA086E5DF05B5E12AB5BD479A0A0566E77144E53F * get_mTrackableBehaviours_0() const { return ___mTrackableBehaviours_0; }
	inline Dictionary_2_tA086E5DF05B5E12AB5BD479A0A0566E77144E53F ** get_address_of_mTrackableBehaviours_0() { return &___mTrackableBehaviours_0; }
	inline void set_mTrackableBehaviours_0(Dictionary_2_tA086E5DF05B5E12AB5BD479A0A0566E77144E53F * value)
	{
		___mTrackableBehaviours_0 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviours_0), value);
	}

	inline static int32_t get_offset_of_mAutomaticallyCreatedBehaviours_1() { return static_cast<int32_t>(offsetof(StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1, ___mAutomaticallyCreatedBehaviours_1)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_mAutomaticallyCreatedBehaviours_1() const { return ___mAutomaticallyCreatedBehaviours_1; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_mAutomaticallyCreatedBehaviours_1() { return &___mAutomaticallyCreatedBehaviours_1; }
	inline void set_mAutomaticallyCreatedBehaviours_1(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___mAutomaticallyCreatedBehaviours_1 = value;
		Il2CppCodeGenWriteBarrier((&___mAutomaticallyCreatedBehaviours_1), value);
	}

	inline static int32_t get_offset_of_mBehavioursMarkedForDeletion_2() { return static_cast<int32_t>(offsetof(StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1, ___mBehavioursMarkedForDeletion_2)); }
	inline List_1_tE21EDDA7B9D124D56D393C74E3F66347A01B8147 * get_mBehavioursMarkedForDeletion_2() const { return ___mBehavioursMarkedForDeletion_2; }
	inline List_1_tE21EDDA7B9D124D56D393C74E3F66347A01B8147 ** get_address_of_mBehavioursMarkedForDeletion_2() { return &___mBehavioursMarkedForDeletion_2; }
	inline void set_mBehavioursMarkedForDeletion_2(List_1_tE21EDDA7B9D124D56D393C74E3F66347A01B8147 * value)
	{
		___mBehavioursMarkedForDeletion_2 = value;
		Il2CppCodeGenWriteBarrier((&___mBehavioursMarkedForDeletion_2), value);
	}

	inline static int32_t get_offset_of_mActiveTrackableBehaviours_3() { return static_cast<int32_t>(offsetof(StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1, ___mActiveTrackableBehaviours_3)); }
	inline List_1_tE21EDDA7B9D124D56D393C74E3F66347A01B8147 * get_mActiveTrackableBehaviours_3() const { return ___mActiveTrackableBehaviours_3; }
	inline List_1_tE21EDDA7B9D124D56D393C74E3F66347A01B8147 ** get_address_of_mActiveTrackableBehaviours_3() { return &___mActiveTrackableBehaviours_3; }
	inline void set_mActiveTrackableBehaviours_3(List_1_tE21EDDA7B9D124D56D393C74E3F66347A01B8147 * value)
	{
		___mActiveTrackableBehaviours_3 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveTrackableBehaviours_3), value);
	}

	inline static int32_t get_offset_of_mVuMarkManager_4() { return static_cast<int32_t>(offsetof(StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1, ___mVuMarkManager_4)); }
	inline VuMarkManager_tD0288834C61D3F733AD8E4E98FE9C5DED6AC56F9 * get_mVuMarkManager_4() const { return ___mVuMarkManager_4; }
	inline VuMarkManager_tD0288834C61D3F733AD8E4E98FE9C5DED6AC56F9 ** get_address_of_mVuMarkManager_4() { return &___mVuMarkManager_4; }
	inline void set_mVuMarkManager_4(VuMarkManager_tD0288834C61D3F733AD8E4E98FE9C5DED6AC56F9 * value)
	{
		___mVuMarkManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkManager_4), value);
	}

	inline static int32_t get_offset_of_mDeviceTrackingManager_5() { return static_cast<int32_t>(offsetof(StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1, ___mDeviceTrackingManager_5)); }
	inline DeviceTrackingManager_t1D264AEA989AA45ED5A85F7FB142F38DB3EE482A * get_mDeviceTrackingManager_5() const { return ___mDeviceTrackingManager_5; }
	inline DeviceTrackingManager_t1D264AEA989AA45ED5A85F7FB142F38DB3EE482A ** get_address_of_mDeviceTrackingManager_5() { return &___mDeviceTrackingManager_5; }
	inline void set_mDeviceTrackingManager_5(DeviceTrackingManager_t1D264AEA989AA45ED5A85F7FB142F38DB3EE482A * value)
	{
		___mDeviceTrackingManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___mDeviceTrackingManager_5), value);
	}

	inline static int32_t get_offset_of_mCameraPositioningHelper_6() { return static_cast<int32_t>(offsetof(StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1, ___mCameraPositioningHelper_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mCameraPositioningHelper_6() const { return ___mCameraPositioningHelper_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mCameraPositioningHelper_6() { return &___mCameraPositioningHelper_6; }
	inline void set_mCameraPositioningHelper_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mCameraPositioningHelper_6 = value;
		Il2CppCodeGenWriteBarrier((&___mCameraPositioningHelper_6), value);
	}

	inline static int32_t get_offset_of_mExtendedTrackingManager_7() { return static_cast<int32_t>(offsetof(StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1, ___mExtendedTrackingManager_7)); }
	inline RuntimeObject* get_mExtendedTrackingManager_7() const { return ___mExtendedTrackingManager_7; }
	inline RuntimeObject** get_address_of_mExtendedTrackingManager_7() { return &___mExtendedTrackingManager_7; }
	inline void set_mExtendedTrackingManager_7(RuntimeObject* value)
	{
		___mExtendedTrackingManager_7 = value;
		Il2CppCodeGenWriteBarrier((&___mExtendedTrackingManager_7), value);
	}

	inline static int32_t get_offset_of_mIlluminationManager_8() { return static_cast<int32_t>(offsetof(StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1, ___mIlluminationManager_8)); }
	inline IlluminationManager_t20764D05A4DA19AE6C1CADF24630DEFC5B59E296 * get_mIlluminationManager_8() const { return ___mIlluminationManager_8; }
	inline IlluminationManager_t20764D05A4DA19AE6C1CADF24630DEFC5B59E296 ** get_address_of_mIlluminationManager_8() { return &___mIlluminationManager_8; }
	inline void set_mIlluminationManager_8(IlluminationManager_t20764D05A4DA19AE6C1CADF24630DEFC5B59E296 * value)
	{
		___mIlluminationManager_8 = value;
		Il2CppCodeGenWriteBarrier((&___mIlluminationManager_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEMANAGER_T3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1_H
#ifndef U3CU3EC_T5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_H
#define U3CU3EC_T5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.StateManager_<>c
struct  U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_StaticFields
{
public:
	// Vuforia.StateManager_<>c Vuforia.StateManager_<>c::<>9
	U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010 * ___U3CU3E9_0;
	// System.Func`2<Vuforia.VuMarkBehaviour,System.Boolean> Vuforia.StateManager_<>c::<>9__31_0
	Func_2_t312F769A21CE39065F93E4424EBAC780E5795565 * ___U3CU3E9__31_0_1;
	// System.Func`2<Vuforia.TrackerData_VuMarkTargetResultData,System.Int32> Vuforia.StateManager_<>c::<>9__31_1
	Func_2_tA8E4FF64EEF8A5BB3CD0DA7185224DA89FD65166 * ___U3CU3E9__31_1_2;
	// System.Func`2<Vuforia.TrackerData_VuMarkTargetResultData,Vuforia.TrackerData_VuMarkTargetResultData> Vuforia.StateManager_<>c::<>9__31_2
	Func_2_t643EB9103FB4685428597EAAF61735FC0C319932 * ___U3CU3E9__31_2_3;
	// System.Func`2<Vuforia.TrackerData_VuMarkTargetResultData,Vuforia.TrackableBehaviour_Status> Vuforia.StateManager_<>c::<>9__31_3
	Func_2_tD56B3E30FD7CB7B6C9BBEBFD094C2437FD6C5028 * ___U3CU3E9__31_3_4;
	// System.Func`2<Vuforia.TrackableBehaviour,System.Boolean> Vuforia.StateManager_<>c::<>9__32_0
	Func_2_tB1EDC94D5DE2903BAA30A4C2DF00098781945AA6 * ___U3CU3E9__32_0_5;
	// System.Func`2<Vuforia.TrackerData_TrackableResultData,System.Int32> Vuforia.StateManager_<>c::<>9__32_1
	Func_2_t9505439C411FE019A9092CD2D73D7AE231ED1C15 * ___U3CU3E9__32_1_6;
	// System.Func`2<Vuforia.TrackerData_TrackableResultData,Vuforia.TrackerData_TrackableResultData> Vuforia.StateManager_<>c::<>9__32_2
	Func_2_tD4955204FFB5A50CA24FF4974284E149A59D6136 * ___U3CU3E9__32_2_7;
	// System.Func`2<Vuforia.TrackerData_TrackableResultData,Vuforia.TrackableBehaviour_Status> Vuforia.StateManager_<>c::<>9__32_3
	Func_2_t1AF4389885EDCAF20EE4E6FBD0CD50A435897369 * ___U3CU3E9__32_3_8;
	// System.Func`2<Vuforia.TrackerData_TrackableResultData,Vuforia.TrackableBehaviour_StatusInfo> Vuforia.StateManager_<>c::<>9__32_4
	Func_2_tFA00CC82AB530CBB3AE283838E61F2A92D8AADEA * ___U3CU3E9__32_4_9;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_StaticFields, ___U3CU3E9__31_0_1)); }
	inline Func_2_t312F769A21CE39065F93E4424EBAC780E5795565 * get_U3CU3E9__31_0_1() const { return ___U3CU3E9__31_0_1; }
	inline Func_2_t312F769A21CE39065F93E4424EBAC780E5795565 ** get_address_of_U3CU3E9__31_0_1() { return &___U3CU3E9__31_0_1; }
	inline void set_U3CU3E9__31_0_1(Func_2_t312F769A21CE39065F93E4424EBAC780E5795565 * value)
	{
		___U3CU3E9__31_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__31_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_StaticFields, ___U3CU3E9__31_1_2)); }
	inline Func_2_tA8E4FF64EEF8A5BB3CD0DA7185224DA89FD65166 * get_U3CU3E9__31_1_2() const { return ___U3CU3E9__31_1_2; }
	inline Func_2_tA8E4FF64EEF8A5BB3CD0DA7185224DA89FD65166 ** get_address_of_U3CU3E9__31_1_2() { return &___U3CU3E9__31_1_2; }
	inline void set_U3CU3E9__31_1_2(Func_2_tA8E4FF64EEF8A5BB3CD0DA7185224DA89FD65166 * value)
	{
		___U3CU3E9__31_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__31_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_StaticFields, ___U3CU3E9__31_2_3)); }
	inline Func_2_t643EB9103FB4685428597EAAF61735FC0C319932 * get_U3CU3E9__31_2_3() const { return ___U3CU3E9__31_2_3; }
	inline Func_2_t643EB9103FB4685428597EAAF61735FC0C319932 ** get_address_of_U3CU3E9__31_2_3() { return &___U3CU3E9__31_2_3; }
	inline void set_U3CU3E9__31_2_3(Func_2_t643EB9103FB4685428597EAAF61735FC0C319932 * value)
	{
		___U3CU3E9__31_2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__31_2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_3_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_StaticFields, ___U3CU3E9__31_3_4)); }
	inline Func_2_tD56B3E30FD7CB7B6C9BBEBFD094C2437FD6C5028 * get_U3CU3E9__31_3_4() const { return ___U3CU3E9__31_3_4; }
	inline Func_2_tD56B3E30FD7CB7B6C9BBEBFD094C2437FD6C5028 ** get_address_of_U3CU3E9__31_3_4() { return &___U3CU3E9__31_3_4; }
	inline void set_U3CU3E9__31_3_4(Func_2_tD56B3E30FD7CB7B6C9BBEBFD094C2437FD6C5028 * value)
	{
		___U3CU3E9__31_3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__31_3_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__32_0_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_StaticFields, ___U3CU3E9__32_0_5)); }
	inline Func_2_tB1EDC94D5DE2903BAA30A4C2DF00098781945AA6 * get_U3CU3E9__32_0_5() const { return ___U3CU3E9__32_0_5; }
	inline Func_2_tB1EDC94D5DE2903BAA30A4C2DF00098781945AA6 ** get_address_of_U3CU3E9__32_0_5() { return &___U3CU3E9__32_0_5; }
	inline void set_U3CU3E9__32_0_5(Func_2_tB1EDC94D5DE2903BAA30A4C2DF00098781945AA6 * value)
	{
		___U3CU3E9__32_0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__32_0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__32_1_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_StaticFields, ___U3CU3E9__32_1_6)); }
	inline Func_2_t9505439C411FE019A9092CD2D73D7AE231ED1C15 * get_U3CU3E9__32_1_6() const { return ___U3CU3E9__32_1_6; }
	inline Func_2_t9505439C411FE019A9092CD2D73D7AE231ED1C15 ** get_address_of_U3CU3E9__32_1_6() { return &___U3CU3E9__32_1_6; }
	inline void set_U3CU3E9__32_1_6(Func_2_t9505439C411FE019A9092CD2D73D7AE231ED1C15 * value)
	{
		___U3CU3E9__32_1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__32_1_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__32_2_7() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_StaticFields, ___U3CU3E9__32_2_7)); }
	inline Func_2_tD4955204FFB5A50CA24FF4974284E149A59D6136 * get_U3CU3E9__32_2_7() const { return ___U3CU3E9__32_2_7; }
	inline Func_2_tD4955204FFB5A50CA24FF4974284E149A59D6136 ** get_address_of_U3CU3E9__32_2_7() { return &___U3CU3E9__32_2_7; }
	inline void set_U3CU3E9__32_2_7(Func_2_tD4955204FFB5A50CA24FF4974284E149A59D6136 * value)
	{
		___U3CU3E9__32_2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__32_2_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__32_3_8() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_StaticFields, ___U3CU3E9__32_3_8)); }
	inline Func_2_t1AF4389885EDCAF20EE4E6FBD0CD50A435897369 * get_U3CU3E9__32_3_8() const { return ___U3CU3E9__32_3_8; }
	inline Func_2_t1AF4389885EDCAF20EE4E6FBD0CD50A435897369 ** get_address_of_U3CU3E9__32_3_8() { return &___U3CU3E9__32_3_8; }
	inline void set_U3CU3E9__32_3_8(Func_2_t1AF4389885EDCAF20EE4E6FBD0CD50A435897369 * value)
	{
		___U3CU3E9__32_3_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__32_3_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__32_4_9() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_StaticFields, ___U3CU3E9__32_4_9)); }
	inline Func_2_tFA00CC82AB530CBB3AE283838E61F2A92D8AADEA * get_U3CU3E9__32_4_9() const { return ___U3CU3E9__32_4_9; }
	inline Func_2_tFA00CC82AB530CBB3AE283838E61F2A92D8AADEA ** get_address_of_U3CU3E9__32_4_9() { return &___U3CU3E9__32_4_9; }
	inline void set_U3CU3E9__32_4_9(Func_2_tFA00CC82AB530CBB3AE283838E61F2A92D8AADEA * value)
	{
		___U3CU3E9__32_4_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__32_4_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_H
#ifndef TEXTURERENDERER_T033DF107DC5C8ADC8C63277D48BA615920542D22_H
#define TEXTURERENDERER_T033DF107DC5C8ADC8C63277D48BA615920542D22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TextureRenderer
struct  TextureRenderer_t033DF107DC5C8ADC8C63277D48BA615920542D22  : public RuntimeObject
{
public:
	// UnityEngine.Camera Vuforia.TextureRenderer::mTextureBufferCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___mTextureBufferCamera_0;
	// System.Int32 Vuforia.TextureRenderer::mTextureWidth
	int32_t ___mTextureWidth_1;
	// System.Int32 Vuforia.TextureRenderer::mTextureHeight
	int32_t ___mTextureHeight_2;

public:
	inline static int32_t get_offset_of_mTextureBufferCamera_0() { return static_cast<int32_t>(offsetof(TextureRenderer_t033DF107DC5C8ADC8C63277D48BA615920542D22, ___mTextureBufferCamera_0)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_mTextureBufferCamera_0() const { return ___mTextureBufferCamera_0; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_mTextureBufferCamera_0() { return &___mTextureBufferCamera_0; }
	inline void set_mTextureBufferCamera_0(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___mTextureBufferCamera_0 = value;
		Il2CppCodeGenWriteBarrier((&___mTextureBufferCamera_0), value);
	}

	inline static int32_t get_offset_of_mTextureWidth_1() { return static_cast<int32_t>(offsetof(TextureRenderer_t033DF107DC5C8ADC8C63277D48BA615920542D22, ___mTextureWidth_1)); }
	inline int32_t get_mTextureWidth_1() const { return ___mTextureWidth_1; }
	inline int32_t* get_address_of_mTextureWidth_1() { return &___mTextureWidth_1; }
	inline void set_mTextureWidth_1(int32_t value)
	{
		___mTextureWidth_1 = value;
	}

	inline static int32_t get_offset_of_mTextureHeight_2() { return static_cast<int32_t>(offsetof(TextureRenderer_t033DF107DC5C8ADC8C63277D48BA615920542D22, ___mTextureHeight_2)); }
	inline int32_t get_mTextureHeight_2() const { return ___mTextureHeight_2; }
	inline int32_t* get_address_of_mTextureHeight_2() { return &___mTextureHeight_2; }
	inline void set_mTextureHeight_2(int32_t value)
	{
		___mTextureHeight_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURERENDERER_T033DF107DC5C8ADC8C63277D48BA615920542D22_H
#ifndef TRACKABLEIMPL_T97E7D175DE38822AF55FC03FCA26FB62B7E290A2_H
#define TRACKABLEIMPL_T97E7D175DE38822AF55FC03FCA26FB62B7E290A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableImpl
struct  TrackableImpl_t97E7D175DE38822AF55FC03FCA26FB62B7E290A2  : public RuntimeObject
{
public:
	// System.String Vuforia.TrackableImpl::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Int32 Vuforia.TrackableImpl::<ID>k__BackingField
	int32_t ___U3CIDU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TrackableImpl_t97E7D175DE38822AF55FC03FCA26FB62B7E290A2, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CIDU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TrackableImpl_t97E7D175DE38822AF55FC03FCA26FB62B7E290A2, ___U3CIDU3Ek__BackingField_1)); }
	inline int32_t get_U3CIDU3Ek__BackingField_1() const { return ___U3CIDU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CIDU3Ek__BackingField_1() { return &___U3CIDU3Ek__BackingField_1; }
	inline void set_U3CIDU3Ek__BackingField_1(int32_t value)
	{
		___U3CIDU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEIMPL_T97E7D175DE38822AF55FC03FCA26FB62B7E290A2_H
#ifndef TRACKER_T11C8E7B84615512E8125186CDC5DF90D9D7B58F1_H
#define TRACKER_T11C8E7B84615512E8125186CDC5DF90D9D7B58F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Tracker
struct  Tracker_t11C8E7B84615512E8125186CDC5DF90D9D7B58F1  : public RuntimeObject
{
public:
	// System.Boolean Vuforia.Tracker::<IsActive>k__BackingField
	bool ___U3CIsActiveU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CIsActiveU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Tracker_t11C8E7B84615512E8125186CDC5DF90D9D7B58F1, ___U3CIsActiveU3Ek__BackingField_0)); }
	inline bool get_U3CIsActiveU3Ek__BackingField_0() const { return ___U3CIsActiveU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsActiveU3Ek__BackingField_0() { return &___U3CIsActiveU3Ek__BackingField_0; }
	inline void set_U3CIsActiveU3Ek__BackingField_0(bool value)
	{
		___U3CIsActiveU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKER_T11C8E7B84615512E8125186CDC5DF90D9D7B58F1_H
#ifndef TYPEMAPPING_TD7716603EBE8D435AD26ABAEC1C1BFD88D70E5AC_H
#define TYPEMAPPING_TD7716603EBE8D435AD26ABAEC1C1BFD88D70E5AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TypeMapping
struct  TypeMapping_tD7716603EBE8D435AD26ABAEC1C1BFD88D70E5AC  : public RuntimeObject
{
public:

public:
};

struct TypeMapping_tD7716603EBE8D435AD26ABAEC1C1BFD88D70E5AC_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.UInt16> Vuforia.TypeMapping::sTypes
	Dictionary_2_t44E234598935A034894846D9A90E3242F2F2D6D7 * ___sTypes_0;

public:
	inline static int32_t get_offset_of_sTypes_0() { return static_cast<int32_t>(offsetof(TypeMapping_tD7716603EBE8D435AD26ABAEC1C1BFD88D70E5AC_StaticFields, ___sTypes_0)); }
	inline Dictionary_2_t44E234598935A034894846D9A90E3242F2F2D6D7 * get_sTypes_0() const { return ___sTypes_0; }
	inline Dictionary_2_t44E234598935A034894846D9A90E3242F2F2D6D7 ** get_address_of_sTypes_0() { return &___sTypes_0; }
	inline void set_sTypes_0(Dictionary_2_t44E234598935A034894846D9A90E3242F2F2D6D7 * value)
	{
		___sTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&___sTypes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEMAPPING_TD7716603EBE8D435AD26ABAEC1C1BFD88D70E5AC_H
#ifndef VRDEVICECONTROLLER_TDFB2A6DE022256FD695E024050B1F3A07B97D044_H
#define VRDEVICECONTROLLER_TDFB2A6DE022256FD695E024050B1F3A07B97D044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VRDeviceController
struct  VRDeviceController_tDFB2A6DE022256FD695E024050B1F3A07B97D044  : public RuntimeObject
{
public:
	// System.Collections.Generic.HashSet`1<System.String> Vuforia.VRDeviceController::mHeadPoseDisablingClasses
	HashSet_1_tC8214FEC830040D37F12A482FF0284D9C2B9001E * ___mHeadPoseDisablingClasses_0;
	// System.Boolean Vuforia.VRDeviceController::mHeadPosesEnabledOnce
	bool ___mHeadPosesEnabledOnce_1;

public:
	inline static int32_t get_offset_of_mHeadPoseDisablingClasses_0() { return static_cast<int32_t>(offsetof(VRDeviceController_tDFB2A6DE022256FD695E024050B1F3A07B97D044, ___mHeadPoseDisablingClasses_0)); }
	inline HashSet_1_tC8214FEC830040D37F12A482FF0284D9C2B9001E * get_mHeadPoseDisablingClasses_0() const { return ___mHeadPoseDisablingClasses_0; }
	inline HashSet_1_tC8214FEC830040D37F12A482FF0284D9C2B9001E ** get_address_of_mHeadPoseDisablingClasses_0() { return &___mHeadPoseDisablingClasses_0; }
	inline void set_mHeadPoseDisablingClasses_0(HashSet_1_tC8214FEC830040D37F12A482FF0284D9C2B9001E * value)
	{
		___mHeadPoseDisablingClasses_0 = value;
		Il2CppCodeGenWriteBarrier((&___mHeadPoseDisablingClasses_0), value);
	}

	inline static int32_t get_offset_of_mHeadPosesEnabledOnce_1() { return static_cast<int32_t>(offsetof(VRDeviceController_tDFB2A6DE022256FD695E024050B1F3A07B97D044, ___mHeadPosesEnabledOnce_1)); }
	inline bool get_mHeadPosesEnabledOnce_1() const { return ___mHeadPosesEnabledOnce_1; }
	inline bool* get_address_of_mHeadPosesEnabledOnce_1() { return &___mHeadPosesEnabledOnce_1; }
	inline void set_mHeadPosesEnabledOnce_1(bool value)
	{
		___mHeadPosesEnabledOnce_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VRDEVICECONTROLLER_TDFB2A6DE022256FD695E024050B1F3A07B97D044_H
#ifndef VUMARKMANAGER_TD0288834C61D3F733AD8E4E98FE9C5DED6AC56F9_H
#define VUMARKMANAGER_TD0288834C61D3F733AD8E4E98FE9C5DED6AC56F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuMarkManager
struct  VuMarkManager_tD0288834C61D3F733AD8E4E98FE9C5DED6AC56F9  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<Vuforia.VuMarkBehaviour>> Vuforia.VuMarkManager::mBehaviours
	Dictionary_2_t028B7061DF6BB16493BDAD455EDBA593439778FE * ___mBehaviours_0;
	// System.Collections.Generic.List`1<Vuforia.VuMarkTarget> Vuforia.VuMarkManager::mActiveVuMarkTargets
	List_1_tBAEACD03FC5EAAC0FEDAC89712307241E318AA69 * ___mActiveVuMarkTargets_1;
	// System.Collections.Generic.List`1<Vuforia.VuMarkBehaviour> Vuforia.VuMarkManager::mDestroyedBehaviours
	List_1_tC8B88347864264398BE3608B6662A0EFEB5E8450 * ___mDestroyedBehaviours_2;
	// System.Action`1<Vuforia.VuMarkTarget> Vuforia.VuMarkManager::mOnVuMarkDetected
	Action_1_t54E72A24489A68155FB05D0D7258B36444955310 * ___mOnVuMarkDetected_3;
	// System.Action`1<Vuforia.VuMarkTarget> Vuforia.VuMarkManager::mOnVuMarkLost
	Action_1_t54E72A24489A68155FB05D0D7258B36444955310 * ___mOnVuMarkLost_4;
	// System.Action`1<Vuforia.VuMarkBehaviour> Vuforia.VuMarkManager::mOnVuMarkBehaviourDetected
	Action_1_t8E59E4A442B30DDF0D8C60EC54C9F8CBCE045E91 * ___mOnVuMarkBehaviourDetected_5;

public:
	inline static int32_t get_offset_of_mBehaviours_0() { return static_cast<int32_t>(offsetof(VuMarkManager_tD0288834C61D3F733AD8E4E98FE9C5DED6AC56F9, ___mBehaviours_0)); }
	inline Dictionary_2_t028B7061DF6BB16493BDAD455EDBA593439778FE * get_mBehaviours_0() const { return ___mBehaviours_0; }
	inline Dictionary_2_t028B7061DF6BB16493BDAD455EDBA593439778FE ** get_address_of_mBehaviours_0() { return &___mBehaviours_0; }
	inline void set_mBehaviours_0(Dictionary_2_t028B7061DF6BB16493BDAD455EDBA593439778FE * value)
	{
		___mBehaviours_0 = value;
		Il2CppCodeGenWriteBarrier((&___mBehaviours_0), value);
	}

	inline static int32_t get_offset_of_mActiveVuMarkTargets_1() { return static_cast<int32_t>(offsetof(VuMarkManager_tD0288834C61D3F733AD8E4E98FE9C5DED6AC56F9, ___mActiveVuMarkTargets_1)); }
	inline List_1_tBAEACD03FC5EAAC0FEDAC89712307241E318AA69 * get_mActiveVuMarkTargets_1() const { return ___mActiveVuMarkTargets_1; }
	inline List_1_tBAEACD03FC5EAAC0FEDAC89712307241E318AA69 ** get_address_of_mActiveVuMarkTargets_1() { return &___mActiveVuMarkTargets_1; }
	inline void set_mActiveVuMarkTargets_1(List_1_tBAEACD03FC5EAAC0FEDAC89712307241E318AA69 * value)
	{
		___mActiveVuMarkTargets_1 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveVuMarkTargets_1), value);
	}

	inline static int32_t get_offset_of_mDestroyedBehaviours_2() { return static_cast<int32_t>(offsetof(VuMarkManager_tD0288834C61D3F733AD8E4E98FE9C5DED6AC56F9, ___mDestroyedBehaviours_2)); }
	inline List_1_tC8B88347864264398BE3608B6662A0EFEB5E8450 * get_mDestroyedBehaviours_2() const { return ___mDestroyedBehaviours_2; }
	inline List_1_tC8B88347864264398BE3608B6662A0EFEB5E8450 ** get_address_of_mDestroyedBehaviours_2() { return &___mDestroyedBehaviours_2; }
	inline void set_mDestroyedBehaviours_2(List_1_tC8B88347864264398BE3608B6662A0EFEB5E8450 * value)
	{
		___mDestroyedBehaviours_2 = value;
		Il2CppCodeGenWriteBarrier((&___mDestroyedBehaviours_2), value);
	}

	inline static int32_t get_offset_of_mOnVuMarkDetected_3() { return static_cast<int32_t>(offsetof(VuMarkManager_tD0288834C61D3F733AD8E4E98FE9C5DED6AC56F9, ___mOnVuMarkDetected_3)); }
	inline Action_1_t54E72A24489A68155FB05D0D7258B36444955310 * get_mOnVuMarkDetected_3() const { return ___mOnVuMarkDetected_3; }
	inline Action_1_t54E72A24489A68155FB05D0D7258B36444955310 ** get_address_of_mOnVuMarkDetected_3() { return &___mOnVuMarkDetected_3; }
	inline void set_mOnVuMarkDetected_3(Action_1_t54E72A24489A68155FB05D0D7258B36444955310 * value)
	{
		___mOnVuMarkDetected_3 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuMarkDetected_3), value);
	}

	inline static int32_t get_offset_of_mOnVuMarkLost_4() { return static_cast<int32_t>(offsetof(VuMarkManager_tD0288834C61D3F733AD8E4E98FE9C5DED6AC56F9, ___mOnVuMarkLost_4)); }
	inline Action_1_t54E72A24489A68155FB05D0D7258B36444955310 * get_mOnVuMarkLost_4() const { return ___mOnVuMarkLost_4; }
	inline Action_1_t54E72A24489A68155FB05D0D7258B36444955310 ** get_address_of_mOnVuMarkLost_4() { return &___mOnVuMarkLost_4; }
	inline void set_mOnVuMarkLost_4(Action_1_t54E72A24489A68155FB05D0D7258B36444955310 * value)
	{
		___mOnVuMarkLost_4 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuMarkLost_4), value);
	}

	inline static int32_t get_offset_of_mOnVuMarkBehaviourDetected_5() { return static_cast<int32_t>(offsetof(VuMarkManager_tD0288834C61D3F733AD8E4E98FE9C5DED6AC56F9, ___mOnVuMarkBehaviourDetected_5)); }
	inline Action_1_t8E59E4A442B30DDF0D8C60EC54C9F8CBCE045E91 * get_mOnVuMarkBehaviourDetected_5() const { return ___mOnVuMarkBehaviourDetected_5; }
	inline Action_1_t8E59E4A442B30DDF0D8C60EC54C9F8CBCE045E91 ** get_address_of_mOnVuMarkBehaviourDetected_5() { return &___mOnVuMarkBehaviourDetected_5; }
	inline void set_mOnVuMarkBehaviourDetected_5(Action_1_t8E59E4A442B30DDF0D8C60EC54C9F8CBCE045E91 * value)
	{
		___mOnVuMarkBehaviourDetected_5 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuMarkBehaviourDetected_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKMANAGER_TD0288834C61D3F733AD8E4E98FE9C5DED6AC56F9_H
#ifndef DATABASECONFIGURATION_T0B1E7EF9676AD0191B8AFA87BFD43EB9F141928A_H
#define DATABASECONFIGURATION_T0B1E7EF9676AD0191B8AFA87BFD43EB9F141928A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaConfiguration_DatabaseConfiguration
struct  DatabaseConfiguration_t0B1E7EF9676AD0191B8AFA87BFD43EB9F141928A  : public RuntimeObject
{
public:
	// System.Boolean Vuforia.VuforiaConfiguration_DatabaseConfiguration::disableModelExtraction
	bool ___disableModelExtraction_0;

public:
	inline static int32_t get_offset_of_disableModelExtraction_0() { return static_cast<int32_t>(offsetof(DatabaseConfiguration_t0B1E7EF9676AD0191B8AFA87BFD43EB9F141928A, ___disableModelExtraction_0)); }
	inline bool get_disableModelExtraction_0() const { return ___disableModelExtraction_0; }
	inline bool* get_address_of_disableModelExtraction_0() { return &___disableModelExtraction_0; }
	inline void set_disableModelExtraction_0(bool value)
	{
		___disableModelExtraction_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATABASECONFIGURATION_T0B1E7EF9676AD0191B8AFA87BFD43EB9F141928A_H
#ifndef TRACKERCONFIGURATION_T15942AA3CED7034F69729C6FC20AFCD7C078670A_H
#define TRACKERCONFIGURATION_T15942AA3CED7034F69729C6FC20AFCD7C078670A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaConfiguration_TrackerConfiguration
struct  TrackerConfiguration_t15942AA3CED7034F69729C6FC20AFCD7C078670A  : public RuntimeObject
{
public:
	// System.Boolean Vuforia.VuforiaConfiguration_TrackerConfiguration::autoInitTracker
	bool ___autoInitTracker_0;
	// System.Boolean Vuforia.VuforiaConfiguration_TrackerConfiguration::autoStartTracker
	bool ___autoStartTracker_1;

public:
	inline static int32_t get_offset_of_autoInitTracker_0() { return static_cast<int32_t>(offsetof(TrackerConfiguration_t15942AA3CED7034F69729C6FC20AFCD7C078670A, ___autoInitTracker_0)); }
	inline bool get_autoInitTracker_0() const { return ___autoInitTracker_0; }
	inline bool* get_address_of_autoInitTracker_0() { return &___autoInitTracker_0; }
	inline void set_autoInitTracker_0(bool value)
	{
		___autoInitTracker_0 = value;
	}

	inline static int32_t get_offset_of_autoStartTracker_1() { return static_cast<int32_t>(offsetof(TrackerConfiguration_t15942AA3CED7034F69729C6FC20AFCD7C078670A, ___autoStartTracker_1)); }
	inline bool get_autoStartTracker_1() const { return ___autoStartTracker_1; }
	inline bool* get_address_of_autoStartTracker_1() { return &___autoStartTracker_1; }
	inline void set_autoStartTracker_1(bool value)
	{
		___autoStartTracker_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKERCONFIGURATION_T15942AA3CED7034F69729C6FC20AFCD7C078670A_H
#ifndef WEBCAMCONFIGURATION_T0A5BD19A86D3AA8B68FB79C1EA8A5855A6C9F321_H
#define WEBCAMCONFIGURATION_T0A5BD19A86D3AA8B68FB79C1EA8A5855A6C9F321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaConfiguration_WebCamConfiguration
struct  WebCamConfiguration_t0A5BD19A86D3AA8B68FB79C1EA8A5855A6C9F321  : public RuntimeObject
{
public:
	// System.String Vuforia.VuforiaConfiguration_WebCamConfiguration::deviceNameSetInEditor
	String_t* ___deviceNameSetInEditor_0;
	// System.Boolean Vuforia.VuforiaConfiguration_WebCamConfiguration::turnOffWebCam
	bool ___turnOffWebCam_1;
	// System.Int32 Vuforia.VuforiaConfiguration_WebCamConfiguration::renderTextureLayer
	int32_t ___renderTextureLayer_2;

public:
	inline static int32_t get_offset_of_deviceNameSetInEditor_0() { return static_cast<int32_t>(offsetof(WebCamConfiguration_t0A5BD19A86D3AA8B68FB79C1EA8A5855A6C9F321, ___deviceNameSetInEditor_0)); }
	inline String_t* get_deviceNameSetInEditor_0() const { return ___deviceNameSetInEditor_0; }
	inline String_t** get_address_of_deviceNameSetInEditor_0() { return &___deviceNameSetInEditor_0; }
	inline void set_deviceNameSetInEditor_0(String_t* value)
	{
		___deviceNameSetInEditor_0 = value;
		Il2CppCodeGenWriteBarrier((&___deviceNameSetInEditor_0), value);
	}

	inline static int32_t get_offset_of_turnOffWebCam_1() { return static_cast<int32_t>(offsetof(WebCamConfiguration_t0A5BD19A86D3AA8B68FB79C1EA8A5855A6C9F321, ___turnOffWebCam_1)); }
	inline bool get_turnOffWebCam_1() const { return ___turnOffWebCam_1; }
	inline bool* get_address_of_turnOffWebCam_1() { return &___turnOffWebCam_1; }
	inline void set_turnOffWebCam_1(bool value)
	{
		___turnOffWebCam_1 = value;
	}

	inline static int32_t get_offset_of_renderTextureLayer_2() { return static_cast<int32_t>(offsetof(WebCamConfiguration_t0A5BD19A86D3AA8B68FB79C1EA8A5855A6C9F321, ___renderTextureLayer_2)); }
	inline int32_t get_renderTextureLayer_2() const { return ___renderTextureLayer_2; }
	inline int32_t* get_address_of_renderTextureLayer_2() { return &___renderTextureLayer_2; }
	inline void set_renderTextureLayer_2(int32_t value)
	{
		___renderTextureLayer_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCAMCONFIGURATION_T0A5BD19A86D3AA8B68FB79C1EA8A5855A6C9F321_H
#ifndef VUFORIANATIVEWRAPPER_T2BC1D2FB459100031EE442E9607A94D6644CD2D2_H
#define VUFORIANATIVEWRAPPER_T2BC1D2FB459100031EE442E9607A94D6644CD2D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaNativeWrapper
struct  VuforiaNativeWrapper_t2BC1D2FB459100031EE442E9607A94D6644CD2D2  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIANATIVEWRAPPER_T2BC1D2FB459100031EE442E9607A94D6644CD2D2_H
#ifndef VUFORIANULLWRAPPER_TB0EFEA2000D8D4037DF5330B0AE989A191875F03_H
#define VUFORIANULLWRAPPER_TB0EFEA2000D8D4037DF5330B0AE989A191875F03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaNullWrapper
struct  VuforiaNullWrapper_tB0EFEA2000D8D4037DF5330B0AE989A191875F03  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIANULLWRAPPER_TB0EFEA2000D8D4037DF5330B0AE989A191875F03_H
#ifndef U3CU3EC_T0D9CA9C8F7A76A99A794019E01671BDCA16F9609_H
#define U3CU3EC_T0D9CA9C8F7A76A99A794019E01671BDCA16F9609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer_<>c
struct  U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609_StaticFields
{
public:
	// Vuforia.VuforiaRenderer_<>c Vuforia.VuforiaRenderer_<>c::<>9
	U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609 * ___U3CU3E9_0;
	// System.Func`1<System.Boolean> Vuforia.VuforiaRenderer_<>c::<>9__18_0
	Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * ___U3CU3E9__18_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__18_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609_StaticFields, ___U3CU3E9__18_0_1)); }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * get_U3CU3E9__18_0_1() const { return ___U3CU3E9__18_0_1; }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 ** get_address_of_U3CU3E9__18_0_1() { return &___U3CU3E9__18_0_1; }
	inline void set_U3CU3E9__18_0_1(Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * value)
	{
		___U3CU3E9__18_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__18_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T0D9CA9C8F7A76A99A794019E01671BDCA16F9609_H
#ifndef VUFORIAUNITY_T0642B0D82DCCD1F058C8133A227319530273E77F_H
#define VUFORIAUNITY_T0642B0D82DCCD1F058C8133A227319530273E77F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaUnity
struct  VuforiaUnity_t0642B0D82DCCD1F058C8133A227319530273E77F  : public RuntimeObject
{
public:

public:
};

struct VuforiaUnity_t0642B0D82DCCD1F058C8133A227319530273E77F_StaticFields
{
public:
	// Vuforia.IHoloLensApiAbstraction Vuforia.VuforiaUnity::mHoloLensApiAbstraction
	RuntimeObject* ___mHoloLensApiAbstraction_0;
	// System.Boolean Vuforia.VuforiaUnity::mRendererDirty
	bool ___mRendererDirty_2;
	// System.Int32 Vuforia.VuforiaUnity::mWrapperType
	int32_t ___mWrapperType_3;

public:
	inline static int32_t get_offset_of_mHoloLensApiAbstraction_0() { return static_cast<int32_t>(offsetof(VuforiaUnity_t0642B0D82DCCD1F058C8133A227319530273E77F_StaticFields, ___mHoloLensApiAbstraction_0)); }
	inline RuntimeObject* get_mHoloLensApiAbstraction_0() const { return ___mHoloLensApiAbstraction_0; }
	inline RuntimeObject** get_address_of_mHoloLensApiAbstraction_0() { return &___mHoloLensApiAbstraction_0; }
	inline void set_mHoloLensApiAbstraction_0(RuntimeObject* value)
	{
		___mHoloLensApiAbstraction_0 = value;
		Il2CppCodeGenWriteBarrier((&___mHoloLensApiAbstraction_0), value);
	}

	inline static int32_t get_offset_of_mRendererDirty_2() { return static_cast<int32_t>(offsetof(VuforiaUnity_t0642B0D82DCCD1F058C8133A227319530273E77F_StaticFields, ___mRendererDirty_2)); }
	inline bool get_mRendererDirty_2() const { return ___mRendererDirty_2; }
	inline bool* get_address_of_mRendererDirty_2() { return &___mRendererDirty_2; }
	inline void set_mRendererDirty_2(bool value)
	{
		___mRendererDirty_2 = value;
	}

	inline static int32_t get_offset_of_mWrapperType_3() { return static_cast<int32_t>(offsetof(VuforiaUnity_t0642B0D82DCCD1F058C8133A227319530273E77F_StaticFields, ___mWrapperType_3)); }
	inline int32_t get_mWrapperType_3() const { return ___mWrapperType_3; }
	inline int32_t* get_address_of_mWrapperType_3() { return &___mWrapperType_3; }
	inline void set_mWrapperType_3(int32_t value)
	{
		___mWrapperType_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAUNITY_T0642B0D82DCCD1F058C8133A227319530273E77F_H
#ifndef VUFORIAWRAPPER_TDC7A7816CA0A902A65EAE1EEBA8A27D87F94C1FD_H
#define VUFORIAWRAPPER_TDC7A7816CA0A902A65EAE1EEBA8A27D87F94C1FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaWrapper
struct  VuforiaWrapper_tDC7A7816CA0A902A65EAE1EEBA8A27D87F94C1FD  : public RuntimeObject
{
public:

public:
};

struct VuforiaWrapper_tDC7A7816CA0A902A65EAE1EEBA8A27D87F94C1FD_StaticFields
{
public:
	// Vuforia.IVuforiaWrapper Vuforia.VuforiaWrapper::sWrapper
	RuntimeObject* ___sWrapper_0;
	// Vuforia.IVuforiaWrapper Vuforia.VuforiaWrapper::sCamIndependentWrapper
	RuntimeObject* ___sCamIndependentWrapper_1;

public:
	inline static int32_t get_offset_of_sWrapper_0() { return static_cast<int32_t>(offsetof(VuforiaWrapper_tDC7A7816CA0A902A65EAE1EEBA8A27D87F94C1FD_StaticFields, ___sWrapper_0)); }
	inline RuntimeObject* get_sWrapper_0() const { return ___sWrapper_0; }
	inline RuntimeObject** get_address_of_sWrapper_0() { return &___sWrapper_0; }
	inline void set_sWrapper_0(RuntimeObject* value)
	{
		___sWrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&___sWrapper_0), value);
	}

	inline static int32_t get_offset_of_sCamIndependentWrapper_1() { return static_cast<int32_t>(offsetof(VuforiaWrapper_tDC7A7816CA0A902A65EAE1EEBA8A27D87F94C1FD_StaticFields, ___sCamIndependentWrapper_1)); }
	inline RuntimeObject* get_sCamIndependentWrapper_1() const { return ___sCamIndependentWrapper_1; }
	inline RuntimeObject** get_address_of_sCamIndependentWrapper_1() { return &___sCamIndependentWrapper_1; }
	inline void set_sCamIndependentWrapper_1(RuntimeObject* value)
	{
		___sCamIndependentWrapper_1 = value;
		Il2CppCodeGenWriteBarrier((&___sCamIndependentWrapper_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAWRAPPER_TDC7A7816CA0A902A65EAE1EEBA8A27D87F94C1FD_H
#ifndef WEBCAMTEXADAPTOR_T048726FEC9AC07F2D1949310A33517A042E50F5F_H
#define WEBCAMTEXADAPTOR_T048726FEC9AC07F2D1949310A33517A042E50F5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamTexAdaptor
struct  WebCamTexAdaptor_t048726FEC9AC07F2D1949310A33517A042E50F5F  : public RuntimeObject
{
public:
	// UnityEngine.WebCamTexture Vuforia.WebCamTexAdaptor::mWebCamTexture
	WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * ___mWebCamTexture_0;
	// UnityEngine.AsyncOperation Vuforia.WebCamTexAdaptor::mCheckCameraPermissions
	AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * ___mCheckCameraPermissions_1;

public:
	inline static int32_t get_offset_of_mWebCamTexture_0() { return static_cast<int32_t>(offsetof(WebCamTexAdaptor_t048726FEC9AC07F2D1949310A33517A042E50F5F, ___mWebCamTexture_0)); }
	inline WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * get_mWebCamTexture_0() const { return ___mWebCamTexture_0; }
	inline WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 ** get_address_of_mWebCamTexture_0() { return &___mWebCamTexture_0; }
	inline void set_mWebCamTexture_0(WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * value)
	{
		___mWebCamTexture_0 = value;
		Il2CppCodeGenWriteBarrier((&___mWebCamTexture_0), value);
	}

	inline static int32_t get_offset_of_mCheckCameraPermissions_1() { return static_cast<int32_t>(offsetof(WebCamTexAdaptor_t048726FEC9AC07F2D1949310A33517A042E50F5F, ___mCheckCameraPermissions_1)); }
	inline AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * get_mCheckCameraPermissions_1() const { return ___mCheckCameraPermissions_1; }
	inline AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D ** get_address_of_mCheckCameraPermissions_1() { return &___mCheckCameraPermissions_1; }
	inline void set_mCheckCameraPermissions_1(AsyncOperation_t304C51ABED8AE734CC8DDDFE13013D8D5A44641D * value)
	{
		___mCheckCameraPermissions_1 = value;
		Il2CppCodeGenWriteBarrier((&___mCheckCameraPermissions_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCAMTEXADAPTOR_T048726FEC9AC07F2D1949310A33517A042E50F5F_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#define VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifndef VIDEOMODEDATA_T7BC59F654FEA6A276ACE52D08AA89A8438698C3D_H
#define VIDEOMODEDATA_T7BC59F654FEA6A276ACE52D08AA89A8438698C3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice_VideoModeData
#pragma pack(push, tp, 1)
struct  VideoModeData_t7BC59F654FEA6A276ACE52D08AA89A8438698C3D 
{
public:
	// System.Int32 Vuforia.CameraDevice_VideoModeData::width
	int32_t ___width_0;
	// System.Int32 Vuforia.CameraDevice_VideoModeData::height
	int32_t ___height_1;
	// System.Single Vuforia.CameraDevice_VideoModeData::frameRate
	float ___frameRate_2;
	// System.Int32 Vuforia.CameraDevice_VideoModeData::format
	int32_t ___format_3;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(VideoModeData_t7BC59F654FEA6A276ACE52D08AA89A8438698C3D, ___width_0)); }
	inline int32_t get_width_0() const { return ___width_0; }
	inline int32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(int32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(VideoModeData_t7BC59F654FEA6A276ACE52D08AA89A8438698C3D, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_frameRate_2() { return static_cast<int32_t>(offsetof(VideoModeData_t7BC59F654FEA6A276ACE52D08AA89A8438698C3D, ___frameRate_2)); }
	inline float get_frameRate_2() const { return ___frameRate_2; }
	inline float* get_address_of_frameRate_2() { return &___frameRate_2; }
	inline void set_frameRate_2(float value)
	{
		___frameRate_2 = value;
	}

	inline static int32_t get_offset_of_format_3() { return static_cast<int32_t>(offsetof(VideoModeData_t7BC59F654FEA6A276ACE52D08AA89A8438698C3D, ___format_3)); }
	inline int32_t get_format_3() const { return ___format_3; }
	inline int32_t* get_address_of_format_3() { return &___format_3; }
	inline void set_format_3(int32_t value)
	{
		___format_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOMODEDATA_T7BC59F654FEA6A276ACE52D08AA89A8438698C3D_H
#ifndef DATABASELOADARCONTROLLER_T46BD7BB216821464A9EB9C66E30CC19A31FA1C7A_H
#define DATABASELOADARCONTROLLER_T46BD7BB216821464A9EB9C66E30CC19A31FA1C7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DatabaseLoadARController
struct  DatabaseLoadARController_t46BD7BB216821464A9EB9C66E30CC19A31FA1C7A  : public ARController_tBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293
{
public:
	// System.Boolean Vuforia.DatabaseLoadARController::mDatasetsLoaded
	bool ___mDatasetsLoaded_1;
	// System.Collections.Generic.List`1<System.String> Vuforia.DatabaseLoadARController::mExternalDatasetRoots
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___mExternalDatasetRoots_2;
	// System.Collections.Generic.HashSet`1<System.String> Vuforia.DatabaseLoadARController::mDataSetsToLoad
	HashSet_1_tC8214FEC830040D37F12A482FF0284D9C2B9001E * ___mDataSetsToLoad_3;
	// System.Collections.Generic.HashSet`1<System.String> Vuforia.DatabaseLoadARController::mDataSetsToNOTLoad
	HashSet_1_tC8214FEC830040D37F12A482FF0284D9C2B9001E * ___mDataSetsToNOTLoad_4;
	// System.Collections.Generic.HashSet`1<System.String> Vuforia.DatabaseLoadARController::mDataSetsToActivate
	HashSet_1_tC8214FEC830040D37F12A482FF0284D9C2B9001E * ___mDataSetsToActivate_5;

public:
	inline static int32_t get_offset_of_mDatasetsLoaded_1() { return static_cast<int32_t>(offsetof(DatabaseLoadARController_t46BD7BB216821464A9EB9C66E30CC19A31FA1C7A, ___mDatasetsLoaded_1)); }
	inline bool get_mDatasetsLoaded_1() const { return ___mDatasetsLoaded_1; }
	inline bool* get_address_of_mDatasetsLoaded_1() { return &___mDatasetsLoaded_1; }
	inline void set_mDatasetsLoaded_1(bool value)
	{
		___mDatasetsLoaded_1 = value;
	}

	inline static int32_t get_offset_of_mExternalDatasetRoots_2() { return static_cast<int32_t>(offsetof(DatabaseLoadARController_t46BD7BB216821464A9EB9C66E30CC19A31FA1C7A, ___mExternalDatasetRoots_2)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_mExternalDatasetRoots_2() const { return ___mExternalDatasetRoots_2; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_mExternalDatasetRoots_2() { return &___mExternalDatasetRoots_2; }
	inline void set_mExternalDatasetRoots_2(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___mExternalDatasetRoots_2 = value;
		Il2CppCodeGenWriteBarrier((&___mExternalDatasetRoots_2), value);
	}

	inline static int32_t get_offset_of_mDataSetsToLoad_3() { return static_cast<int32_t>(offsetof(DatabaseLoadARController_t46BD7BB216821464A9EB9C66E30CC19A31FA1C7A, ___mDataSetsToLoad_3)); }
	inline HashSet_1_tC8214FEC830040D37F12A482FF0284D9C2B9001E * get_mDataSetsToLoad_3() const { return ___mDataSetsToLoad_3; }
	inline HashSet_1_tC8214FEC830040D37F12A482FF0284D9C2B9001E ** get_address_of_mDataSetsToLoad_3() { return &___mDataSetsToLoad_3; }
	inline void set_mDataSetsToLoad_3(HashSet_1_tC8214FEC830040D37F12A482FF0284D9C2B9001E * value)
	{
		___mDataSetsToLoad_3 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSetsToLoad_3), value);
	}

	inline static int32_t get_offset_of_mDataSetsToNOTLoad_4() { return static_cast<int32_t>(offsetof(DatabaseLoadARController_t46BD7BB216821464A9EB9C66E30CC19A31FA1C7A, ___mDataSetsToNOTLoad_4)); }
	inline HashSet_1_tC8214FEC830040D37F12A482FF0284D9C2B9001E * get_mDataSetsToNOTLoad_4() const { return ___mDataSetsToNOTLoad_4; }
	inline HashSet_1_tC8214FEC830040D37F12A482FF0284D9C2B9001E ** get_address_of_mDataSetsToNOTLoad_4() { return &___mDataSetsToNOTLoad_4; }
	inline void set_mDataSetsToNOTLoad_4(HashSet_1_tC8214FEC830040D37F12A482FF0284D9C2B9001E * value)
	{
		___mDataSetsToNOTLoad_4 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSetsToNOTLoad_4), value);
	}

	inline static int32_t get_offset_of_mDataSetsToActivate_5() { return static_cast<int32_t>(offsetof(DatabaseLoadARController_t46BD7BB216821464A9EB9C66E30CC19A31FA1C7A, ___mDataSetsToActivate_5)); }
	inline HashSet_1_tC8214FEC830040D37F12A482FF0284D9C2B9001E * get_mDataSetsToActivate_5() const { return ___mDataSetsToActivate_5; }
	inline HashSet_1_tC8214FEC830040D37F12A482FF0284D9C2B9001E ** get_address_of_mDataSetsToActivate_5() { return &___mDataSetsToActivate_5; }
	inline void set_mDataSetsToActivate_5(HashSet_1_tC8214FEC830040D37F12A482FF0284D9C2B9001E * value)
	{
		___mDataSetsToActivate_5 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSetsToActivate_5), value);
	}
};

struct DatabaseLoadARController_t46BD7BB216821464A9EB9C66E30CC19A31FA1C7A_StaticFields
{
public:
	// Vuforia.DatabaseLoadARController Vuforia.DatabaseLoadARController::mInstance
	DatabaseLoadARController_t46BD7BB216821464A9EB9C66E30CC19A31FA1C7A * ___mInstance_6;
	// System.Object Vuforia.DatabaseLoadARController::mPadlock
	RuntimeObject * ___mPadlock_7;

public:
	inline static int32_t get_offset_of_mInstance_6() { return static_cast<int32_t>(offsetof(DatabaseLoadARController_t46BD7BB216821464A9EB9C66E30CC19A31FA1C7A_StaticFields, ___mInstance_6)); }
	inline DatabaseLoadARController_t46BD7BB216821464A9EB9C66E30CC19A31FA1C7A * get_mInstance_6() const { return ___mInstance_6; }
	inline DatabaseLoadARController_t46BD7BB216821464A9EB9C66E30CC19A31FA1C7A ** get_address_of_mInstance_6() { return &___mInstance_6; }
	inline void set_mInstance_6(DatabaseLoadARController_t46BD7BB216821464A9EB9C66E30CC19A31FA1C7A * value)
	{
		___mInstance_6 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_6), value);
	}

	inline static int32_t get_offset_of_mPadlock_7() { return static_cast<int32_t>(offsetof(DatabaseLoadARController_t46BD7BB216821464A9EB9C66E30CC19A31FA1C7A_StaticFields, ___mPadlock_7)); }
	inline RuntimeObject * get_mPadlock_7() const { return ___mPadlock_7; }
	inline RuntimeObject ** get_address_of_mPadlock_7() { return &___mPadlock_7; }
	inline void set_mPadlock_7(RuntimeObject * value)
	{
		___mPadlock_7 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATABASELOADARCONTROLLER_T46BD7BB216821464A9EB9C66E30CC19A31FA1C7A_H
#ifndef OBJECTTARGETIMPL_TEDDDACB8EF83100B0896957801DF1F85DB6A126A_H
#define OBJECTTARGETIMPL_TEDDDACB8EF83100B0896957801DF1F85DB6A126A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ObjectTargetImpl
struct  ObjectTargetImpl_tEDDDACB8EF83100B0896957801DF1F85DB6A126A  : public TrackableImpl_t97E7D175DE38822AF55FC03FCA26FB62B7E290A2
{
public:
	// Vuforia.ITargetSize Vuforia.ObjectTargetImpl::mTargetSizeImpl
	RuntimeObject* ___mTargetSizeImpl_2;

public:
	inline static int32_t get_offset_of_mTargetSizeImpl_2() { return static_cast<int32_t>(offsetof(ObjectTargetImpl_tEDDDACB8EF83100B0896957801DF1F85DB6A126A, ___mTargetSizeImpl_2)); }
	inline RuntimeObject* get_mTargetSizeImpl_2() const { return ___mTargetSizeImpl_2; }
	inline RuntimeObject** get_address_of_mTargetSizeImpl_2() { return &___mTargetSizeImpl_2; }
	inline void set_mTargetSizeImpl_2(RuntimeObject* value)
	{
		___mTargetSizeImpl_2 = value;
		Il2CppCodeGenWriteBarrier((&___mTargetSizeImpl_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTARGETIMPL_TEDDDACB8EF83100B0896957801DF1F85DB6A126A_H
#ifndef OBJECTTRACKER_TC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E_H
#define OBJECTTRACKER_TC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ObjectTracker
struct  ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E  : public Tracker_t11C8E7B84615512E8125186CDC5DF90D9D7B58F1
{
public:
	// System.Collections.Generic.List`1<Vuforia.DataSet> Vuforia.ObjectTracker::mActiveDataSets
	List_1_t03C9CEF50E5A7C36B2BC4CD68C627FA6080A1BF9 * ___mActiveDataSets_1;
	// System.Collections.Generic.List`1<Vuforia.DataSet> Vuforia.ObjectTracker::mDataSets
	List_1_t03C9CEF50E5A7C36B2BC4CD68C627FA6080A1BF9 * ___mDataSets_2;
	// System.Collections.Generic.Dictionary`2<System.Type,Vuforia.TargetFinder> Vuforia.ObjectTracker::mTargetFinders
	Dictionary_2_t97145FAA8A62E3A6895B2E7A97B7BB62B2F6B972 * ___mTargetFinders_3;
	// Vuforia.ImageTargetBuilder Vuforia.ObjectTracker::mImageTargetBuilder
	ImageTargetBuilder_t5BA66A134696E24A591FA066087BAABE66F00755 * ___mImageTargetBuilder_4;

public:
	inline static int32_t get_offset_of_mActiveDataSets_1() { return static_cast<int32_t>(offsetof(ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E, ___mActiveDataSets_1)); }
	inline List_1_t03C9CEF50E5A7C36B2BC4CD68C627FA6080A1BF9 * get_mActiveDataSets_1() const { return ___mActiveDataSets_1; }
	inline List_1_t03C9CEF50E5A7C36B2BC4CD68C627FA6080A1BF9 ** get_address_of_mActiveDataSets_1() { return &___mActiveDataSets_1; }
	inline void set_mActiveDataSets_1(List_1_t03C9CEF50E5A7C36B2BC4CD68C627FA6080A1BF9 * value)
	{
		___mActiveDataSets_1 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveDataSets_1), value);
	}

	inline static int32_t get_offset_of_mDataSets_2() { return static_cast<int32_t>(offsetof(ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E, ___mDataSets_2)); }
	inline List_1_t03C9CEF50E5A7C36B2BC4CD68C627FA6080A1BF9 * get_mDataSets_2() const { return ___mDataSets_2; }
	inline List_1_t03C9CEF50E5A7C36B2BC4CD68C627FA6080A1BF9 ** get_address_of_mDataSets_2() { return &___mDataSets_2; }
	inline void set_mDataSets_2(List_1_t03C9CEF50E5A7C36B2BC4CD68C627FA6080A1BF9 * value)
	{
		___mDataSets_2 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSets_2), value);
	}

	inline static int32_t get_offset_of_mTargetFinders_3() { return static_cast<int32_t>(offsetof(ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E, ___mTargetFinders_3)); }
	inline Dictionary_2_t97145FAA8A62E3A6895B2E7A97B7BB62B2F6B972 * get_mTargetFinders_3() const { return ___mTargetFinders_3; }
	inline Dictionary_2_t97145FAA8A62E3A6895B2E7A97B7BB62B2F6B972 ** get_address_of_mTargetFinders_3() { return &___mTargetFinders_3; }
	inline void set_mTargetFinders_3(Dictionary_2_t97145FAA8A62E3A6895B2E7A97B7BB62B2F6B972 * value)
	{
		___mTargetFinders_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTargetFinders_3), value);
	}

	inline static int32_t get_offset_of_mImageTargetBuilder_4() { return static_cast<int32_t>(offsetof(ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E, ___mImageTargetBuilder_4)); }
	inline ImageTargetBuilder_t5BA66A134696E24A591FA066087BAABE66F00755 * get_mImageTargetBuilder_4() const { return ___mImageTargetBuilder_4; }
	inline ImageTargetBuilder_t5BA66A134696E24A591FA066087BAABE66F00755 ** get_address_of_mImageTargetBuilder_4() { return &___mImageTargetBuilder_4; }
	inline void set_mImageTargetBuilder_4(ImageTargetBuilder_t5BA66A134696E24A591FA066087BAABE66F00755 * value)
	{
		___mImageTargetBuilder_4 = value;
		Il2CppCodeGenWriteBarrier((&___mImageTargetBuilder_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTRACKER_TC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E_H
#ifndef RECTANGLEDATA_TA9B66ACDEB48962ADACFB8D18129ED016D732BE9_H
#define RECTANGLEDATA_TA9B66ACDEB48962ADACFB8D18129ED016D732BE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.RectangleData
#pragma pack(push, tp, 1)
struct  RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9 
{
public:
	// System.Single Vuforia.RectangleData::leftTopX
	float ___leftTopX_0;
	// System.Single Vuforia.RectangleData::leftTopY
	float ___leftTopY_1;
	// System.Single Vuforia.RectangleData::rightBottomX
	float ___rightBottomX_2;
	// System.Single Vuforia.RectangleData::rightBottomY
	float ___rightBottomY_3;

public:
	inline static int32_t get_offset_of_leftTopX_0() { return static_cast<int32_t>(offsetof(RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9, ___leftTopX_0)); }
	inline float get_leftTopX_0() const { return ___leftTopX_0; }
	inline float* get_address_of_leftTopX_0() { return &___leftTopX_0; }
	inline void set_leftTopX_0(float value)
	{
		___leftTopX_0 = value;
	}

	inline static int32_t get_offset_of_leftTopY_1() { return static_cast<int32_t>(offsetof(RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9, ___leftTopY_1)); }
	inline float get_leftTopY_1() const { return ___leftTopY_1; }
	inline float* get_address_of_leftTopY_1() { return &___leftTopY_1; }
	inline void set_leftTopY_1(float value)
	{
		___leftTopY_1 = value;
	}

	inline static int32_t get_offset_of_rightBottomX_2() { return static_cast<int32_t>(offsetof(RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9, ___rightBottomX_2)); }
	inline float get_rightBottomX_2() const { return ___rightBottomX_2; }
	inline float* get_address_of_rightBottomX_2() { return &___rightBottomX_2; }
	inline void set_rightBottomX_2(float value)
	{
		___rightBottomX_2 = value;
	}

	inline static int32_t get_offset_of_rightBottomY_3() { return static_cast<int32_t>(offsetof(RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9, ___rightBottomY_3)); }
	inline float get_rightBottomY_3() const { return ___rightBottomY_3; }
	inline float* get_address_of_rightBottomY_3() { return &___rightBottomY_3; }
	inline void set_rightBottomY_3(float value)
	{
		___rightBottomY_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTANGLEDATA_TA9B66ACDEB48962ADACFB8D18129ED016D732BE9_H
#ifndef RECTANGLEINTDATA_T3E3BC17A4EC0DBA1E1206EB4E9BE76FC2D012553_H
#define RECTANGLEINTDATA_T3E3BC17A4EC0DBA1E1206EB4E9BE76FC2D012553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.RectangleIntData
#pragma pack(push, tp, 1)
struct  RectangleIntData_t3E3BC17A4EC0DBA1E1206EB4E9BE76FC2D012553 
{
public:
	// System.Int32 Vuforia.RectangleIntData::leftTopX
	int32_t ___leftTopX_0;
	// System.Int32 Vuforia.RectangleIntData::leftTopY
	int32_t ___leftTopY_1;
	// System.Int32 Vuforia.RectangleIntData::rightBottomX
	int32_t ___rightBottomX_2;
	// System.Int32 Vuforia.RectangleIntData::rightBottomY
	int32_t ___rightBottomY_3;

public:
	inline static int32_t get_offset_of_leftTopX_0() { return static_cast<int32_t>(offsetof(RectangleIntData_t3E3BC17A4EC0DBA1E1206EB4E9BE76FC2D012553, ___leftTopX_0)); }
	inline int32_t get_leftTopX_0() const { return ___leftTopX_0; }
	inline int32_t* get_address_of_leftTopX_0() { return &___leftTopX_0; }
	inline void set_leftTopX_0(int32_t value)
	{
		___leftTopX_0 = value;
	}

	inline static int32_t get_offset_of_leftTopY_1() { return static_cast<int32_t>(offsetof(RectangleIntData_t3E3BC17A4EC0DBA1E1206EB4E9BE76FC2D012553, ___leftTopY_1)); }
	inline int32_t get_leftTopY_1() const { return ___leftTopY_1; }
	inline int32_t* get_address_of_leftTopY_1() { return &___leftTopY_1; }
	inline void set_leftTopY_1(int32_t value)
	{
		___leftTopY_1 = value;
	}

	inline static int32_t get_offset_of_rightBottomX_2() { return static_cast<int32_t>(offsetof(RectangleIntData_t3E3BC17A4EC0DBA1E1206EB4E9BE76FC2D012553, ___rightBottomX_2)); }
	inline int32_t get_rightBottomX_2() const { return ___rightBottomX_2; }
	inline int32_t* get_address_of_rightBottomX_2() { return &___rightBottomX_2; }
	inline void set_rightBottomX_2(int32_t value)
	{
		___rightBottomX_2 = value;
	}

	inline static int32_t get_offset_of_rightBottomY_3() { return static_cast<int32_t>(offsetof(RectangleIntData_t3E3BC17A4EC0DBA1E1206EB4E9BE76FC2D012553, ___rightBottomY_3)); }
	inline int32_t get_rightBottomY_3() const { return ___rightBottomY_3; }
	inline int32_t* get_address_of_rightBottomY_3() { return &___rightBottomY_3; }
	inline void set_rightBottomY_3(int32_t value)
	{
		___rightBottomY_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTANGLEINTDATA_T3E3BC17A4EC0DBA1E1206EB4E9BE76FC2D012553_H
#ifndef SMARTTERRAINCONFIGURATION_T6104CF56BC1AFE13ED509BB01CBE90E7E64372C7_H
#define SMARTTERRAINCONFIGURATION_T6104CF56BC1AFE13ED509BB01CBE90E7E64372C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaConfiguration_SmartTerrainConfiguration
struct  SmartTerrainConfiguration_t6104CF56BC1AFE13ED509BB01CBE90E7E64372C7  : public TrackerConfiguration_t15942AA3CED7034F69729C6FC20AFCD7C078670A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTTERRAINCONFIGURATION_T6104CF56BC1AFE13ED509BB01CBE90E7E64372C7_H
#ifndef VUFORIAMACROS_TED85BB64AE7B78C234CDEEF78193EBFD4369CE2D_H
#define VUFORIAMACROS_TED85BB64AE7B78C234CDEEF78193EBFD4369CE2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaMacros
struct  VuforiaMacros_tED85BB64AE7B78C234CDEEF78193EBFD4369CE2D 
{
public:
	union
	{
		struct
		{
		};
		uint8_t VuforiaMacros_tED85BB64AE7B78C234CDEEF78193EBFD4369CE2D__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMACROS_TED85BB64AE7B78C234CDEEF78193EBFD4369CE2D_H
#ifndef AUTOROTATIONSTATE_T7763EA1A2607ECEF69E2D2438C471FE93451E508_H
#define AUTOROTATIONSTATE_T7763EA1A2607ECEF69E2D2438C471FE93451E508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManager_AutoRotationState
struct  AutoRotationState_t7763EA1A2607ECEF69E2D2438C471FE93451E508 
{
public:
	// System.Boolean Vuforia.VuforiaManager_AutoRotationState::setOnPause
	bool ___setOnPause_0;
	// System.Boolean Vuforia.VuforiaManager_AutoRotationState::autorotateToPortrait
	bool ___autorotateToPortrait_1;
	// System.Boolean Vuforia.VuforiaManager_AutoRotationState::autorotateToPortraitUpsideDown
	bool ___autorotateToPortraitUpsideDown_2;
	// System.Boolean Vuforia.VuforiaManager_AutoRotationState::autorotateToLandscapeLeft
	bool ___autorotateToLandscapeLeft_3;
	// System.Boolean Vuforia.VuforiaManager_AutoRotationState::autorotateToLandscapeRight
	bool ___autorotateToLandscapeRight_4;

public:
	inline static int32_t get_offset_of_setOnPause_0() { return static_cast<int32_t>(offsetof(AutoRotationState_t7763EA1A2607ECEF69E2D2438C471FE93451E508, ___setOnPause_0)); }
	inline bool get_setOnPause_0() const { return ___setOnPause_0; }
	inline bool* get_address_of_setOnPause_0() { return &___setOnPause_0; }
	inline void set_setOnPause_0(bool value)
	{
		___setOnPause_0 = value;
	}

	inline static int32_t get_offset_of_autorotateToPortrait_1() { return static_cast<int32_t>(offsetof(AutoRotationState_t7763EA1A2607ECEF69E2D2438C471FE93451E508, ___autorotateToPortrait_1)); }
	inline bool get_autorotateToPortrait_1() const { return ___autorotateToPortrait_1; }
	inline bool* get_address_of_autorotateToPortrait_1() { return &___autorotateToPortrait_1; }
	inline void set_autorotateToPortrait_1(bool value)
	{
		___autorotateToPortrait_1 = value;
	}

	inline static int32_t get_offset_of_autorotateToPortraitUpsideDown_2() { return static_cast<int32_t>(offsetof(AutoRotationState_t7763EA1A2607ECEF69E2D2438C471FE93451E508, ___autorotateToPortraitUpsideDown_2)); }
	inline bool get_autorotateToPortraitUpsideDown_2() const { return ___autorotateToPortraitUpsideDown_2; }
	inline bool* get_address_of_autorotateToPortraitUpsideDown_2() { return &___autorotateToPortraitUpsideDown_2; }
	inline void set_autorotateToPortraitUpsideDown_2(bool value)
	{
		___autorotateToPortraitUpsideDown_2 = value;
	}

	inline static int32_t get_offset_of_autorotateToLandscapeLeft_3() { return static_cast<int32_t>(offsetof(AutoRotationState_t7763EA1A2607ECEF69E2D2438C471FE93451E508, ___autorotateToLandscapeLeft_3)); }
	inline bool get_autorotateToLandscapeLeft_3() const { return ___autorotateToLandscapeLeft_3; }
	inline bool* get_address_of_autorotateToLandscapeLeft_3() { return &___autorotateToLandscapeLeft_3; }
	inline void set_autorotateToLandscapeLeft_3(bool value)
	{
		___autorotateToLandscapeLeft_3 = value;
	}

	inline static int32_t get_offset_of_autorotateToLandscapeRight_4() { return static_cast<int32_t>(offsetof(AutoRotationState_t7763EA1A2607ECEF69E2D2438C471FE93451E508, ___autorotateToLandscapeRight_4)); }
	inline bool get_autorotateToLandscapeRight_4() const { return ___autorotateToLandscapeRight_4; }
	inline bool* get_address_of_autorotateToLandscapeRight_4() { return &___autorotateToLandscapeRight_4; }
	inline void set_autorotateToLandscapeRight_4(bool value)
	{
		___autorotateToLandscapeRight_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Vuforia.VuforiaManager/AutoRotationState
struct AutoRotationState_t7763EA1A2607ECEF69E2D2438C471FE93451E508_marshaled_pinvoke
{
	int32_t ___setOnPause_0;
	int32_t ___autorotateToPortrait_1;
	int32_t ___autorotateToPortraitUpsideDown_2;
	int32_t ___autorotateToLandscapeLeft_3;
	int32_t ___autorotateToLandscapeRight_4;
};
// Native definition for COM marshalling of Vuforia.VuforiaManager/AutoRotationState
struct AutoRotationState_t7763EA1A2607ECEF69E2D2438C471FE93451E508_marshaled_com
{
	int32_t ___setOnPause_0;
	int32_t ___autorotateToPortrait_1;
	int32_t ___autorotateToPortraitUpsideDown_2;
	int32_t ___autorotateToLandscapeLeft_3;
	int32_t ___autorotateToLandscapeRight_4;
};
#endif // AUTOROTATIONSTATE_T7763EA1A2607ECEF69E2D2438C471FE93451E508_H
#ifndef TRACKABLEIDPAIR_T0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F_H
#define TRACKABLEIDPAIR_T0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManager_TrackableIdPair
struct  TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F 
{
public:
	// System.Int32 Vuforia.VuforiaManager_TrackableIdPair::TrackableId
	int32_t ___TrackableId_0;
	// System.Int32 Vuforia.VuforiaManager_TrackableIdPair::ResultId
	int32_t ___ResultId_1;

public:
	inline static int32_t get_offset_of_TrackableId_0() { return static_cast<int32_t>(offsetof(TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F, ___TrackableId_0)); }
	inline int32_t get_TrackableId_0() const { return ___TrackableId_0; }
	inline int32_t* get_address_of_TrackableId_0() { return &___TrackableId_0; }
	inline void set_TrackableId_0(int32_t value)
	{
		___TrackableId_0 = value;
	}

	inline static int32_t get_offset_of_ResultId_1() { return static_cast<int32_t>(offsetof(TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F, ___ResultId_1)); }
	inline int32_t get_ResultId_1() const { return ___ResultId_1; }
	inline int32_t* get_address_of_ResultId_1() { return &___ResultId_1; }
	inline void set_ResultId_1(int32_t value)
	{
		___ResultId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEIDPAIR_T0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F_H
#ifndef VEC2I_T42F341F398E3117176C37CAAAF2D29943EA9FCF5_H
#define VEC2I_T42F341F398E3117176C37CAAAF2D29943EA9FCF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer_Vec2I
#pragma pack(push, tp, 1)
struct  Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer_Vec2I::x
	int32_t ___x_0;
	// System.Int32 Vuforia.VuforiaRenderer_Vec2I::y
	int32_t ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VEC2I_T42F341F398E3117176C37CAAAF2D29943EA9FCF5_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef CAMERADEVICEMODE_T31CE15C1D60CED5FC63DF3962D53D5DAADD40589_H
#define CAMERADEVICEMODE_T31CE15C1D60CED5FC63DF3962D53D5DAADD40589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice_CameraDeviceMode
struct  CameraDeviceMode_t31CE15C1D60CED5FC63DF3962D53D5DAADD40589 
{
public:
	// System.Int32 Vuforia.CameraDevice_CameraDeviceMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraDeviceMode_t31CE15C1D60CED5FC63DF3962D53D5DAADD40589, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADEVICEMODE_T31CE15C1D60CED5FC63DF3962D53D5DAADD40589_H
#ifndef CLOUDRECOIMAGETARGETIMPL_TE3588C83AF4E12EC1A5CC46A20581E58DB56D797_H
#define CLOUDRECOIMAGETARGETIMPL_TE3588C83AF4E12EC1A5CC46A20581E58DB56D797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CloudRecoImageTargetImpl
struct  CloudRecoImageTargetImpl_tE3588C83AF4E12EC1A5CC46A20581E58DB56D797  : public ObjectTargetImpl_tEDDDACB8EF83100B0896957801DF1F85DB6A126A
{
public:
	// UnityEngine.Vector3 Vuforia.CloudRecoImageTargetImpl::mSize
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mSize_3;

public:
	inline static int32_t get_offset_of_mSize_3() { return static_cast<int32_t>(offsetof(CloudRecoImageTargetImpl_tE3588C83AF4E12EC1A5CC46A20581E58DB56D797, ___mSize_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mSize_3() const { return ___mSize_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mSize_3() { return &___mSize_3; }
	inline void set_mSize_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mSize_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDRECOIMAGETARGETIMPL_TE3588C83AF4E12EC1A5CC46A20581E58DB56D797_H
#ifndef STORAGETYPE_T42B154A18B4AF9C010AA731DB709FC5EABF5D1D8_H
#define STORAGETYPE_T42B154A18B4AF9C010AA731DB709FC5EABF5D1D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DataSet_StorageType
struct  StorageType_t42B154A18B4AF9C010AA731DB709FC5EABF5D1D8 
{
public:
	// System.Int32 Vuforia.DataSet_StorageType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StorageType_t42B154A18B4AF9C010AA731DB709FC5EABF5D1D8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORAGETYPE_T42B154A18B4AF9C010AA731DB709FC5EABF5D1D8_H
#ifndef DATASETOBJECTTARGETIMPL_TC2D896B84A1FDE730F7E2F20045D12CBC6F30C98_H
#define DATASETOBJECTTARGETIMPL_TC2D896B84A1FDE730F7E2F20045D12CBC6F30C98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DataSetObjectTargetImpl
struct  DataSetObjectTargetImpl_tC2D896B84A1FDE730F7E2F20045D12CBC6F30C98  : public ObjectTargetImpl_tEDDDACB8EF83100B0896957801DF1F85DB6A126A
{
public:
	// Vuforia.DataSet Vuforia.DataSetObjectTargetImpl::mDataSet
	DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644 * ___mDataSet_3;

public:
	inline static int32_t get_offset_of_mDataSet_3() { return static_cast<int32_t>(offsetof(DataSetObjectTargetImpl_tC2D896B84A1FDE730F7E2F20045D12CBC6F30C98, ___mDataSet_3)); }
	inline DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644 * get_mDataSet_3() const { return ___mDataSet_3; }
	inline DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644 ** get_address_of_mDataSet_3() { return &___mDataSet_3; }
	inline void set_mDataSet_3(DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644 * value)
	{
		___mDataSet_3 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSet_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASETOBJECTTARGETIMPL_TC2D896B84A1FDE730F7E2F20045D12CBC6F30C98_H
#ifndef DEFAULTWEBCAMTEXADAPTOR_T76E5C24E8EBA6DCA906BF5C289D7C19115FEFF8D_H
#define DEFAULTWEBCAMTEXADAPTOR_T76E5C24E8EBA6DCA906BF5C289D7C19115FEFF8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DefaultWebCamTexAdaptor
struct  DefaultWebCamTexAdaptor_t76E5C24E8EBA6DCA906BF5C289D7C19115FEFF8D  : public RuntimeObject
{
public:
	// UnityEngine.Texture2D Vuforia.DefaultWebCamTexAdaptor::mTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___mTexture_0;
	// System.Boolean Vuforia.DefaultWebCamTexAdaptor::mPseudoPlaying
	bool ___mPseudoPlaying_1;
	// System.Double Vuforia.DefaultWebCamTexAdaptor::mMsBetweenFrames
	double ___mMsBetweenFrames_2;
	// System.DateTime Vuforia.DefaultWebCamTexAdaptor::mLastFrame
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___mLastFrame_3;

public:
	inline static int32_t get_offset_of_mTexture_0() { return static_cast<int32_t>(offsetof(DefaultWebCamTexAdaptor_t76E5C24E8EBA6DCA906BF5C289D7C19115FEFF8D, ___mTexture_0)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_mTexture_0() const { return ___mTexture_0; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_mTexture_0() { return &___mTexture_0; }
	inline void set_mTexture_0(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___mTexture_0 = value;
		Il2CppCodeGenWriteBarrier((&___mTexture_0), value);
	}

	inline static int32_t get_offset_of_mPseudoPlaying_1() { return static_cast<int32_t>(offsetof(DefaultWebCamTexAdaptor_t76E5C24E8EBA6DCA906BF5C289D7C19115FEFF8D, ___mPseudoPlaying_1)); }
	inline bool get_mPseudoPlaying_1() const { return ___mPseudoPlaying_1; }
	inline bool* get_address_of_mPseudoPlaying_1() { return &___mPseudoPlaying_1; }
	inline void set_mPseudoPlaying_1(bool value)
	{
		___mPseudoPlaying_1 = value;
	}

	inline static int32_t get_offset_of_mMsBetweenFrames_2() { return static_cast<int32_t>(offsetof(DefaultWebCamTexAdaptor_t76E5C24E8EBA6DCA906BF5C289D7C19115FEFF8D, ___mMsBetweenFrames_2)); }
	inline double get_mMsBetweenFrames_2() const { return ___mMsBetweenFrames_2; }
	inline double* get_address_of_mMsBetweenFrames_2() { return &___mMsBetweenFrames_2; }
	inline void set_mMsBetweenFrames_2(double value)
	{
		___mMsBetweenFrames_2 = value;
	}

	inline static int32_t get_offset_of_mLastFrame_3() { return static_cast<int32_t>(offsetof(DefaultWebCamTexAdaptor_t76E5C24E8EBA6DCA906BF5C289D7C19115FEFF8D, ___mLastFrame_3)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_mLastFrame_3() const { return ___mLastFrame_3; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_mLastFrame_3() { return &___mLastFrame_3; }
	inline void set_mLastFrame_3(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___mLastFrame_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTWEBCAMTEXADAPTOR_T76E5C24E8EBA6DCA906BF5C289D7C19115FEFF8D_H
#ifndef TRACKING_MODE_T2ABEC66C8561190920E2F2637F28B37437D2E5E0_H
#define TRACKING_MODE_T2ABEC66C8561190920E2F2637F28B37437D2E5E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DeviceTracker_TRACKING_MODE
struct  TRACKING_MODE_t2ABEC66C8561190920E2F2637F28B37437D2E5E0 
{
public:
	// System.Int32 Vuforia.DeviceTracker_TRACKING_MODE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TRACKING_MODE_t2ABEC66C8561190920E2F2637F28B37437D2E5E0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKING_MODE_T2ABEC66C8561190920E2F2637F28B37437D2E5E0_H
#ifndef EYEWEARTYPE_T0DF90F97DCCF9F068455C2AF59B1758E89615C13_H
#define EYEWEARTYPE_T0DF90F97DCCF9F068455C2AF59B1758E89615C13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController_EyewearType
struct  EyewearType_t0DF90F97DCCF9F068455C2AF59B1758E89615C13 
{
public:
	// System.Int32 Vuforia.DigitalEyewearARController_EyewearType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EyewearType_t0DF90F97DCCF9F068455C2AF59B1758E89615C13, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEWEARTYPE_T0DF90F97DCCF9F068455C2AF59B1758E89615C13_H
#ifndef SEETHROUGHCONFIGURATION_TF34F5E33DC51F5EEB23599A905DD112FF00C8E34_H
#define SEETHROUGHCONFIGURATION_TF34F5E33DC51F5EEB23599A905DD112FF00C8E34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController_SeeThroughConfiguration
struct  SeeThroughConfiguration_tF34F5E33DC51F5EEB23599A905DD112FF00C8E34 
{
public:
	// System.Int32 Vuforia.DigitalEyewearARController_SeeThroughConfiguration::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SeeThroughConfiguration_tF34F5E33DC51F5EEB23599A905DD112FF00C8E34, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEETHROUGHCONFIGURATION_TF34F5E33DC51F5EEB23599A905DD112FF00C8E34_H
#ifndef STEREOFRAMEWORK_T95A56F9A03F0EBAFFC34ABBE8309C936859E13BA_H
#define STEREOFRAMEWORK_T95A56F9A03F0EBAFFC34ABBE8309C936859E13BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController_StereoFramework
struct  StereoFramework_t95A56F9A03F0EBAFFC34ABBE8309C936859E13BA 
{
public:
	// System.Int32 Vuforia.DigitalEyewearARController_StereoFramework::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StereoFramework_t95A56F9A03F0EBAFFC34ABBE8309C936859E13BA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEREOFRAMEWORK_T95A56F9A03F0EBAFFC34ABBE8309C936859E13BA_H
#ifndef DISABLEDSETTARGETSIZE_TED3AEC3CD7D4DA45A1D88625566B3D8E9141B0DA_H
#define DISABLEDSETTARGETSIZE_TED3AEC3CD7D4DA45A1D88625566B3D8E9141B0DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DisabledSetTargetSize
struct  DisabledSetTargetSize_tED3AEC3CD7D4DA45A1D88625566B3D8E9141B0DA  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Vuforia.DisabledSetTargetSize::mSize
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mSize_0;

public:
	inline static int32_t get_offset_of_mSize_0() { return static_cast<int32_t>(offsetof(DisabledSetTargetSize_tED3AEC3CD7D4DA45A1D88625566B3D8E9141B0DA, ___mSize_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mSize_0() const { return ___mSize_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mSize_0() { return &___mSize_0; }
	inline void set_mSize_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mSize_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISABLEDSETTARGETSIZE_TED3AEC3CD7D4DA45A1D88625566B3D8E9141B0DA_H
#ifndef CLIPPING_MODE_T74517F84F38E21AC75F4DB3A1D76698EA0D2580D_H
#define CLIPPING_MODE_T74517F84F38E21AC75F4DB3A1D76698EA0D2580D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HideExcessAreaUtility_CLIPPING_MODE
struct  CLIPPING_MODE_t74517F84F38E21AC75F4DB3A1D76698EA0D2580D 
{
public:
	// System.Int32 Vuforia.HideExcessAreaUtility_CLIPPING_MODE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CLIPPING_MODE_t74517F84F38E21AC75F4DB3A1D76698EA0D2580D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPING_MODE_T74517F84F38E21AC75F4DB3A1D76698EA0D2580D_H
#ifndef FRAMEQUALITY_TEA002835E0F464BB9506076B691826DAAFEB87FB_H
#define FRAMEQUALITY_TEA002835E0F464BB9506076B691826DAAFEB87FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetBuilder_FrameQuality
struct  FrameQuality_tEA002835E0F464BB9506076B691826DAAFEB87FB 
{
public:
	// System.Int32 Vuforia.ImageTargetBuilder_FrameQuality::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FrameQuality_tEA002835E0F464BB9506076B691826DAAFEB87FB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEQUALITY_TEA002835E0F464BB9506076B691826DAAFEB87FB_H
#ifndef IMAGETARGETDATA_T5C57B09E08494327B42B9DDE3E2D0F1433E0A198_H
#define IMAGETARGETDATA_T5C57B09E08494327B42B9DDE3E2D0F1433E0A198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetData
#pragma pack(push, tp, 1)
struct  ImageTargetData_t5C57B09E08494327B42B9DDE3E2D0F1433E0A198 
{
public:
	// System.Int32 Vuforia.ImageTargetData::id
	int32_t ___id_0;
	// UnityEngine.Vector3 Vuforia.ImageTargetData::size
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___size_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ImageTargetData_t5C57B09E08494327B42B9DDE3E2D0F1433E0A198, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(ImageTargetData_t5C57B09E08494327B42B9DDE3E2D0F1433E0A198, ___size_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_size_1() const { return ___size_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___size_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETDATA_T5C57B09E08494327B42B9DDE3E2D0F1433E0A198_H
#ifndef IMAGETARGETTYPE_T3A1F68B5507799B9B73A9806FA199BF2F3A819D6_H
#define IMAGETARGETTYPE_T3A1F68B5507799B9B73A9806FA199BF2F3A819D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetType
struct  ImageTargetType_t3A1F68B5507799B9B73A9806FA199BF2F3A819D6 
{
public:
	// System.Int32 Vuforia.ImageTargetType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ImageTargetType_t3A1F68B5507799B9B73A9806FA199BF2F3A819D6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETTYPE_T3A1F68B5507799B9B73A9806FA199BF2F3A819D6_H
#ifndef INSTANCEIDTYPE_T2B8C4E1066D9FFD1C8BAA369E303860476BABF0D_H
#define INSTANCEIDTYPE_T2B8C4E1066D9FFD1C8BAA369E303860476BABF0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.InstanceIdType
struct  InstanceIdType_t2B8C4E1066D9FFD1C8BAA369E303860476BABF0D 
{
public:
	// System.Int32 Vuforia.InstanceIdType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InstanceIdType_t2B8C4E1066D9FFD1C8BAA369E303860476BABF0D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEIDTYPE_T2B8C4E1066D9FFD1C8BAA369E303860476BABF0D_H
#ifndef LATELATCHINGMANAGER_T989F0737102DC3958150786EDFBA8BBE6C63663C_H
#define LATELATCHINGMANAGER_T989F0737102DC3958150786EDFBA8BBE6C63663C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.LateLatchingManager
struct  LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C  : public RuntimeObject
{
public:
	// System.Boolean Vuforia.LateLatchingManager::mInitialized
	bool ___mInitialized_0;
	// System.Boolean Vuforia.LateLatchingManager::mEnabled
	bool ___mEnabled_1;
	// System.Boolean Vuforia.LateLatchingManager::mUpdated
	bool ___mUpdated_2;
	// UnityEngine.Vector3 Vuforia.LateLatchingManager::mCachedPositionalOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mCachedPositionalOffset_3;
	// UnityEngine.Quaternion Vuforia.LateLatchingManager::mCachedRotationalOffset
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___mCachedRotationalOffset_4;
	// Vuforia.TrackerData_TrackableResultData[] Vuforia.LateLatchingManager::mCachedTrackableResultDataArray
	TrackableResultDataU5BU5D_t9F14AA7BB2B4B86B497397AA4ABC42C964D0477D* ___mCachedTrackableResultDataArray_5;
	// Vuforia.TrackerData_VuMarkTargetResultData[] Vuforia.LateLatchingManager::mCachedVuMarkResultDataArray
	VuMarkTargetResultDataU5BU5D_t0AAD90D01B7B61C91A7B4DE994F065D01013FF9A* ___mCachedVuMarkResultDataArray_6;
	// Vuforia.VuforiaManager_TrackableIdPair Vuforia.LateLatchingManager::mCachedOriginTrackableID
	TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F  ___mCachedOriginTrackableID_7;
	// System.Int32 Vuforia.LateLatchingManager::mCachedFrameIndex
	int32_t ___mCachedFrameIndex_8;

public:
	inline static int32_t get_offset_of_mInitialized_0() { return static_cast<int32_t>(offsetof(LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C, ___mInitialized_0)); }
	inline bool get_mInitialized_0() const { return ___mInitialized_0; }
	inline bool* get_address_of_mInitialized_0() { return &___mInitialized_0; }
	inline void set_mInitialized_0(bool value)
	{
		___mInitialized_0 = value;
	}

	inline static int32_t get_offset_of_mEnabled_1() { return static_cast<int32_t>(offsetof(LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C, ___mEnabled_1)); }
	inline bool get_mEnabled_1() const { return ___mEnabled_1; }
	inline bool* get_address_of_mEnabled_1() { return &___mEnabled_1; }
	inline void set_mEnabled_1(bool value)
	{
		___mEnabled_1 = value;
	}

	inline static int32_t get_offset_of_mUpdated_2() { return static_cast<int32_t>(offsetof(LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C, ___mUpdated_2)); }
	inline bool get_mUpdated_2() const { return ___mUpdated_2; }
	inline bool* get_address_of_mUpdated_2() { return &___mUpdated_2; }
	inline void set_mUpdated_2(bool value)
	{
		___mUpdated_2 = value;
	}

	inline static int32_t get_offset_of_mCachedPositionalOffset_3() { return static_cast<int32_t>(offsetof(LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C, ___mCachedPositionalOffset_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mCachedPositionalOffset_3() const { return ___mCachedPositionalOffset_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mCachedPositionalOffset_3() { return &___mCachedPositionalOffset_3; }
	inline void set_mCachedPositionalOffset_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mCachedPositionalOffset_3 = value;
	}

	inline static int32_t get_offset_of_mCachedRotationalOffset_4() { return static_cast<int32_t>(offsetof(LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C, ___mCachedRotationalOffset_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_mCachedRotationalOffset_4() const { return ___mCachedRotationalOffset_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_mCachedRotationalOffset_4() { return &___mCachedRotationalOffset_4; }
	inline void set_mCachedRotationalOffset_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___mCachedRotationalOffset_4 = value;
	}

	inline static int32_t get_offset_of_mCachedTrackableResultDataArray_5() { return static_cast<int32_t>(offsetof(LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C, ___mCachedTrackableResultDataArray_5)); }
	inline TrackableResultDataU5BU5D_t9F14AA7BB2B4B86B497397AA4ABC42C964D0477D* get_mCachedTrackableResultDataArray_5() const { return ___mCachedTrackableResultDataArray_5; }
	inline TrackableResultDataU5BU5D_t9F14AA7BB2B4B86B497397AA4ABC42C964D0477D** get_address_of_mCachedTrackableResultDataArray_5() { return &___mCachedTrackableResultDataArray_5; }
	inline void set_mCachedTrackableResultDataArray_5(TrackableResultDataU5BU5D_t9F14AA7BB2B4B86B497397AA4ABC42C964D0477D* value)
	{
		___mCachedTrackableResultDataArray_5 = value;
		Il2CppCodeGenWriteBarrier((&___mCachedTrackableResultDataArray_5), value);
	}

	inline static int32_t get_offset_of_mCachedVuMarkResultDataArray_6() { return static_cast<int32_t>(offsetof(LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C, ___mCachedVuMarkResultDataArray_6)); }
	inline VuMarkTargetResultDataU5BU5D_t0AAD90D01B7B61C91A7B4DE994F065D01013FF9A* get_mCachedVuMarkResultDataArray_6() const { return ___mCachedVuMarkResultDataArray_6; }
	inline VuMarkTargetResultDataU5BU5D_t0AAD90D01B7B61C91A7B4DE994F065D01013FF9A** get_address_of_mCachedVuMarkResultDataArray_6() { return &___mCachedVuMarkResultDataArray_6; }
	inline void set_mCachedVuMarkResultDataArray_6(VuMarkTargetResultDataU5BU5D_t0AAD90D01B7B61C91A7B4DE994F065D01013FF9A* value)
	{
		___mCachedVuMarkResultDataArray_6 = value;
		Il2CppCodeGenWriteBarrier((&___mCachedVuMarkResultDataArray_6), value);
	}

	inline static int32_t get_offset_of_mCachedOriginTrackableID_7() { return static_cast<int32_t>(offsetof(LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C, ___mCachedOriginTrackableID_7)); }
	inline TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F  get_mCachedOriginTrackableID_7() const { return ___mCachedOriginTrackableID_7; }
	inline TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F * get_address_of_mCachedOriginTrackableID_7() { return &___mCachedOriginTrackableID_7; }
	inline void set_mCachedOriginTrackableID_7(TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F  value)
	{
		___mCachedOriginTrackableID_7 = value;
	}

	inline static int32_t get_offset_of_mCachedFrameIndex_8() { return static_cast<int32_t>(offsetof(LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C, ___mCachedFrameIndex_8)); }
	inline int32_t get_mCachedFrameIndex_8() const { return ___mCachedFrameIndex_8; }
	inline int32_t* get_address_of_mCachedFrameIndex_8() { return &___mCachedFrameIndex_8; }
	inline void set_mCachedFrameIndex_8(int32_t value)
	{
		___mCachedFrameIndex_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LATELATCHINGMANAGER_T989F0737102DC3958150786EDFBA8BBE6C63663C_H
#ifndef OBJECTTARGETDATA_TD6160795783492DD1322A2E4B1842086FD50EF00_H
#define OBJECTTARGETDATA_TD6160795783492DD1322A2E4B1842086FD50EF00_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ObjectTargetData
#pragma pack(push, tp, 1)
struct  ObjectTargetData_tD6160795783492DD1322A2E4B1842086FD50EF00 
{
public:
	// System.Int32 Vuforia.ObjectTargetData::id
	int32_t ___id_0;
	// UnityEngine.Vector3 Vuforia.ObjectTargetData::size
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___size_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ObjectTargetData_tD6160795783492DD1322A2E4B1842086FD50EF00, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(ObjectTargetData_tD6160795783492DD1322A2E4B1842086FD50EF00, ___size_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_size_1() const { return ___size_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___size_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTARGETDATA_TD6160795783492DD1322A2E4B1842086FD50EF00_H
#ifndef ORIENTEDBOUNDINGBOX_T0698A68A263D40AA4048260B1DAD47A15EEB598B_H
#define ORIENTEDBOUNDINGBOX_T0698A68A263D40AA4048260B1DAD47A15EEB598B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.OrientedBoundingBox
struct  OrientedBoundingBox_t0698A68A263D40AA4048260B1DAD47A15EEB598B 
{
public:
	// UnityEngine.Vector2 Vuforia.OrientedBoundingBox::<Center>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CCenterU3Ek__BackingField_0;
	// UnityEngine.Vector2 Vuforia.OrientedBoundingBox::<HalfExtents>k__BackingField
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___U3CHalfExtentsU3Ek__BackingField_1;
	// System.Single Vuforia.OrientedBoundingBox::<Rotation>k__BackingField
	float ___U3CRotationU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCenterU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(OrientedBoundingBox_t0698A68A263D40AA4048260B1DAD47A15EEB598B, ___U3CCenterU3Ek__BackingField_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CCenterU3Ek__BackingField_0() const { return ___U3CCenterU3Ek__BackingField_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CCenterU3Ek__BackingField_0() { return &___U3CCenterU3Ek__BackingField_0; }
	inline void set_U3CCenterU3Ek__BackingField_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CCenterU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CHalfExtentsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OrientedBoundingBox_t0698A68A263D40AA4048260B1DAD47A15EEB598B, ___U3CHalfExtentsU3Ek__BackingField_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_U3CHalfExtentsU3Ek__BackingField_1() const { return ___U3CHalfExtentsU3Ek__BackingField_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_U3CHalfExtentsU3Ek__BackingField_1() { return &___U3CHalfExtentsU3Ek__BackingField_1; }
	inline void set_U3CHalfExtentsU3Ek__BackingField_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___U3CHalfExtentsU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CRotationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(OrientedBoundingBox_t0698A68A263D40AA4048260B1DAD47A15EEB598B, ___U3CRotationU3Ek__BackingField_2)); }
	inline float get_U3CRotationU3Ek__BackingField_2() const { return ___U3CRotationU3Ek__BackingField_2; }
	inline float* get_address_of_U3CRotationU3Ek__BackingField_2() { return &___U3CRotationU3Ek__BackingField_2; }
	inline void set_U3CRotationU3Ek__BackingField_2(float value)
	{
		___U3CRotationU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTEDBOUNDINGBOX_T0698A68A263D40AA4048260B1DAD47A15EEB598B_H
#ifndef ORIENTEDBOUNDINGBOX3D_T2239DBD535FBC2F84F49502D23761C0630CC93B9_H
#define ORIENTEDBOUNDINGBOX3D_T2239DBD535FBC2F84F49502D23761C0630CC93B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.OrientedBoundingBox3D
struct  OrientedBoundingBox3D_t2239DBD535FBC2F84F49502D23761C0630CC93B9 
{
public:
	// UnityEngine.Vector3 Vuforia.OrientedBoundingBox3D::<Center>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CCenterU3Ek__BackingField_0;
	// UnityEngine.Vector3 Vuforia.OrientedBoundingBox3D::<HalfExtents>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CHalfExtentsU3Ek__BackingField_1;
	// System.Single Vuforia.OrientedBoundingBox3D::<RotationY>k__BackingField
	float ___U3CRotationYU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCenterU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(OrientedBoundingBox3D_t2239DBD535FBC2F84F49502D23761C0630CC93B9, ___U3CCenterU3Ek__BackingField_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CCenterU3Ek__BackingField_0() const { return ___U3CCenterU3Ek__BackingField_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CCenterU3Ek__BackingField_0() { return &___U3CCenterU3Ek__BackingField_0; }
	inline void set_U3CCenterU3Ek__BackingField_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CCenterU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CHalfExtentsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OrientedBoundingBox3D_t2239DBD535FBC2F84F49502D23761C0630CC93B9, ___U3CHalfExtentsU3Ek__BackingField_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CHalfExtentsU3Ek__BackingField_1() const { return ___U3CHalfExtentsU3Ek__BackingField_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CHalfExtentsU3Ek__BackingField_1() { return &___U3CHalfExtentsU3Ek__BackingField_1; }
	inline void set_U3CHalfExtentsU3Ek__BackingField_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CHalfExtentsU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CRotationYU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(OrientedBoundingBox3D_t2239DBD535FBC2F84F49502D23761C0630CC93B9, ___U3CRotationYU3Ek__BackingField_2)); }
	inline float get_U3CRotationYU3Ek__BackingField_2() const { return ___U3CRotationYU3Ek__BackingField_2; }
	inline float* get_address_of_U3CRotationYU3Ek__BackingField_2() { return &___U3CRotationYU3Ek__BackingField_2; }
	inline void set_U3CRotationYU3Ek__BackingField_2(float value)
	{
		___U3CRotationYU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTEDBOUNDINGBOX3D_T2239DBD535FBC2F84F49502D23761C0630CC93B9_H
#ifndef PIXEL_FORMAT_T2C0F763FCD84C633340917492F06896787FD9383_H
#define PIXEL_FORMAT_T2C0F763FCD84C633340917492F06896787FD9383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PIXEL_FORMAT
struct  PIXEL_FORMAT_t2C0F763FCD84C633340917492F06896787FD9383 
{
public:
	// System.Int32 Vuforia.PIXEL_FORMAT::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PIXEL_FORMAT_t2C0F763FCD84C633340917492F06896787FD9383, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIXEL_FORMAT_T2C0F763FCD84C633340917492F06896787FD9383_H
#ifndef MODEL_CORRECTION_MODE_TA628D33CBBD6F312044315FED05B471AD6AEA17E_H
#define MODEL_CORRECTION_MODE_TA628D33CBBD6F312044315FED05B471AD6AEA17E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.RotationalDeviceTracker_MODEL_CORRECTION_MODE
struct  MODEL_CORRECTION_MODE_tA628D33CBBD6F312044315FED05B471AD6AEA17E 
{
public:
	// System.Int32 Vuforia.RotationalDeviceTracker_MODEL_CORRECTION_MODE::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MODEL_CORRECTION_MODE_tA628D33CBBD6F312044315FED05B471AD6AEA17E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODEL_CORRECTION_MODE_TA628D33CBBD6F312044315FED05B471AD6AEA17E_H
#ifndef STATUS_T9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092_H
#define STATUS_T9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour_Status
struct  Status_t9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour_Status::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Status_t9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092_H
#ifndef STATUSINFO_T5507FB8CC09640E7771385EBE27221431A2FEB4E_H
#define STATUSINFO_T5507FB8CC09640E7771385EBE27221431A2FEB4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour_StatusInfo
struct  StatusInfo_t5507FB8CC09640E7771385EBE27221431A2FEB4E 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour_StatusInfo::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StatusInfo_t5507FB8CC09640E7771385EBE27221431A2FEB4E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUSINFO_T5507FB8CC09640E7771385EBE27221431A2FEB4E_H
#ifndef FRAMESTATE_T40C5EF3D259514DAB23DEDCD72218A934C5645E0_H
#define FRAMESTATE_T40C5EF3D259514DAB23DEDCD72218A934C5645E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackerData_FrameState
#pragma pack(push, tp, 1)
struct  FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0 
{
public:
	// System.IntPtr Vuforia.TrackerData_FrameState::trackableDataArray
	intptr_t ___trackableDataArray_0;
	// System.IntPtr Vuforia.TrackerData_FrameState::vbDataArray
	intptr_t ___vbDataArray_1;
	// System.IntPtr Vuforia.TrackerData_FrameState::vuMarkResultArray
	intptr_t ___vuMarkResultArray_2;
	// System.IntPtr Vuforia.TrackerData_FrameState::newVuMarkDataArray
	intptr_t ___newVuMarkDataArray_3;
	// System.IntPtr Vuforia.TrackerData_FrameState::illuminationData
	intptr_t ___illuminationData_4;
	// System.Int32 Vuforia.TrackerData_FrameState::numTrackableResults
	int32_t ___numTrackableResults_5;
	// System.Int32 Vuforia.TrackerData_FrameState::numVirtualButtonResults
	int32_t ___numVirtualButtonResults_6;
	// System.Int32 Vuforia.TrackerData_FrameState::frameIndex
	int32_t ___frameIndex_7;
	// System.Int32 Vuforia.TrackerData_FrameState::numVuMarkResults
	int32_t ___numVuMarkResults_8;
	// System.Int32 Vuforia.TrackerData_FrameState::numNewVuMarks
	int32_t ___numNewVuMarks_9;
	// System.Int32 Vuforia.TrackerData_FrameState::deviceTrackableId
	int32_t ___deviceTrackableId_10;
	// System.Int32 Vuforia.TrackerData_FrameState::deviceTrackableStatusInfo
	int32_t ___deviceTrackableStatusInfo_11;
	// UnityEngine.Vector4 Vuforia.TrackerData_FrameState::minCameraCalibration
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___minCameraCalibration_12;

public:
	inline static int32_t get_offset_of_trackableDataArray_0() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___trackableDataArray_0)); }
	inline intptr_t get_trackableDataArray_0() const { return ___trackableDataArray_0; }
	inline intptr_t* get_address_of_trackableDataArray_0() { return &___trackableDataArray_0; }
	inline void set_trackableDataArray_0(intptr_t value)
	{
		___trackableDataArray_0 = value;
	}

	inline static int32_t get_offset_of_vbDataArray_1() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___vbDataArray_1)); }
	inline intptr_t get_vbDataArray_1() const { return ___vbDataArray_1; }
	inline intptr_t* get_address_of_vbDataArray_1() { return &___vbDataArray_1; }
	inline void set_vbDataArray_1(intptr_t value)
	{
		___vbDataArray_1 = value;
	}

	inline static int32_t get_offset_of_vuMarkResultArray_2() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___vuMarkResultArray_2)); }
	inline intptr_t get_vuMarkResultArray_2() const { return ___vuMarkResultArray_2; }
	inline intptr_t* get_address_of_vuMarkResultArray_2() { return &___vuMarkResultArray_2; }
	inline void set_vuMarkResultArray_2(intptr_t value)
	{
		___vuMarkResultArray_2 = value;
	}

	inline static int32_t get_offset_of_newVuMarkDataArray_3() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___newVuMarkDataArray_3)); }
	inline intptr_t get_newVuMarkDataArray_3() const { return ___newVuMarkDataArray_3; }
	inline intptr_t* get_address_of_newVuMarkDataArray_3() { return &___newVuMarkDataArray_3; }
	inline void set_newVuMarkDataArray_3(intptr_t value)
	{
		___newVuMarkDataArray_3 = value;
	}

	inline static int32_t get_offset_of_illuminationData_4() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___illuminationData_4)); }
	inline intptr_t get_illuminationData_4() const { return ___illuminationData_4; }
	inline intptr_t* get_address_of_illuminationData_4() { return &___illuminationData_4; }
	inline void set_illuminationData_4(intptr_t value)
	{
		___illuminationData_4 = value;
	}

	inline static int32_t get_offset_of_numTrackableResults_5() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___numTrackableResults_5)); }
	inline int32_t get_numTrackableResults_5() const { return ___numTrackableResults_5; }
	inline int32_t* get_address_of_numTrackableResults_5() { return &___numTrackableResults_5; }
	inline void set_numTrackableResults_5(int32_t value)
	{
		___numTrackableResults_5 = value;
	}

	inline static int32_t get_offset_of_numVirtualButtonResults_6() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___numVirtualButtonResults_6)); }
	inline int32_t get_numVirtualButtonResults_6() const { return ___numVirtualButtonResults_6; }
	inline int32_t* get_address_of_numVirtualButtonResults_6() { return &___numVirtualButtonResults_6; }
	inline void set_numVirtualButtonResults_6(int32_t value)
	{
		___numVirtualButtonResults_6 = value;
	}

	inline static int32_t get_offset_of_frameIndex_7() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___frameIndex_7)); }
	inline int32_t get_frameIndex_7() const { return ___frameIndex_7; }
	inline int32_t* get_address_of_frameIndex_7() { return &___frameIndex_7; }
	inline void set_frameIndex_7(int32_t value)
	{
		___frameIndex_7 = value;
	}

	inline static int32_t get_offset_of_numVuMarkResults_8() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___numVuMarkResults_8)); }
	inline int32_t get_numVuMarkResults_8() const { return ___numVuMarkResults_8; }
	inline int32_t* get_address_of_numVuMarkResults_8() { return &___numVuMarkResults_8; }
	inline void set_numVuMarkResults_8(int32_t value)
	{
		___numVuMarkResults_8 = value;
	}

	inline static int32_t get_offset_of_numNewVuMarks_9() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___numNewVuMarks_9)); }
	inline int32_t get_numNewVuMarks_9() const { return ___numNewVuMarks_9; }
	inline int32_t* get_address_of_numNewVuMarks_9() { return &___numNewVuMarks_9; }
	inline void set_numNewVuMarks_9(int32_t value)
	{
		___numNewVuMarks_9 = value;
	}

	inline static int32_t get_offset_of_deviceTrackableId_10() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___deviceTrackableId_10)); }
	inline int32_t get_deviceTrackableId_10() const { return ___deviceTrackableId_10; }
	inline int32_t* get_address_of_deviceTrackableId_10() { return &___deviceTrackableId_10; }
	inline void set_deviceTrackableId_10(int32_t value)
	{
		___deviceTrackableId_10 = value;
	}

	inline static int32_t get_offset_of_deviceTrackableStatusInfo_11() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___deviceTrackableStatusInfo_11)); }
	inline int32_t get_deviceTrackableStatusInfo_11() const { return ___deviceTrackableStatusInfo_11; }
	inline int32_t* get_address_of_deviceTrackableStatusInfo_11() { return &___deviceTrackableStatusInfo_11; }
	inline void set_deviceTrackableStatusInfo_11(int32_t value)
	{
		___deviceTrackableStatusInfo_11 = value;
	}

	inline static int32_t get_offset_of_minCameraCalibration_12() { return static_cast<int32_t>(offsetof(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0, ___minCameraCalibration_12)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_minCameraCalibration_12() const { return ___minCameraCalibration_12; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_minCameraCalibration_12() { return &___minCameraCalibration_12; }
	inline void set_minCameraCalibration_12(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___minCameraCalibration_12 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMESTATE_T40C5EF3D259514DAB23DEDCD72218A934C5645E0_H
#ifndef WORLDCENTERMODE_T53E1430BD989A54F75332A2DA9D61C93545897E6_H
#define WORLDCENTERMODE_T53E1430BD989A54F75332A2DA9D61C93545897E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaARController_WorldCenterMode
struct  WorldCenterMode_t53E1430BD989A54F75332A2DA9D61C93545897E6 
{
public:
	// System.Int32 Vuforia.VuforiaARController_WorldCenterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WorldCenterMode_t53E1430BD989A54F75332A2DA9D61C93545897E6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDCENTERMODE_T53E1430BD989A54F75332A2DA9D61C93545897E6_H
#ifndef ARCOREREQUIREMENT_TE27B0C1428A8BB5AD741CC60732FEB1F09AC9155_H
#define ARCOREREQUIREMENT_TE27B0C1428A8BB5AD741CC60732FEB1F09AC9155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaConfiguration_DeviceTrackerConfiguration_ARCoreRequirement
struct  ARCoreRequirement_tE27B0C1428A8BB5AD741CC60732FEB1F09AC9155 
{
public:
	// System.Int32 Vuforia.VuforiaConfiguration_DeviceTrackerConfiguration_ARCoreRequirement::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ARCoreRequirement_tE27B0C1428A8BB5AD741CC60732FEB1F09AC9155, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCOREREQUIREMENT_TE27B0C1428A8BB5AD741CC60732FEB1F09AC9155_H
#ifndef U3CU3EC__DISPLAYCLASS71_0_TF16DC2DDDAFB2FC93B740291F3BF21ECA28C9174_H
#define U3CU3EC__DISPLAYCLASS71_0_TF16DC2DDDAFB2FC93B740291F3BF21ECA28C9174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManager_<>c__DisplayClass71_0
struct  U3CU3Ec__DisplayClass71_0_tF16DC2DDDAFB2FC93B740291F3BF21ECA28C9174  : public RuntimeObject
{
public:
	// Vuforia.VuforiaManager_TrackableIdPair Vuforia.VuforiaManager_<>c__DisplayClass71_0::id
	TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F  ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass71_0_tF16DC2DDDAFB2FC93B740291F3BF21ECA28C9174, ___id_0)); }
	inline TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F  get_id_0() const { return ___id_0; }
	inline TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F * get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F  value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS71_0_TF16DC2DDDAFB2FC93B740291F3BF21ECA28C9174_H
#ifndef FPSHINT_T53FC6839AC85BCC4C8696D147820A30A5C8A5276_H
#define FPSHINT_T53FC6839AC85BCC4C8696D147820A30A5C8A5276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer_FpsHint
struct  FpsHint_t53FC6839AC85BCC4C8696D147820A30A5C8A5276 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer_FpsHint::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FpsHint_t53FC6839AC85BCC4C8696D147820A30A5C8A5276, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSHINT_T53FC6839AC85BCC4C8696D147820A30A5C8A5276_H
#ifndef RENDEREVENT_T855A5B140DD41FF1AF8179D914AEA4D2B1B88DA7_H
#define RENDEREVENT_T855A5B140DD41FF1AF8179D914AEA4D2B1B88DA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer_RenderEvent
struct  RenderEvent_t855A5B140DD41FF1AF8179D914AEA4D2B1B88DA7 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer_RenderEvent::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RenderEvent_t855A5B140DD41FF1AF8179D914AEA4D2B1B88DA7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDEREVENT_T855A5B140DD41FF1AF8179D914AEA4D2B1B88DA7_H
#ifndef RENDERERAPI_TC4AFE17C6FEA6EFD2328230E48CF33F7884AAAB1_H
#define RENDERERAPI_TC4AFE17C6FEA6EFD2328230E48CF33F7884AAAB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer_RendererAPI
struct  RendererAPI_tC4AFE17C6FEA6EFD2328230E48CF33F7884AAAB1 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer_RendererAPI::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RendererAPI_tC4AFE17C6FEA6EFD2328230E48CF33F7884AAAB1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERERAPI_TC4AFE17C6FEA6EFD2328230E48CF33F7884AAAB1_H
#ifndef VIDEOBGCFGDATA_T4FD9E3E825BD3AF119DFAE696F50C478336B1A9A_H
#define VIDEOBGCFGDATA_T4FD9E3E825BD3AF119DFAE696F50C478336B1A9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer_VideoBGCfgData
#pragma pack(push, tp, 1)
struct  VideoBGCfgData_t4FD9E3E825BD3AF119DFAE696F50C478336B1A9A 
{
public:
	// Vuforia.VuforiaRenderer_Vec2I Vuforia.VuforiaRenderer_VideoBGCfgData::position
	Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  ___position_0;
	// Vuforia.VuforiaRenderer_Vec2I Vuforia.VuforiaRenderer_VideoBGCfgData::size
	Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  ___size_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(VideoBGCfgData_t4FD9E3E825BD3AF119DFAE696F50C478336B1A9A, ___position_0)); }
	inline Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  get_position_0() const { return ___position_0; }
	inline Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(VideoBGCfgData_t4FD9E3E825BD3AF119DFAE696F50C478336B1A9A, ___size_1)); }
	inline Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  get_size_1() const { return ___size_1; }
	inline Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5 * get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  value)
	{
		___size_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBGCFGDATA_T4FD9E3E825BD3AF119DFAE696F50C478336B1A9A_H
#ifndef VIDEOTEXTUREINFO_TE91303BB8B6C3E4C49CB1A717AE3ECEB2175D36D_H
#define VIDEOTEXTUREINFO_TE91303BB8B6C3E4C49CB1A717AE3ECEB2175D36D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer_VideoTextureInfo
#pragma pack(push, tp, 1)
struct  VideoTextureInfo_tE91303BB8B6C3E4C49CB1A717AE3ECEB2175D36D 
{
public:
	// Vuforia.VuforiaRenderer_Vec2I Vuforia.VuforiaRenderer_VideoTextureInfo::textureSize
	Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  ___textureSize_0;
	// Vuforia.VuforiaRenderer_Vec2I Vuforia.VuforiaRenderer_VideoTextureInfo::imageSize
	Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  ___imageSize_1;

public:
	inline static int32_t get_offset_of_textureSize_0() { return static_cast<int32_t>(offsetof(VideoTextureInfo_tE91303BB8B6C3E4C49CB1A717AE3ECEB2175D36D, ___textureSize_0)); }
	inline Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  get_textureSize_0() const { return ___textureSize_0; }
	inline Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5 * get_address_of_textureSize_0() { return &___textureSize_0; }
	inline void set_textureSize_0(Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  value)
	{
		___textureSize_0 = value;
	}

	inline static int32_t get_offset_of_imageSize_1() { return static_cast<int32_t>(offsetof(VideoTextureInfo_tE91303BB8B6C3E4C49CB1A717AE3ECEB2175D36D, ___imageSize_1)); }
	inline Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  get_imageSize_1() const { return ___imageSize_1; }
	inline Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5 * get_address_of_imageSize_1() { return &___imageSize_1; }
	inline void set_imageSize_1(Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  value)
	{
		___imageSize_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOTEXTUREINFO_TE91303BB8B6C3E4C49CB1A717AE3ECEB2175D36D_H
#ifndef INITSTATE_T0072A2BD2C69378E4575A8148A3DB0974451CD70_H
#define INITSTATE_T0072A2BD2C69378E4575A8148A3DB0974451CD70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntime_InitState
struct  InitState_t0072A2BD2C69378E4575A8148A3DB0974451CD70 
{
public:
	// System.Int32 Vuforia.VuforiaRuntime_InitState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InitState_t0072A2BD2C69378E4575A8148A3DB0974451CD70, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITSTATE_T0072A2BD2C69378E4575A8148A3DB0974451CD70_H
#ifndef INITIALIZABLEBOOL_T6C711D5999687FC6E8CAF38208EC8E185BF1FC22_H
#define INITIALIZABLEBOOL_T6C711D5999687FC6E8CAF38208EC8E185BF1FC22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntimeUtilities_InitializableBool
struct  InitializableBool_t6C711D5999687FC6E8CAF38208EC8E185BF1FC22 
{
public:
	// System.Int32 Vuforia.VuforiaRuntimeUtilities_InitializableBool::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InitializableBool_t6C711D5999687FC6E8CAF38208EC8E185BF1FC22, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITIALIZABLEBOOL_T6C711D5999687FC6E8CAF38208EC8E185BF1FC22_H
#ifndef INITERROR_T486F7D53F5B0B7943D4E22BE2D32F4913D4A0431_H
#define INITERROR_T486F7D53F5B0B7943D4E22BE2D32F4913D4A0431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaUnity_InitError
struct  InitError_t486F7D53F5B0B7943D4E22BE2D32F4913D4A0431 
{
public:
	// System.Int32 Vuforia.VuforiaUnity_InitError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InitError_t486F7D53F5B0B7943D4E22BE2D32F4913D4A0431, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITERROR_T486F7D53F5B0B7943D4E22BE2D32F4913D4A0431_H
#ifndef STORAGETYPE_T6847D699566F42A8C42777B30537BA0C0521A4D3_H
#define STORAGETYPE_T6847D699566F42A8C42777B30537BA0C0521A4D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaUnity_StorageType
struct  StorageType_t6847D699566F42A8C42777B30537BA0C0521A4D3 
{
public:
	// System.Int32 Vuforia.VuforiaUnity_StorageType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StorageType_t6847D699566F42A8C42777B30537BA0C0521A4D3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORAGETYPE_T6847D699566F42A8C42777B30537BA0C0521A4D3_H
#ifndef VUFORIAHINT_T5E4E8E78687FAEB9311A944CD4051169EF4A5A6D_H
#define VUFORIAHINT_T5E4E8E78687FAEB9311A944CD4051169EF4A5A6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaUnity_VuforiaHint
struct  VuforiaHint_t5E4E8E78687FAEB9311A944CD4051169EF4A5A6D 
{
public:
	// System.Int32 Vuforia.VuforiaUnity_VuforiaHint::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VuforiaHint_t5E4E8E78687FAEB9311A944CD4051169EF4A5A6D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAHINT_T5E4E8E78687FAEB9311A944CD4051169EF4A5A6D_H
#ifndef PROFILEDATA_T7A423949645BADBD3002DCB2C13AD2BB7B6C2F9E_H
#define PROFILEDATA_T7A423949645BADBD3002DCB2C13AD2BB7B6C2F9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamProfile_ProfileData
struct  ProfileData_t7A423949645BADBD3002DCB2C13AD2BB7B6C2F9E 
{
public:
	// Vuforia.VuforiaRenderer_Vec2I Vuforia.WebCamProfile_ProfileData::RequestedTextureSize
	Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  ___RequestedTextureSize_0;
	// Vuforia.VuforiaRenderer_Vec2I Vuforia.WebCamProfile_ProfileData::ResampledTextureSize
	Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  ___ResampledTextureSize_1;
	// System.Int32 Vuforia.WebCamProfile_ProfileData::RequestedFPS
	int32_t ___RequestedFPS_2;

public:
	inline static int32_t get_offset_of_RequestedTextureSize_0() { return static_cast<int32_t>(offsetof(ProfileData_t7A423949645BADBD3002DCB2C13AD2BB7B6C2F9E, ___RequestedTextureSize_0)); }
	inline Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  get_RequestedTextureSize_0() const { return ___RequestedTextureSize_0; }
	inline Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5 * get_address_of_RequestedTextureSize_0() { return &___RequestedTextureSize_0; }
	inline void set_RequestedTextureSize_0(Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  value)
	{
		___RequestedTextureSize_0 = value;
	}

	inline static int32_t get_offset_of_ResampledTextureSize_1() { return static_cast<int32_t>(offsetof(ProfileData_t7A423949645BADBD3002DCB2C13AD2BB7B6C2F9E, ___ResampledTextureSize_1)); }
	inline Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  get_ResampledTextureSize_1() const { return ___ResampledTextureSize_1; }
	inline Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5 * get_address_of_ResampledTextureSize_1() { return &___ResampledTextureSize_1; }
	inline void set_ResampledTextureSize_1(Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5  value)
	{
		___ResampledTextureSize_1 = value;
	}

	inline static int32_t get_offset_of_RequestedFPS_2() { return static_cast<int32_t>(offsetof(ProfileData_t7A423949645BADBD3002DCB2C13AD2BB7B6C2F9E, ___RequestedFPS_2)); }
	inline int32_t get_RequestedFPS_2() const { return ___RequestedFPS_2; }
	inline int32_t* get_address_of_RequestedFPS_2() { return &___RequestedFPS_2; }
	inline void set_RequestedFPS_2(int32_t value)
	{
		___RequestedFPS_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFILEDATA_T7A423949645BADBD3002DCB2C13AD2BB7B6C2F9E_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef CYLINDERTARGETIMPL_T5DF553BFC311E3E8DBF13776A39F11C50F39BFA5_H
#define CYLINDERTARGETIMPL_T5DF553BFC311E3E8DBF13776A39F11C50F39BFA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CylinderTargetImpl
struct  CylinderTargetImpl_t5DF553BFC311E3E8DBF13776A39F11C50F39BFA5  : public DataSetObjectTargetImpl_tC2D896B84A1FDE730F7E2F20045D12CBC6F30C98
{
public:
	// System.Single Vuforia.CylinderTargetImpl::mSideLength
	float ___mSideLength_4;
	// System.Single Vuforia.CylinderTargetImpl::mTopDiameter
	float ___mTopDiameter_5;
	// System.Single Vuforia.CylinderTargetImpl::mBottomDiameter
	float ___mBottomDiameter_6;
	// Vuforia.CustomDataSetTargetSize Vuforia.CylinderTargetImpl::mCustomTargetSize
	CustomDataSetTargetSize_t2BD3BD33FBB280F4FA853FB9E9D00ED16BCBF031 * ___mCustomTargetSize_7;

public:
	inline static int32_t get_offset_of_mSideLength_4() { return static_cast<int32_t>(offsetof(CylinderTargetImpl_t5DF553BFC311E3E8DBF13776A39F11C50F39BFA5, ___mSideLength_4)); }
	inline float get_mSideLength_4() const { return ___mSideLength_4; }
	inline float* get_address_of_mSideLength_4() { return &___mSideLength_4; }
	inline void set_mSideLength_4(float value)
	{
		___mSideLength_4 = value;
	}

	inline static int32_t get_offset_of_mTopDiameter_5() { return static_cast<int32_t>(offsetof(CylinderTargetImpl_t5DF553BFC311E3E8DBF13776A39F11C50F39BFA5, ___mTopDiameter_5)); }
	inline float get_mTopDiameter_5() const { return ___mTopDiameter_5; }
	inline float* get_address_of_mTopDiameter_5() { return &___mTopDiameter_5; }
	inline void set_mTopDiameter_5(float value)
	{
		___mTopDiameter_5 = value;
	}

	inline static int32_t get_offset_of_mBottomDiameter_6() { return static_cast<int32_t>(offsetof(CylinderTargetImpl_t5DF553BFC311E3E8DBF13776A39F11C50F39BFA5, ___mBottomDiameter_6)); }
	inline float get_mBottomDiameter_6() const { return ___mBottomDiameter_6; }
	inline float* get_address_of_mBottomDiameter_6() { return &___mBottomDiameter_6; }
	inline void set_mBottomDiameter_6(float value)
	{
		___mBottomDiameter_6 = value;
	}

	inline static int32_t get_offset_of_mCustomTargetSize_7() { return static_cast<int32_t>(offsetof(CylinderTargetImpl_t5DF553BFC311E3E8DBF13776A39F11C50F39BFA5, ___mCustomTargetSize_7)); }
	inline CustomDataSetTargetSize_t2BD3BD33FBB280F4FA853FB9E9D00ED16BCBF031 * get_mCustomTargetSize_7() const { return ___mCustomTargetSize_7; }
	inline CustomDataSetTargetSize_t2BD3BD33FBB280F4FA853FB9E9D00ED16BCBF031 ** get_address_of_mCustomTargetSize_7() { return &___mCustomTargetSize_7; }
	inline void set_mCustomTargetSize_7(CustomDataSetTargetSize_t2BD3BD33FBB280F4FA853FB9E9D00ED16BCBF031 * value)
	{
		___mCustomTargetSize_7 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomTargetSize_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYLINDERTARGETIMPL_T5DF553BFC311E3E8DBF13776A39F11C50F39BFA5_H
#ifndef DATASET_T9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644_H
#define DATASET_T9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DataSet
struct  DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644  : public RuntimeObject
{
public:
	// System.IntPtr Vuforia.DataSet::mDataSetPtr
	intptr_t ___mDataSetPtr_0;
	// System.String Vuforia.DataSet::mPath
	String_t* ___mPath_1;
	// Vuforia.VuforiaUnity_StorageType Vuforia.DataSet::mStorageType
	int32_t ___mStorageType_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable> Vuforia.DataSet::mTrackablesDict
	Dictionary_2_t7A03A041C3064135DC6552629B8B07C88FC4A0CD * ___mTrackablesDict_3;

public:
	inline static int32_t get_offset_of_mDataSetPtr_0() { return static_cast<int32_t>(offsetof(DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644, ___mDataSetPtr_0)); }
	inline intptr_t get_mDataSetPtr_0() const { return ___mDataSetPtr_0; }
	inline intptr_t* get_address_of_mDataSetPtr_0() { return &___mDataSetPtr_0; }
	inline void set_mDataSetPtr_0(intptr_t value)
	{
		___mDataSetPtr_0 = value;
	}

	inline static int32_t get_offset_of_mPath_1() { return static_cast<int32_t>(offsetof(DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644, ___mPath_1)); }
	inline String_t* get_mPath_1() const { return ___mPath_1; }
	inline String_t** get_address_of_mPath_1() { return &___mPath_1; }
	inline void set_mPath_1(String_t* value)
	{
		___mPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___mPath_1), value);
	}

	inline static int32_t get_offset_of_mStorageType_2() { return static_cast<int32_t>(offsetof(DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644, ___mStorageType_2)); }
	inline int32_t get_mStorageType_2() const { return ___mStorageType_2; }
	inline int32_t* get_address_of_mStorageType_2() { return &___mStorageType_2; }
	inline void set_mStorageType_2(int32_t value)
	{
		___mStorageType_2 = value;
	}

	inline static int32_t get_offset_of_mTrackablesDict_3() { return static_cast<int32_t>(offsetof(DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644, ___mTrackablesDict_3)); }
	inline Dictionary_2_t7A03A041C3064135DC6552629B8B07C88FC4A0CD * get_mTrackablesDict_3() const { return ___mTrackablesDict_3; }
	inline Dictionary_2_t7A03A041C3064135DC6552629B8B07C88FC4A0CD ** get_address_of_mTrackablesDict_3() { return &___mTrackablesDict_3; }
	inline void set_mTrackablesDict_3(Dictionary_2_t7A03A041C3064135DC6552629B8B07C88FC4A0CD * value)
	{
		___mTrackablesDict_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackablesDict_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASET_T9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644_H
#ifndef IMAGETARGETIMPL_T5DF2A3C71EC32EFB7A5D80571C65827C436BE0FC_H
#define IMAGETARGETIMPL_T5DF2A3C71EC32EFB7A5D80571C65827C436BE0FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetImpl
struct  ImageTargetImpl_t5DF2A3C71EC32EFB7A5D80571C65827C436BE0FC  : public DataSetObjectTargetImpl_tC2D896B84A1FDE730F7E2F20045D12CBC6F30C98
{
public:
	// Vuforia.ImageTargetType Vuforia.ImageTargetImpl::mImageTargetType
	int32_t ___mImageTargetType_4;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton> Vuforia.ImageTargetImpl::mVirtualButtons
	Dictionary_2_t8226BA474A6287F4B7854D0BB11D209617E6ECD6 * ___mVirtualButtons_5;

public:
	inline static int32_t get_offset_of_mImageTargetType_4() { return static_cast<int32_t>(offsetof(ImageTargetImpl_t5DF2A3C71EC32EFB7A5D80571C65827C436BE0FC, ___mImageTargetType_4)); }
	inline int32_t get_mImageTargetType_4() const { return ___mImageTargetType_4; }
	inline int32_t* get_address_of_mImageTargetType_4() { return &___mImageTargetType_4; }
	inline void set_mImageTargetType_4(int32_t value)
	{
		___mImageTargetType_4 = value;
	}

	inline static int32_t get_offset_of_mVirtualButtons_5() { return static_cast<int32_t>(offsetof(ImageTargetImpl_t5DF2A3C71EC32EFB7A5D80571C65827C436BE0FC, ___mVirtualButtons_5)); }
	inline Dictionary_2_t8226BA474A6287F4B7854D0BB11D209617E6ECD6 * get_mVirtualButtons_5() const { return ___mVirtualButtons_5; }
	inline Dictionary_2_t8226BA474A6287F4B7854D0BB11D209617E6ECD6 ** get_address_of_mVirtualButtons_5() { return &___mVirtualButtons_5; }
	inline void set_mVirtualButtons_5(Dictionary_2_t8226BA474A6287F4B7854D0BB11D209617E6ECD6 * value)
	{
		___mVirtualButtons_5 = value;
		Il2CppCodeGenWriteBarrier((&___mVirtualButtons_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETIMPL_T5DF2A3C71EC32EFB7A5D80571C65827C436BE0FC_H
#ifndef MULTITARGETIMPL_T7EC94DEAA083EF8826B31B4700CADDACA4B1643A_H
#define MULTITARGETIMPL_T7EC94DEAA083EF8826B31B4700CADDACA4B1643A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MultiTargetImpl
struct  MultiTargetImpl_t7EC94DEAA083EF8826B31B4700CADDACA4B1643A  : public DataSetObjectTargetImpl_tC2D896B84A1FDE730F7E2F20045D12CBC6F30C98
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTITARGETIMPL_T7EC94DEAA083EF8826B31B4700CADDACA4B1643A_H
#ifndef MULTISETTARGETSIZE_T4D4F4D573C37F7A936EB268A4576CDC0602E22E3_H
#define MULTISETTARGETSIZE_T4D4F4D573C37F7A936EB268A4576CDC0602E22E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MultiTargetImpl_MultiSetTargetSize
struct  MultiSetTargetSize_t4D4F4D573C37F7A936EB268A4576CDC0602E22E3  : public DisabledSetTargetSize_tED3AEC3CD7D4DA45A1D88625566B3D8E9141B0DA
{
public:
	// System.IntPtr Vuforia.MultiTargetImpl_MultiSetTargetSize::mDataSetPtr
	intptr_t ___mDataSetPtr_1;
	// System.String Vuforia.MultiTargetImpl_MultiSetTargetSize::mName
	String_t* ___mName_2;

public:
	inline static int32_t get_offset_of_mDataSetPtr_1() { return static_cast<int32_t>(offsetof(MultiSetTargetSize_t4D4F4D573C37F7A936EB268A4576CDC0602E22E3, ___mDataSetPtr_1)); }
	inline intptr_t get_mDataSetPtr_1() const { return ___mDataSetPtr_1; }
	inline intptr_t* get_address_of_mDataSetPtr_1() { return &___mDataSetPtr_1; }
	inline void set_mDataSetPtr_1(intptr_t value)
	{
		___mDataSetPtr_1 = value;
	}

	inline static int32_t get_offset_of_mName_2() { return static_cast<int32_t>(offsetof(MultiSetTargetSize_t4D4F4D573C37F7A936EB268A4576CDC0602E22E3, ___mName_2)); }
	inline String_t* get_mName_2() const { return ___mName_2; }
	inline String_t** get_address_of_mName_2() { return &___mName_2; }
	inline void set_mName_2(String_t* value)
	{
		___mName_2 = value;
		Il2CppCodeGenWriteBarrier((&___mName_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTISETTARGETSIZE_T4D4F4D573C37F7A936EB268A4576CDC0602E22E3_H
#ifndef VUFORIAARCONTROLLER_T7732FFB77105A2F5BBEA40E3CECC5C15DA624876_H
#define VUFORIAARCONTROLLER_T7732FFB77105A2F5BBEA40E3CECC5C15DA624876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaARController
struct  VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876  : public ARController_tBCF5CBE22AE2AD3FF6DCAC88E78FF7C929D91293
{
public:
	// Vuforia.CameraDevice_CameraDeviceMode Vuforia.VuforiaARController::mCameraDeviceModeSetting
	int32_t ___mCameraDeviceModeSetting_1;
	// System.Int32 Vuforia.VuforiaARController::mMaxSimultaneousImageTargets
	int32_t ___mMaxSimultaneousImageTargets_2;
	// System.Int32 Vuforia.VuforiaARController::mMaxSimultaneousObjectTargets
	int32_t ___mMaxSimultaneousObjectTargets_3;
	// System.Boolean Vuforia.VuforiaARController::mUseDelayedLoadingObjectTargets
	bool ___mUseDelayedLoadingObjectTargets_4;
	// System.Boolean Vuforia.VuforiaARController::mModelTargetRecoWhileExtendedTracked
	bool ___mModelTargetRecoWhileExtendedTracked_5;
	// Vuforia.VuforiaARController_WorldCenterMode Vuforia.VuforiaARController::mWorldCenterMode
	int32_t ___mWorldCenterMode_6;
	// Vuforia.TrackableBehaviour Vuforia.VuforiaARController::mWorldCenter
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * ___mWorldCenter_7;
	// System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler> Vuforia.VuforiaARController::mVideoBgEventHandlers
	List_1_t1314A3DDAE2D8AD81F9403BD961C3CC0530B04A9 * ___mVideoBgEventHandlers_8;
	// System.Action Vuforia.VuforiaARController::mOnBeforeVuforiaTrackersInitialized
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___mOnBeforeVuforiaTrackersInitialized_9;
	// System.Action Vuforia.VuforiaARController::mOnVuforiaInitialized
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___mOnVuforiaInitialized_10;
	// System.Action Vuforia.VuforiaARController::mOnVuforiaStarted
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___mOnVuforiaStarted_11;
	// System.Action Vuforia.VuforiaARController::mOnVuforiaDeinitialized
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___mOnVuforiaDeinitialized_12;
	// System.Action Vuforia.VuforiaARController::mOnTrackablesUpdated
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___mOnTrackablesUpdated_13;
	// System.Action Vuforia.VuforiaARController::mRenderOnUpdate
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___mRenderOnUpdate_14;
	// System.Action`1<System.Boolean> Vuforia.VuforiaARController::mOnPause
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___mOnPause_15;
	// System.Boolean Vuforia.VuforiaARController::mPaused
	bool ___mPaused_16;
	// System.Action Vuforia.VuforiaARController::mOnBackgroundTextureChanged
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___mOnBackgroundTextureChanged_17;
	// System.Boolean Vuforia.VuforiaARController::mStartHasBeenInvoked
	bool ___mStartHasBeenInvoked_18;
	// System.Boolean Vuforia.VuforiaARController::mHasStarted
	bool ___mHasStarted_19;
	// Vuforia.ICameraConfiguration Vuforia.VuforiaARController::mCameraConfiguration
	RuntimeObject* ___mCameraConfiguration_20;
	// Vuforia.DigitalEyewearARController Vuforia.VuforiaARController::mEyewearBehaviour
	DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4 * ___mEyewearBehaviour_21;
	// System.Boolean Vuforia.VuforiaARController::mCheckStopCamera
	bool ___mCheckStopCamera_22;
	// UnityEngine.Material Vuforia.VuforiaARController::mClearMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___mClearMaterial_23;
	// System.Boolean Vuforia.VuforiaARController::mMetalRendering
	bool ___mMetalRendering_24;
	// System.Boolean Vuforia.VuforiaARController::mHasStartedOnce
	bool ___mHasStartedOnce_25;
	// System.Boolean Vuforia.VuforiaARController::mWasEnabledBeforePause
	bool ___mWasEnabledBeforePause_26;
	// System.Boolean Vuforia.VuforiaARController::mObjectTrackerWasActiveBeforePause
	bool ___mObjectTrackerWasActiveBeforePause_27;
	// System.Boolean Vuforia.VuforiaARController::mObjectTrackerWasActiveBeforeDisabling
	bool ___mObjectTrackerWasActiveBeforeDisabling_28;
	// System.Int32 Vuforia.VuforiaARController::mLastUpdatedFrame
	int32_t ___mLastUpdatedFrame_29;

public:
	inline static int32_t get_offset_of_mCameraDeviceModeSetting_1() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mCameraDeviceModeSetting_1)); }
	inline int32_t get_mCameraDeviceModeSetting_1() const { return ___mCameraDeviceModeSetting_1; }
	inline int32_t* get_address_of_mCameraDeviceModeSetting_1() { return &___mCameraDeviceModeSetting_1; }
	inline void set_mCameraDeviceModeSetting_1(int32_t value)
	{
		___mCameraDeviceModeSetting_1 = value;
	}

	inline static int32_t get_offset_of_mMaxSimultaneousImageTargets_2() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mMaxSimultaneousImageTargets_2)); }
	inline int32_t get_mMaxSimultaneousImageTargets_2() const { return ___mMaxSimultaneousImageTargets_2; }
	inline int32_t* get_address_of_mMaxSimultaneousImageTargets_2() { return &___mMaxSimultaneousImageTargets_2; }
	inline void set_mMaxSimultaneousImageTargets_2(int32_t value)
	{
		___mMaxSimultaneousImageTargets_2 = value;
	}

	inline static int32_t get_offset_of_mMaxSimultaneousObjectTargets_3() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mMaxSimultaneousObjectTargets_3)); }
	inline int32_t get_mMaxSimultaneousObjectTargets_3() const { return ___mMaxSimultaneousObjectTargets_3; }
	inline int32_t* get_address_of_mMaxSimultaneousObjectTargets_3() { return &___mMaxSimultaneousObjectTargets_3; }
	inline void set_mMaxSimultaneousObjectTargets_3(int32_t value)
	{
		___mMaxSimultaneousObjectTargets_3 = value;
	}

	inline static int32_t get_offset_of_mUseDelayedLoadingObjectTargets_4() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mUseDelayedLoadingObjectTargets_4)); }
	inline bool get_mUseDelayedLoadingObjectTargets_4() const { return ___mUseDelayedLoadingObjectTargets_4; }
	inline bool* get_address_of_mUseDelayedLoadingObjectTargets_4() { return &___mUseDelayedLoadingObjectTargets_4; }
	inline void set_mUseDelayedLoadingObjectTargets_4(bool value)
	{
		___mUseDelayedLoadingObjectTargets_4 = value;
	}

	inline static int32_t get_offset_of_mModelTargetRecoWhileExtendedTracked_5() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mModelTargetRecoWhileExtendedTracked_5)); }
	inline bool get_mModelTargetRecoWhileExtendedTracked_5() const { return ___mModelTargetRecoWhileExtendedTracked_5; }
	inline bool* get_address_of_mModelTargetRecoWhileExtendedTracked_5() { return &___mModelTargetRecoWhileExtendedTracked_5; }
	inline void set_mModelTargetRecoWhileExtendedTracked_5(bool value)
	{
		___mModelTargetRecoWhileExtendedTracked_5 = value;
	}

	inline static int32_t get_offset_of_mWorldCenterMode_6() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mWorldCenterMode_6)); }
	inline int32_t get_mWorldCenterMode_6() const { return ___mWorldCenterMode_6; }
	inline int32_t* get_address_of_mWorldCenterMode_6() { return &___mWorldCenterMode_6; }
	inline void set_mWorldCenterMode_6(int32_t value)
	{
		___mWorldCenterMode_6 = value;
	}

	inline static int32_t get_offset_of_mWorldCenter_7() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mWorldCenter_7)); }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * get_mWorldCenter_7() const { return ___mWorldCenter_7; }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 ** get_address_of_mWorldCenter_7() { return &___mWorldCenter_7; }
	inline void set_mWorldCenter_7(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * value)
	{
		___mWorldCenter_7 = value;
		Il2CppCodeGenWriteBarrier((&___mWorldCenter_7), value);
	}

	inline static int32_t get_offset_of_mVideoBgEventHandlers_8() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mVideoBgEventHandlers_8)); }
	inline List_1_t1314A3DDAE2D8AD81F9403BD961C3CC0530B04A9 * get_mVideoBgEventHandlers_8() const { return ___mVideoBgEventHandlers_8; }
	inline List_1_t1314A3DDAE2D8AD81F9403BD961C3CC0530B04A9 ** get_address_of_mVideoBgEventHandlers_8() { return &___mVideoBgEventHandlers_8; }
	inline void set_mVideoBgEventHandlers_8(List_1_t1314A3DDAE2D8AD81F9403BD961C3CC0530B04A9 * value)
	{
		___mVideoBgEventHandlers_8 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoBgEventHandlers_8), value);
	}

	inline static int32_t get_offset_of_mOnBeforeVuforiaTrackersInitialized_9() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mOnBeforeVuforiaTrackersInitialized_9)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_mOnBeforeVuforiaTrackersInitialized_9() const { return ___mOnBeforeVuforiaTrackersInitialized_9; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_mOnBeforeVuforiaTrackersInitialized_9() { return &___mOnBeforeVuforiaTrackersInitialized_9; }
	inline void set_mOnBeforeVuforiaTrackersInitialized_9(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___mOnBeforeVuforiaTrackersInitialized_9 = value;
		Il2CppCodeGenWriteBarrier((&___mOnBeforeVuforiaTrackersInitialized_9), value);
	}

	inline static int32_t get_offset_of_mOnVuforiaInitialized_10() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mOnVuforiaInitialized_10)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_mOnVuforiaInitialized_10() const { return ___mOnVuforiaInitialized_10; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_mOnVuforiaInitialized_10() { return &___mOnVuforiaInitialized_10; }
	inline void set_mOnVuforiaInitialized_10(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___mOnVuforiaInitialized_10 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuforiaInitialized_10), value);
	}

	inline static int32_t get_offset_of_mOnVuforiaStarted_11() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mOnVuforiaStarted_11)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_mOnVuforiaStarted_11() const { return ___mOnVuforiaStarted_11; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_mOnVuforiaStarted_11() { return &___mOnVuforiaStarted_11; }
	inline void set_mOnVuforiaStarted_11(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___mOnVuforiaStarted_11 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuforiaStarted_11), value);
	}

	inline static int32_t get_offset_of_mOnVuforiaDeinitialized_12() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mOnVuforiaDeinitialized_12)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_mOnVuforiaDeinitialized_12() const { return ___mOnVuforiaDeinitialized_12; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_mOnVuforiaDeinitialized_12() { return &___mOnVuforiaDeinitialized_12; }
	inline void set_mOnVuforiaDeinitialized_12(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___mOnVuforiaDeinitialized_12 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuforiaDeinitialized_12), value);
	}

	inline static int32_t get_offset_of_mOnTrackablesUpdated_13() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mOnTrackablesUpdated_13)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_mOnTrackablesUpdated_13() const { return ___mOnTrackablesUpdated_13; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_mOnTrackablesUpdated_13() { return &___mOnTrackablesUpdated_13; }
	inline void set_mOnTrackablesUpdated_13(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___mOnTrackablesUpdated_13 = value;
		Il2CppCodeGenWriteBarrier((&___mOnTrackablesUpdated_13), value);
	}

	inline static int32_t get_offset_of_mRenderOnUpdate_14() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mRenderOnUpdate_14)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_mRenderOnUpdate_14() const { return ___mRenderOnUpdate_14; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_mRenderOnUpdate_14() { return &___mRenderOnUpdate_14; }
	inline void set_mRenderOnUpdate_14(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___mRenderOnUpdate_14 = value;
		Il2CppCodeGenWriteBarrier((&___mRenderOnUpdate_14), value);
	}

	inline static int32_t get_offset_of_mOnPause_15() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mOnPause_15)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_mOnPause_15() const { return ___mOnPause_15; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_mOnPause_15() { return &___mOnPause_15; }
	inline void set_mOnPause_15(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___mOnPause_15 = value;
		Il2CppCodeGenWriteBarrier((&___mOnPause_15), value);
	}

	inline static int32_t get_offset_of_mPaused_16() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mPaused_16)); }
	inline bool get_mPaused_16() const { return ___mPaused_16; }
	inline bool* get_address_of_mPaused_16() { return &___mPaused_16; }
	inline void set_mPaused_16(bool value)
	{
		___mPaused_16 = value;
	}

	inline static int32_t get_offset_of_mOnBackgroundTextureChanged_17() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mOnBackgroundTextureChanged_17)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_mOnBackgroundTextureChanged_17() const { return ___mOnBackgroundTextureChanged_17; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_mOnBackgroundTextureChanged_17() { return &___mOnBackgroundTextureChanged_17; }
	inline void set_mOnBackgroundTextureChanged_17(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___mOnBackgroundTextureChanged_17 = value;
		Il2CppCodeGenWriteBarrier((&___mOnBackgroundTextureChanged_17), value);
	}

	inline static int32_t get_offset_of_mStartHasBeenInvoked_18() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mStartHasBeenInvoked_18)); }
	inline bool get_mStartHasBeenInvoked_18() const { return ___mStartHasBeenInvoked_18; }
	inline bool* get_address_of_mStartHasBeenInvoked_18() { return &___mStartHasBeenInvoked_18; }
	inline void set_mStartHasBeenInvoked_18(bool value)
	{
		___mStartHasBeenInvoked_18 = value;
	}

	inline static int32_t get_offset_of_mHasStarted_19() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mHasStarted_19)); }
	inline bool get_mHasStarted_19() const { return ___mHasStarted_19; }
	inline bool* get_address_of_mHasStarted_19() { return &___mHasStarted_19; }
	inline void set_mHasStarted_19(bool value)
	{
		___mHasStarted_19 = value;
	}

	inline static int32_t get_offset_of_mCameraConfiguration_20() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mCameraConfiguration_20)); }
	inline RuntimeObject* get_mCameraConfiguration_20() const { return ___mCameraConfiguration_20; }
	inline RuntimeObject** get_address_of_mCameraConfiguration_20() { return &___mCameraConfiguration_20; }
	inline void set_mCameraConfiguration_20(RuntimeObject* value)
	{
		___mCameraConfiguration_20 = value;
		Il2CppCodeGenWriteBarrier((&___mCameraConfiguration_20), value);
	}

	inline static int32_t get_offset_of_mEyewearBehaviour_21() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mEyewearBehaviour_21)); }
	inline DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4 * get_mEyewearBehaviour_21() const { return ___mEyewearBehaviour_21; }
	inline DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4 ** get_address_of_mEyewearBehaviour_21() { return &___mEyewearBehaviour_21; }
	inline void set_mEyewearBehaviour_21(DigitalEyewearARController_t973FDCC2DBCE328656150191FBC1A0E49189D9E4 * value)
	{
		___mEyewearBehaviour_21 = value;
		Il2CppCodeGenWriteBarrier((&___mEyewearBehaviour_21), value);
	}

	inline static int32_t get_offset_of_mCheckStopCamera_22() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mCheckStopCamera_22)); }
	inline bool get_mCheckStopCamera_22() const { return ___mCheckStopCamera_22; }
	inline bool* get_address_of_mCheckStopCamera_22() { return &___mCheckStopCamera_22; }
	inline void set_mCheckStopCamera_22(bool value)
	{
		___mCheckStopCamera_22 = value;
	}

	inline static int32_t get_offset_of_mClearMaterial_23() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mClearMaterial_23)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_mClearMaterial_23() const { return ___mClearMaterial_23; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_mClearMaterial_23() { return &___mClearMaterial_23; }
	inline void set_mClearMaterial_23(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___mClearMaterial_23 = value;
		Il2CppCodeGenWriteBarrier((&___mClearMaterial_23), value);
	}

	inline static int32_t get_offset_of_mMetalRendering_24() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mMetalRendering_24)); }
	inline bool get_mMetalRendering_24() const { return ___mMetalRendering_24; }
	inline bool* get_address_of_mMetalRendering_24() { return &___mMetalRendering_24; }
	inline void set_mMetalRendering_24(bool value)
	{
		___mMetalRendering_24 = value;
	}

	inline static int32_t get_offset_of_mHasStartedOnce_25() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mHasStartedOnce_25)); }
	inline bool get_mHasStartedOnce_25() const { return ___mHasStartedOnce_25; }
	inline bool* get_address_of_mHasStartedOnce_25() { return &___mHasStartedOnce_25; }
	inline void set_mHasStartedOnce_25(bool value)
	{
		___mHasStartedOnce_25 = value;
	}

	inline static int32_t get_offset_of_mWasEnabledBeforePause_26() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mWasEnabledBeforePause_26)); }
	inline bool get_mWasEnabledBeforePause_26() const { return ___mWasEnabledBeforePause_26; }
	inline bool* get_address_of_mWasEnabledBeforePause_26() { return &___mWasEnabledBeforePause_26; }
	inline void set_mWasEnabledBeforePause_26(bool value)
	{
		___mWasEnabledBeforePause_26 = value;
	}

	inline static int32_t get_offset_of_mObjectTrackerWasActiveBeforePause_27() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mObjectTrackerWasActiveBeforePause_27)); }
	inline bool get_mObjectTrackerWasActiveBeforePause_27() const { return ___mObjectTrackerWasActiveBeforePause_27; }
	inline bool* get_address_of_mObjectTrackerWasActiveBeforePause_27() { return &___mObjectTrackerWasActiveBeforePause_27; }
	inline void set_mObjectTrackerWasActiveBeforePause_27(bool value)
	{
		___mObjectTrackerWasActiveBeforePause_27 = value;
	}

	inline static int32_t get_offset_of_mObjectTrackerWasActiveBeforeDisabling_28() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mObjectTrackerWasActiveBeforeDisabling_28)); }
	inline bool get_mObjectTrackerWasActiveBeforeDisabling_28() const { return ___mObjectTrackerWasActiveBeforeDisabling_28; }
	inline bool* get_address_of_mObjectTrackerWasActiveBeforeDisabling_28() { return &___mObjectTrackerWasActiveBeforeDisabling_28; }
	inline void set_mObjectTrackerWasActiveBeforeDisabling_28(bool value)
	{
		___mObjectTrackerWasActiveBeforeDisabling_28 = value;
	}

	inline static int32_t get_offset_of_mLastUpdatedFrame_29() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876, ___mLastUpdatedFrame_29)); }
	inline int32_t get_mLastUpdatedFrame_29() const { return ___mLastUpdatedFrame_29; }
	inline int32_t* get_address_of_mLastUpdatedFrame_29() { return &___mLastUpdatedFrame_29; }
	inline void set_mLastUpdatedFrame_29(int32_t value)
	{
		___mLastUpdatedFrame_29 = value;
	}
};

struct VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876_StaticFields
{
public:
	// Vuforia.VuforiaARController Vuforia.VuforiaARController::mInstance
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876 * ___mInstance_30;
	// System.Object Vuforia.VuforiaARController::mPadlock
	RuntimeObject * ___mPadlock_31;

public:
	inline static int32_t get_offset_of_mInstance_30() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876_StaticFields, ___mInstance_30)); }
	inline VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876 * get_mInstance_30() const { return ___mInstance_30; }
	inline VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876 ** get_address_of_mInstance_30() { return &___mInstance_30; }
	inline void set_mInstance_30(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876 * value)
	{
		___mInstance_30 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_30), value);
	}

	inline static int32_t get_offset_of_mPadlock_31() { return static_cast<int32_t>(offsetof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876_StaticFields, ___mPadlock_31)); }
	inline RuntimeObject * get_mPadlock_31() const { return ___mPadlock_31; }
	inline RuntimeObject ** get_address_of_mPadlock_31() { return &___mPadlock_31; }
	inline void set_mPadlock_31(RuntimeObject * value)
	{
		___mPadlock_31 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAARCONTROLLER_T7732FFB77105A2F5BBEA40E3CECC5C15DA624876_H
#ifndef DEVICETRACKERCONFIGURATION_TC11D2DA49200D3693731D6AFF3F793E4315D1E3F_H
#define DEVICETRACKERCONFIGURATION_TC11D2DA49200D3693731D6AFF3F793E4315D1E3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaConfiguration_DeviceTrackerConfiguration
struct  DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F  : public TrackerConfiguration_t15942AA3CED7034F69729C6FC20AFCD7C078670A
{
public:
	// Vuforia.VuforiaConfiguration_DeviceTrackerConfiguration_ARCoreRequirement Vuforia.VuforiaConfiguration_DeviceTrackerConfiguration::arcoreRequirement
	int32_t ___arcoreRequirement_2;
	// Vuforia.DeviceTracker_TRACKING_MODE Vuforia.VuforiaConfiguration_DeviceTrackerConfiguration::trackingMode
	int32_t ___trackingMode_3;
	// System.Boolean Vuforia.VuforiaConfiguration_DeviceTrackerConfiguration::posePrediction
	bool ___posePrediction_4;
	// Vuforia.RotationalDeviceTracker_MODEL_CORRECTION_MODE Vuforia.VuforiaConfiguration_DeviceTrackerConfiguration::modelCorrectionMode
	int32_t ___modelCorrectionMode_5;
	// System.Boolean Vuforia.VuforiaConfiguration_DeviceTrackerConfiguration::modelTransformEnabled
	bool ___modelTransformEnabled_6;
	// UnityEngine.Vector3 Vuforia.VuforiaConfiguration_DeviceTrackerConfiguration::modelTransform
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___modelTransform_7;

public:
	inline static int32_t get_offset_of_arcoreRequirement_2() { return static_cast<int32_t>(offsetof(DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F, ___arcoreRequirement_2)); }
	inline int32_t get_arcoreRequirement_2() const { return ___arcoreRequirement_2; }
	inline int32_t* get_address_of_arcoreRequirement_2() { return &___arcoreRequirement_2; }
	inline void set_arcoreRequirement_2(int32_t value)
	{
		___arcoreRequirement_2 = value;
	}

	inline static int32_t get_offset_of_trackingMode_3() { return static_cast<int32_t>(offsetof(DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F, ___trackingMode_3)); }
	inline int32_t get_trackingMode_3() const { return ___trackingMode_3; }
	inline int32_t* get_address_of_trackingMode_3() { return &___trackingMode_3; }
	inline void set_trackingMode_3(int32_t value)
	{
		___trackingMode_3 = value;
	}

	inline static int32_t get_offset_of_posePrediction_4() { return static_cast<int32_t>(offsetof(DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F, ___posePrediction_4)); }
	inline bool get_posePrediction_4() const { return ___posePrediction_4; }
	inline bool* get_address_of_posePrediction_4() { return &___posePrediction_4; }
	inline void set_posePrediction_4(bool value)
	{
		___posePrediction_4 = value;
	}

	inline static int32_t get_offset_of_modelCorrectionMode_5() { return static_cast<int32_t>(offsetof(DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F, ___modelCorrectionMode_5)); }
	inline int32_t get_modelCorrectionMode_5() const { return ___modelCorrectionMode_5; }
	inline int32_t* get_address_of_modelCorrectionMode_5() { return &___modelCorrectionMode_5; }
	inline void set_modelCorrectionMode_5(int32_t value)
	{
		___modelCorrectionMode_5 = value;
	}

	inline static int32_t get_offset_of_modelTransformEnabled_6() { return static_cast<int32_t>(offsetof(DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F, ___modelTransformEnabled_6)); }
	inline bool get_modelTransformEnabled_6() const { return ___modelTransformEnabled_6; }
	inline bool* get_address_of_modelTransformEnabled_6() { return &___modelTransformEnabled_6; }
	inline void set_modelTransformEnabled_6(bool value)
	{
		___modelTransformEnabled_6 = value;
	}

	inline static int32_t get_offset_of_modelTransform_7() { return static_cast<int32_t>(offsetof(DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F, ___modelTransform_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_modelTransform_7() const { return ___modelTransform_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_modelTransform_7() { return &___modelTransform_7; }
	inline void set_modelTransform_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___modelTransform_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICETRACKERCONFIGURATION_TC11D2DA49200D3693731D6AFF3F793E4315D1E3F_H
#ifndef DIGITALEYEWEARCONFIGURATION_T1BA464AEDA9C847084FCF4F1CA3C92719D655B18_H
#define DIGITALEYEWEARCONFIGURATION_T1BA464AEDA9C847084FCF4F1CA3C92719D655B18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaConfiguration_DigitalEyewearConfiguration
struct  DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18  : public RuntimeObject
{
public:
	// System.Single Vuforia.VuforiaConfiguration_DigitalEyewearConfiguration::cameraOffset
	float ___cameraOffset_0;
	// System.Int32 Vuforia.VuforiaConfiguration_DigitalEyewearConfiguration::distortionRenderingLayer
	int32_t ___distortionRenderingLayer_1;
	// Vuforia.DigitalEyewearARController_EyewearType Vuforia.VuforiaConfiguration_DigitalEyewearConfiguration::eyewearType
	int32_t ___eyewearType_2;
	// Vuforia.DigitalEyewearARController_StereoFramework Vuforia.VuforiaConfiguration_DigitalEyewearConfiguration::stereoFramework
	int32_t ___stereoFramework_3;
	// Vuforia.DigitalEyewearARController_SeeThroughConfiguration Vuforia.VuforiaConfiguration_DigitalEyewearConfiguration::seeThroughConfiguration
	int32_t ___seeThroughConfiguration_4;
	// System.String Vuforia.VuforiaConfiguration_DigitalEyewearConfiguration::viewerName
	String_t* ___viewerName_5;
	// System.String Vuforia.VuforiaConfiguration_DigitalEyewearConfiguration::viewerManufacturer
	String_t* ___viewerManufacturer_6;
	// System.Boolean Vuforia.VuforiaConfiguration_DigitalEyewearConfiguration::useCustomViewer
	bool ___useCustomViewer_7;
	// Vuforia.DigitalEyewearARController_SerializableViewerParameters Vuforia.VuforiaConfiguration_DigitalEyewearConfiguration::customViewer
	SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9 * ___customViewer_8;

public:
	inline static int32_t get_offset_of_cameraOffset_0() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18, ___cameraOffset_0)); }
	inline float get_cameraOffset_0() const { return ___cameraOffset_0; }
	inline float* get_address_of_cameraOffset_0() { return &___cameraOffset_0; }
	inline void set_cameraOffset_0(float value)
	{
		___cameraOffset_0 = value;
	}

	inline static int32_t get_offset_of_distortionRenderingLayer_1() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18, ___distortionRenderingLayer_1)); }
	inline int32_t get_distortionRenderingLayer_1() const { return ___distortionRenderingLayer_1; }
	inline int32_t* get_address_of_distortionRenderingLayer_1() { return &___distortionRenderingLayer_1; }
	inline void set_distortionRenderingLayer_1(int32_t value)
	{
		___distortionRenderingLayer_1 = value;
	}

	inline static int32_t get_offset_of_eyewearType_2() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18, ___eyewearType_2)); }
	inline int32_t get_eyewearType_2() const { return ___eyewearType_2; }
	inline int32_t* get_address_of_eyewearType_2() { return &___eyewearType_2; }
	inline void set_eyewearType_2(int32_t value)
	{
		___eyewearType_2 = value;
	}

	inline static int32_t get_offset_of_stereoFramework_3() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18, ___stereoFramework_3)); }
	inline int32_t get_stereoFramework_3() const { return ___stereoFramework_3; }
	inline int32_t* get_address_of_stereoFramework_3() { return &___stereoFramework_3; }
	inline void set_stereoFramework_3(int32_t value)
	{
		___stereoFramework_3 = value;
	}

	inline static int32_t get_offset_of_seeThroughConfiguration_4() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18, ___seeThroughConfiguration_4)); }
	inline int32_t get_seeThroughConfiguration_4() const { return ___seeThroughConfiguration_4; }
	inline int32_t* get_address_of_seeThroughConfiguration_4() { return &___seeThroughConfiguration_4; }
	inline void set_seeThroughConfiguration_4(int32_t value)
	{
		___seeThroughConfiguration_4 = value;
	}

	inline static int32_t get_offset_of_viewerName_5() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18, ___viewerName_5)); }
	inline String_t* get_viewerName_5() const { return ___viewerName_5; }
	inline String_t** get_address_of_viewerName_5() { return &___viewerName_5; }
	inline void set_viewerName_5(String_t* value)
	{
		___viewerName_5 = value;
		Il2CppCodeGenWriteBarrier((&___viewerName_5), value);
	}

	inline static int32_t get_offset_of_viewerManufacturer_6() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18, ___viewerManufacturer_6)); }
	inline String_t* get_viewerManufacturer_6() const { return ___viewerManufacturer_6; }
	inline String_t** get_address_of_viewerManufacturer_6() { return &___viewerManufacturer_6; }
	inline void set_viewerManufacturer_6(String_t* value)
	{
		___viewerManufacturer_6 = value;
		Il2CppCodeGenWriteBarrier((&___viewerManufacturer_6), value);
	}

	inline static int32_t get_offset_of_useCustomViewer_7() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18, ___useCustomViewer_7)); }
	inline bool get_useCustomViewer_7() const { return ___useCustomViewer_7; }
	inline bool* get_address_of_useCustomViewer_7() { return &___useCustomViewer_7; }
	inline void set_useCustomViewer_7(bool value)
	{
		___useCustomViewer_7 = value;
	}

	inline static int32_t get_offset_of_customViewer_8() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18, ___customViewer_8)); }
	inline SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9 * get_customViewer_8() const { return ___customViewer_8; }
	inline SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9 ** get_address_of_customViewer_8() { return &___customViewer_8; }
	inline void set_customViewer_8(SerializableViewerParameters_t5F6E8C8CE0B55CCA2E3AC93018053A78AAD5DBE9 * value)
	{
		___customViewer_8 = value;
		Il2CppCodeGenWriteBarrier((&___customViewer_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGITALEYEWEARCONFIGURATION_T1BA464AEDA9C847084FCF4F1CA3C92719D655B18_H
#ifndef GENERICVUFORIACONFIGURATION_T6FBB0036347CB878A938375817103A20CDD59064_H
#define GENERICVUFORIACONFIGURATION_T6FBB0036347CB878A938375817103A20CDD59064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaConfiguration_GenericVuforiaConfiguration
struct  GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064  : public RuntimeObject
{
public:
	// System.String Vuforia.VuforiaConfiguration_GenericVuforiaConfiguration::vuforiaLicenseKey
	String_t* ___vuforiaLicenseKey_1;
	// System.String Vuforia.VuforiaConfiguration_GenericVuforiaConfiguration::ufoLicenseKey
	String_t* ___ufoLicenseKey_2;
	// System.Boolean Vuforia.VuforiaConfiguration_GenericVuforiaConfiguration::delayedInitialization
	bool ___delayedInitialization_3;
	// Vuforia.CameraDevice_CameraDeviceMode Vuforia.VuforiaConfiguration_GenericVuforiaConfiguration::cameraDeviceModeSetting
	int32_t ___cameraDeviceModeSetting_4;
	// System.Int32 Vuforia.VuforiaConfiguration_GenericVuforiaConfiguration::maxSimultaneousImageTargets
	int32_t ___maxSimultaneousImageTargets_5;
	// System.Int32 Vuforia.VuforiaConfiguration_GenericVuforiaConfiguration::maxSimultaneousObjectTargets
	int32_t ___maxSimultaneousObjectTargets_6;
	// System.Boolean Vuforia.VuforiaConfiguration_GenericVuforiaConfiguration::useDelayedLoadingObjectTargets
	bool ___useDelayedLoadingObjectTargets_7;
	// System.Boolean Vuforia.VuforiaConfiguration_GenericVuforiaConfiguration::modelTargetRecoWhileExtendedTracked
	bool ___modelTargetRecoWhileExtendedTracked_8;
	// System.String Vuforia.VuforiaConfiguration_GenericVuforiaConfiguration::version
	String_t* ___version_9;
	// System.String Vuforia.VuforiaConfiguration_GenericVuforiaConfiguration::eulaAcceptedVersions
	String_t* ___eulaAcceptedVersions_10;

public:
	inline static int32_t get_offset_of_vuforiaLicenseKey_1() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064, ___vuforiaLicenseKey_1)); }
	inline String_t* get_vuforiaLicenseKey_1() const { return ___vuforiaLicenseKey_1; }
	inline String_t** get_address_of_vuforiaLicenseKey_1() { return &___vuforiaLicenseKey_1; }
	inline void set_vuforiaLicenseKey_1(String_t* value)
	{
		___vuforiaLicenseKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___vuforiaLicenseKey_1), value);
	}

	inline static int32_t get_offset_of_ufoLicenseKey_2() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064, ___ufoLicenseKey_2)); }
	inline String_t* get_ufoLicenseKey_2() const { return ___ufoLicenseKey_2; }
	inline String_t** get_address_of_ufoLicenseKey_2() { return &___ufoLicenseKey_2; }
	inline void set_ufoLicenseKey_2(String_t* value)
	{
		___ufoLicenseKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___ufoLicenseKey_2), value);
	}

	inline static int32_t get_offset_of_delayedInitialization_3() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064, ___delayedInitialization_3)); }
	inline bool get_delayedInitialization_3() const { return ___delayedInitialization_3; }
	inline bool* get_address_of_delayedInitialization_3() { return &___delayedInitialization_3; }
	inline void set_delayedInitialization_3(bool value)
	{
		___delayedInitialization_3 = value;
	}

	inline static int32_t get_offset_of_cameraDeviceModeSetting_4() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064, ___cameraDeviceModeSetting_4)); }
	inline int32_t get_cameraDeviceModeSetting_4() const { return ___cameraDeviceModeSetting_4; }
	inline int32_t* get_address_of_cameraDeviceModeSetting_4() { return &___cameraDeviceModeSetting_4; }
	inline void set_cameraDeviceModeSetting_4(int32_t value)
	{
		___cameraDeviceModeSetting_4 = value;
	}

	inline static int32_t get_offset_of_maxSimultaneousImageTargets_5() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064, ___maxSimultaneousImageTargets_5)); }
	inline int32_t get_maxSimultaneousImageTargets_5() const { return ___maxSimultaneousImageTargets_5; }
	inline int32_t* get_address_of_maxSimultaneousImageTargets_5() { return &___maxSimultaneousImageTargets_5; }
	inline void set_maxSimultaneousImageTargets_5(int32_t value)
	{
		___maxSimultaneousImageTargets_5 = value;
	}

	inline static int32_t get_offset_of_maxSimultaneousObjectTargets_6() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064, ___maxSimultaneousObjectTargets_6)); }
	inline int32_t get_maxSimultaneousObjectTargets_6() const { return ___maxSimultaneousObjectTargets_6; }
	inline int32_t* get_address_of_maxSimultaneousObjectTargets_6() { return &___maxSimultaneousObjectTargets_6; }
	inline void set_maxSimultaneousObjectTargets_6(int32_t value)
	{
		___maxSimultaneousObjectTargets_6 = value;
	}

	inline static int32_t get_offset_of_useDelayedLoadingObjectTargets_7() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064, ___useDelayedLoadingObjectTargets_7)); }
	inline bool get_useDelayedLoadingObjectTargets_7() const { return ___useDelayedLoadingObjectTargets_7; }
	inline bool* get_address_of_useDelayedLoadingObjectTargets_7() { return &___useDelayedLoadingObjectTargets_7; }
	inline void set_useDelayedLoadingObjectTargets_7(bool value)
	{
		___useDelayedLoadingObjectTargets_7 = value;
	}

	inline static int32_t get_offset_of_modelTargetRecoWhileExtendedTracked_8() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064, ___modelTargetRecoWhileExtendedTracked_8)); }
	inline bool get_modelTargetRecoWhileExtendedTracked_8() const { return ___modelTargetRecoWhileExtendedTracked_8; }
	inline bool* get_address_of_modelTargetRecoWhileExtendedTracked_8() { return &___modelTargetRecoWhileExtendedTracked_8; }
	inline void set_modelTargetRecoWhileExtendedTracked_8(bool value)
	{
		___modelTargetRecoWhileExtendedTracked_8 = value;
	}

	inline static int32_t get_offset_of_version_9() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064, ___version_9)); }
	inline String_t* get_version_9() const { return ___version_9; }
	inline String_t** get_address_of_version_9() { return &___version_9; }
	inline void set_version_9(String_t* value)
	{
		___version_9 = value;
		Il2CppCodeGenWriteBarrier((&___version_9), value);
	}

	inline static int32_t get_offset_of_eulaAcceptedVersions_10() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064, ___eulaAcceptedVersions_10)); }
	inline String_t* get_eulaAcceptedVersions_10() const { return ___eulaAcceptedVersions_10; }
	inline String_t** get_address_of_eulaAcceptedVersions_10() { return &___eulaAcceptedVersions_10; }
	inline void set_eulaAcceptedVersions_10(String_t* value)
	{
		___eulaAcceptedVersions_10 = value;
		Il2CppCodeGenWriteBarrier((&___eulaAcceptedVersions_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICVUFORIACONFIGURATION_T6FBB0036347CB878A938375817103A20CDD59064_H
#ifndef VIDEOBACKGROUNDCONFIGURATION_TCC24E374B966B79D018C14F6807A6DDA47302F17_H
#define VIDEOBACKGROUNDCONFIGURATION_TCC24E374B966B79D018C14F6807A6DDA47302F17_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaConfiguration_VideoBackgroundConfiguration
struct  VideoBackgroundConfiguration_tCC24E374B966B79D018C14F6807A6DDA47302F17  : public RuntimeObject
{
public:
	// Vuforia.HideExcessAreaUtility_CLIPPING_MODE Vuforia.VuforiaConfiguration_VideoBackgroundConfiguration::clippingMode
	int32_t ___clippingMode_0;
	// System.Int32 Vuforia.VuforiaConfiguration_VideoBackgroundConfiguration::numDivisions
	int32_t ___numDivisions_1;
	// UnityEngine.Shader Vuforia.VuforiaConfiguration_VideoBackgroundConfiguration::videoBackgroundShader
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___videoBackgroundShader_2;
	// UnityEngine.Shader Vuforia.VuforiaConfiguration_VideoBackgroundConfiguration::matteShader
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___matteShader_3;
	// System.Boolean Vuforia.VuforiaConfiguration_VideoBackgroundConfiguration::videoBackgroundEnabled
	bool ___videoBackgroundEnabled_4;

public:
	inline static int32_t get_offset_of_clippingMode_0() { return static_cast<int32_t>(offsetof(VideoBackgroundConfiguration_tCC24E374B966B79D018C14F6807A6DDA47302F17, ___clippingMode_0)); }
	inline int32_t get_clippingMode_0() const { return ___clippingMode_0; }
	inline int32_t* get_address_of_clippingMode_0() { return &___clippingMode_0; }
	inline void set_clippingMode_0(int32_t value)
	{
		___clippingMode_0 = value;
	}

	inline static int32_t get_offset_of_numDivisions_1() { return static_cast<int32_t>(offsetof(VideoBackgroundConfiguration_tCC24E374B966B79D018C14F6807A6DDA47302F17, ___numDivisions_1)); }
	inline int32_t get_numDivisions_1() const { return ___numDivisions_1; }
	inline int32_t* get_address_of_numDivisions_1() { return &___numDivisions_1; }
	inline void set_numDivisions_1(int32_t value)
	{
		___numDivisions_1 = value;
	}

	inline static int32_t get_offset_of_videoBackgroundShader_2() { return static_cast<int32_t>(offsetof(VideoBackgroundConfiguration_tCC24E374B966B79D018C14F6807A6DDA47302F17, ___videoBackgroundShader_2)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_videoBackgroundShader_2() const { return ___videoBackgroundShader_2; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_videoBackgroundShader_2() { return &___videoBackgroundShader_2; }
	inline void set_videoBackgroundShader_2(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___videoBackgroundShader_2 = value;
		Il2CppCodeGenWriteBarrier((&___videoBackgroundShader_2), value);
	}

	inline static int32_t get_offset_of_matteShader_3() { return static_cast<int32_t>(offsetof(VideoBackgroundConfiguration_tCC24E374B966B79D018C14F6807A6DDA47302F17, ___matteShader_3)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_matteShader_3() const { return ___matteShader_3; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_matteShader_3() { return &___matteShader_3; }
	inline void set_matteShader_3(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___matteShader_3 = value;
		Il2CppCodeGenWriteBarrier((&___matteShader_3), value);
	}

	inline static int32_t get_offset_of_videoBackgroundEnabled_4() { return static_cast<int32_t>(offsetof(VideoBackgroundConfiguration_tCC24E374B966B79D018C14F6807A6DDA47302F17, ___videoBackgroundEnabled_4)); }
	inline bool get_videoBackgroundEnabled_4() const { return ___videoBackgroundEnabled_4; }
	inline bool* get_address_of_videoBackgroundEnabled_4() { return &___videoBackgroundEnabled_4; }
	inline void set_videoBackgroundEnabled_4(bool value)
	{
		___videoBackgroundEnabled_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDCONFIGURATION_TCC24E374B966B79D018C14F6807A6DDA47302F17_H
#ifndef VUFORIAMANAGER_T56F310BC8B59799D4A44824C42168D133AE095F7_H
#define VUFORIAMANAGER_T56F310BC8B59799D4A44824C42168D133AE095F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManager
struct  VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7  : public RuntimeObject
{
public:
	// Vuforia.VuforiaARController_WorldCenterMode Vuforia.VuforiaManager::mWorldCenterMode
	int32_t ___mWorldCenterMode_1;
	// Vuforia.WorldCenterTrackableBehaviour Vuforia.VuforiaManager::mWorldCenter
	RuntimeObject* ___mWorldCenter_2;
	// Vuforia.VuMarkBehaviour Vuforia.VuforiaManager::mVuMarkWorldCenter
	VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4 * ___mVuMarkWorldCenter_3;
	// UnityEngine.Transform Vuforia.VuforiaManager::mARCameraTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___mARCameraTransform_4;
	// UnityEngine.Transform Vuforia.VuforiaManager::mCentralAnchorPoint
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___mCentralAnchorPoint_5;
	// Vuforia.TrackerData_TrackableResultData[] Vuforia.VuforiaManager::mTrackableResultDataArray
	TrackableResultDataU5BU5D_t9F14AA7BB2B4B86B497397AA4ABC42C964D0477D* ___mTrackableResultDataArray_6;
	// Vuforia.TrackerData_VuMarkTargetData[] Vuforia.VuforiaManager::mVuMarkDataArray
	VuMarkTargetDataU5BU5D_tDEF295778F1EBA00260FE5F7CF00E0E91F5E3605* ___mVuMarkDataArray_7;
	// Vuforia.TrackerData_VuMarkTargetResultData[] Vuforia.VuforiaManager::mVuMarkResultDataArray
	VuMarkTargetResultDataU5BU5D_t0AAD90D01B7B61C91A7B4DE994F065D01013FF9A* ___mVuMarkResultDataArray_8;
	// System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManager_TrackableIdPair> Vuforia.VuforiaManager::mWCTrackableFoundQueue
	LinkedList_1_tE86603FF5BEA21BD62849C673324AEE74A31E58A * ___mWCTrackableFoundQueue_9;
	// System.Collections.Generic.Dictionary`2<Vuforia.PIXEL_FORMAT,Vuforia.UnmanagedImage> Vuforia.VuforiaManager::mCameraImages
	Dictionary_2_tD36561317F15A4463B66EFAA17CF49A315A67587 * ___mCameraImages_10;
	// System.IntPtr Vuforia.VuforiaManager::mImageData
	intptr_t ___mImageData_11;
	// System.Int32 Vuforia.VuforiaManager::mInjectedFrameIdx
	int32_t ___mInjectedFrameIdx_12;
	// System.IntPtr Vuforia.VuforiaManager::mLastProcessedFrameStatePtr
	intptr_t ___mLastProcessedFrameStatePtr_13;
	// System.Boolean Vuforia.VuforiaManager::mInitialized
	bool ___mInitialized_14;
	// System.Boolean Vuforia.VuforiaManager::mPaused
	bool ___mPaused_15;
	// Vuforia.TrackerData_FrameState Vuforia.VuforiaManager::mFrameState
	FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0  ___mFrameState_16;
	// Vuforia.VuforiaManager_AutoRotationState Vuforia.VuforiaManager::mAutoRotationState
	AutoRotationState_t7763EA1A2607ECEF69E2D2438C471FE93451E508  ___mAutoRotationState_17;
	// System.Int32 Vuforia.VuforiaManager::mLastFrameIdx
	int32_t ___mLastFrameIdx_18;
	// System.Boolean Vuforia.VuforiaManager::mIsSeeThroughDevice
	bool ___mIsSeeThroughDevice_19;
	// Vuforia.LateLatchingManager Vuforia.VuforiaManager::mLateLatchingManager
	LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C * ___mLateLatchingManager_20;
	// Vuforia.CameraCalibrationComparer Vuforia.VuforiaManager::mCameraCalibrationComparer
	CameraCalibrationComparer_t7CF72E6611106D56BB040F32DB6D7BFF0898F7EA * ___mCameraCalibrationComparer_21;
	// UnityEngine.Vector3 Vuforia.VuforiaManager::mPosePositionalOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mPosePositionalOffset_22;
	// UnityEngine.Quaternion Vuforia.VuforiaManager::mPoseRotationalOffset
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___mPoseRotationalOffset_23;

public:
	inline static int32_t get_offset_of_mWorldCenterMode_1() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mWorldCenterMode_1)); }
	inline int32_t get_mWorldCenterMode_1() const { return ___mWorldCenterMode_1; }
	inline int32_t* get_address_of_mWorldCenterMode_1() { return &___mWorldCenterMode_1; }
	inline void set_mWorldCenterMode_1(int32_t value)
	{
		___mWorldCenterMode_1 = value;
	}

	inline static int32_t get_offset_of_mWorldCenter_2() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mWorldCenter_2)); }
	inline RuntimeObject* get_mWorldCenter_2() const { return ___mWorldCenter_2; }
	inline RuntimeObject** get_address_of_mWorldCenter_2() { return &___mWorldCenter_2; }
	inline void set_mWorldCenter_2(RuntimeObject* value)
	{
		___mWorldCenter_2 = value;
		Il2CppCodeGenWriteBarrier((&___mWorldCenter_2), value);
	}

	inline static int32_t get_offset_of_mVuMarkWorldCenter_3() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mVuMarkWorldCenter_3)); }
	inline VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4 * get_mVuMarkWorldCenter_3() const { return ___mVuMarkWorldCenter_3; }
	inline VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4 ** get_address_of_mVuMarkWorldCenter_3() { return &___mVuMarkWorldCenter_3; }
	inline void set_mVuMarkWorldCenter_3(VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4 * value)
	{
		___mVuMarkWorldCenter_3 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkWorldCenter_3), value);
	}

	inline static int32_t get_offset_of_mARCameraTransform_4() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mARCameraTransform_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_mARCameraTransform_4() const { return ___mARCameraTransform_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_mARCameraTransform_4() { return &___mARCameraTransform_4; }
	inline void set_mARCameraTransform_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___mARCameraTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___mARCameraTransform_4), value);
	}

	inline static int32_t get_offset_of_mCentralAnchorPoint_5() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mCentralAnchorPoint_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_mCentralAnchorPoint_5() const { return ___mCentralAnchorPoint_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_mCentralAnchorPoint_5() { return &___mCentralAnchorPoint_5; }
	inline void set_mCentralAnchorPoint_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___mCentralAnchorPoint_5 = value;
		Il2CppCodeGenWriteBarrier((&___mCentralAnchorPoint_5), value);
	}

	inline static int32_t get_offset_of_mTrackableResultDataArray_6() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mTrackableResultDataArray_6)); }
	inline TrackableResultDataU5BU5D_t9F14AA7BB2B4B86B497397AA4ABC42C964D0477D* get_mTrackableResultDataArray_6() const { return ___mTrackableResultDataArray_6; }
	inline TrackableResultDataU5BU5D_t9F14AA7BB2B4B86B497397AA4ABC42C964D0477D** get_address_of_mTrackableResultDataArray_6() { return &___mTrackableResultDataArray_6; }
	inline void set_mTrackableResultDataArray_6(TrackableResultDataU5BU5D_t9F14AA7BB2B4B86B497397AA4ABC42C964D0477D* value)
	{
		___mTrackableResultDataArray_6 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableResultDataArray_6), value);
	}

	inline static int32_t get_offset_of_mVuMarkDataArray_7() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mVuMarkDataArray_7)); }
	inline VuMarkTargetDataU5BU5D_tDEF295778F1EBA00260FE5F7CF00E0E91F5E3605* get_mVuMarkDataArray_7() const { return ___mVuMarkDataArray_7; }
	inline VuMarkTargetDataU5BU5D_tDEF295778F1EBA00260FE5F7CF00E0E91F5E3605** get_address_of_mVuMarkDataArray_7() { return &___mVuMarkDataArray_7; }
	inline void set_mVuMarkDataArray_7(VuMarkTargetDataU5BU5D_tDEF295778F1EBA00260FE5F7CF00E0E91F5E3605* value)
	{
		___mVuMarkDataArray_7 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkDataArray_7), value);
	}

	inline static int32_t get_offset_of_mVuMarkResultDataArray_8() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mVuMarkResultDataArray_8)); }
	inline VuMarkTargetResultDataU5BU5D_t0AAD90D01B7B61C91A7B4DE994F065D01013FF9A* get_mVuMarkResultDataArray_8() const { return ___mVuMarkResultDataArray_8; }
	inline VuMarkTargetResultDataU5BU5D_t0AAD90D01B7B61C91A7B4DE994F065D01013FF9A** get_address_of_mVuMarkResultDataArray_8() { return &___mVuMarkResultDataArray_8; }
	inline void set_mVuMarkResultDataArray_8(VuMarkTargetResultDataU5BU5D_t0AAD90D01B7B61C91A7B4DE994F065D01013FF9A* value)
	{
		___mVuMarkResultDataArray_8 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkResultDataArray_8), value);
	}

	inline static int32_t get_offset_of_mWCTrackableFoundQueue_9() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mWCTrackableFoundQueue_9)); }
	inline LinkedList_1_tE86603FF5BEA21BD62849C673324AEE74A31E58A * get_mWCTrackableFoundQueue_9() const { return ___mWCTrackableFoundQueue_9; }
	inline LinkedList_1_tE86603FF5BEA21BD62849C673324AEE74A31E58A ** get_address_of_mWCTrackableFoundQueue_9() { return &___mWCTrackableFoundQueue_9; }
	inline void set_mWCTrackableFoundQueue_9(LinkedList_1_tE86603FF5BEA21BD62849C673324AEE74A31E58A * value)
	{
		___mWCTrackableFoundQueue_9 = value;
		Il2CppCodeGenWriteBarrier((&___mWCTrackableFoundQueue_9), value);
	}

	inline static int32_t get_offset_of_mCameraImages_10() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mCameraImages_10)); }
	inline Dictionary_2_tD36561317F15A4463B66EFAA17CF49A315A67587 * get_mCameraImages_10() const { return ___mCameraImages_10; }
	inline Dictionary_2_tD36561317F15A4463B66EFAA17CF49A315A67587 ** get_address_of_mCameraImages_10() { return &___mCameraImages_10; }
	inline void set_mCameraImages_10(Dictionary_2_tD36561317F15A4463B66EFAA17CF49A315A67587 * value)
	{
		___mCameraImages_10 = value;
		Il2CppCodeGenWriteBarrier((&___mCameraImages_10), value);
	}

	inline static int32_t get_offset_of_mImageData_11() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mImageData_11)); }
	inline intptr_t get_mImageData_11() const { return ___mImageData_11; }
	inline intptr_t* get_address_of_mImageData_11() { return &___mImageData_11; }
	inline void set_mImageData_11(intptr_t value)
	{
		___mImageData_11 = value;
	}

	inline static int32_t get_offset_of_mInjectedFrameIdx_12() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mInjectedFrameIdx_12)); }
	inline int32_t get_mInjectedFrameIdx_12() const { return ___mInjectedFrameIdx_12; }
	inline int32_t* get_address_of_mInjectedFrameIdx_12() { return &___mInjectedFrameIdx_12; }
	inline void set_mInjectedFrameIdx_12(int32_t value)
	{
		___mInjectedFrameIdx_12 = value;
	}

	inline static int32_t get_offset_of_mLastProcessedFrameStatePtr_13() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mLastProcessedFrameStatePtr_13)); }
	inline intptr_t get_mLastProcessedFrameStatePtr_13() const { return ___mLastProcessedFrameStatePtr_13; }
	inline intptr_t* get_address_of_mLastProcessedFrameStatePtr_13() { return &___mLastProcessedFrameStatePtr_13; }
	inline void set_mLastProcessedFrameStatePtr_13(intptr_t value)
	{
		___mLastProcessedFrameStatePtr_13 = value;
	}

	inline static int32_t get_offset_of_mInitialized_14() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mInitialized_14)); }
	inline bool get_mInitialized_14() const { return ___mInitialized_14; }
	inline bool* get_address_of_mInitialized_14() { return &___mInitialized_14; }
	inline void set_mInitialized_14(bool value)
	{
		___mInitialized_14 = value;
	}

	inline static int32_t get_offset_of_mPaused_15() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mPaused_15)); }
	inline bool get_mPaused_15() const { return ___mPaused_15; }
	inline bool* get_address_of_mPaused_15() { return &___mPaused_15; }
	inline void set_mPaused_15(bool value)
	{
		___mPaused_15 = value;
	}

	inline static int32_t get_offset_of_mFrameState_16() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mFrameState_16)); }
	inline FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0  get_mFrameState_16() const { return ___mFrameState_16; }
	inline FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0 * get_address_of_mFrameState_16() { return &___mFrameState_16; }
	inline void set_mFrameState_16(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0  value)
	{
		___mFrameState_16 = value;
	}

	inline static int32_t get_offset_of_mAutoRotationState_17() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mAutoRotationState_17)); }
	inline AutoRotationState_t7763EA1A2607ECEF69E2D2438C471FE93451E508  get_mAutoRotationState_17() const { return ___mAutoRotationState_17; }
	inline AutoRotationState_t7763EA1A2607ECEF69E2D2438C471FE93451E508 * get_address_of_mAutoRotationState_17() { return &___mAutoRotationState_17; }
	inline void set_mAutoRotationState_17(AutoRotationState_t7763EA1A2607ECEF69E2D2438C471FE93451E508  value)
	{
		___mAutoRotationState_17 = value;
	}

	inline static int32_t get_offset_of_mLastFrameIdx_18() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mLastFrameIdx_18)); }
	inline int32_t get_mLastFrameIdx_18() const { return ___mLastFrameIdx_18; }
	inline int32_t* get_address_of_mLastFrameIdx_18() { return &___mLastFrameIdx_18; }
	inline void set_mLastFrameIdx_18(int32_t value)
	{
		___mLastFrameIdx_18 = value;
	}

	inline static int32_t get_offset_of_mIsSeeThroughDevice_19() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mIsSeeThroughDevice_19)); }
	inline bool get_mIsSeeThroughDevice_19() const { return ___mIsSeeThroughDevice_19; }
	inline bool* get_address_of_mIsSeeThroughDevice_19() { return &___mIsSeeThroughDevice_19; }
	inline void set_mIsSeeThroughDevice_19(bool value)
	{
		___mIsSeeThroughDevice_19 = value;
	}

	inline static int32_t get_offset_of_mLateLatchingManager_20() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mLateLatchingManager_20)); }
	inline LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C * get_mLateLatchingManager_20() const { return ___mLateLatchingManager_20; }
	inline LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C ** get_address_of_mLateLatchingManager_20() { return &___mLateLatchingManager_20; }
	inline void set_mLateLatchingManager_20(LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C * value)
	{
		___mLateLatchingManager_20 = value;
		Il2CppCodeGenWriteBarrier((&___mLateLatchingManager_20), value);
	}

	inline static int32_t get_offset_of_mCameraCalibrationComparer_21() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mCameraCalibrationComparer_21)); }
	inline CameraCalibrationComparer_t7CF72E6611106D56BB040F32DB6D7BFF0898F7EA * get_mCameraCalibrationComparer_21() const { return ___mCameraCalibrationComparer_21; }
	inline CameraCalibrationComparer_t7CF72E6611106D56BB040F32DB6D7BFF0898F7EA ** get_address_of_mCameraCalibrationComparer_21() { return &___mCameraCalibrationComparer_21; }
	inline void set_mCameraCalibrationComparer_21(CameraCalibrationComparer_t7CF72E6611106D56BB040F32DB6D7BFF0898F7EA * value)
	{
		___mCameraCalibrationComparer_21 = value;
		Il2CppCodeGenWriteBarrier((&___mCameraCalibrationComparer_21), value);
	}

	inline static int32_t get_offset_of_mPosePositionalOffset_22() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mPosePositionalOffset_22)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mPosePositionalOffset_22() const { return ___mPosePositionalOffset_22; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mPosePositionalOffset_22() { return &___mPosePositionalOffset_22; }
	inline void set_mPosePositionalOffset_22(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mPosePositionalOffset_22 = value;
	}

	inline static int32_t get_offset_of_mPoseRotationalOffset_23() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7, ___mPoseRotationalOffset_23)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_mPoseRotationalOffset_23() const { return ___mPoseRotationalOffset_23; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_mPoseRotationalOffset_23() { return &___mPoseRotationalOffset_23; }
	inline void set_mPoseRotationalOffset_23(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___mPoseRotationalOffset_23 = value;
	}
};

struct VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7_StaticFields
{
public:
	// Vuforia.VuforiaManager Vuforia.VuforiaManager::sInstance
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7 * ___sInstance_0;

public:
	inline static int32_t get_offset_of_sInstance_0() { return static_cast<int32_t>(offsetof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7_StaticFields, ___sInstance_0)); }
	inline VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7 * get_sInstance_0() const { return ___sInstance_0; }
	inline VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7 ** get_address_of_sInstance_0() { return &___sInstance_0; }
	inline void set_sInstance_0(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7 * value)
	{
		___sInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMANAGER_T56F310BC8B59799D4A44824C42168D133AE095F7_H
#ifndef U3CU3EC__DISPLAYCLASS60_0_T9B16CEDAB1D664130D52803188ADF3EF78ED78A3_H
#define U3CU3EC__DISPLAYCLASS60_0_T9B16CEDAB1D664130D52803188ADF3EF78ED78A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaManager_<>c__DisplayClass60_0
struct  U3CU3Ec__DisplayClass60_0_t9B16CEDAB1D664130D52803188ADF3EF78ED78A3  : public RuntimeObject
{
public:
	// Vuforia.TrackerData_FrameState Vuforia.VuforiaManager_<>c__DisplayClass60_0::frameState
	FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0  ___frameState_0;

public:
	inline static int32_t get_offset_of_frameState_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass60_0_t9B16CEDAB1D664130D52803188ADF3EF78ED78A3, ___frameState_0)); }
	inline FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0  get_frameState_0() const { return ___frameState_0; }
	inline FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0 * get_address_of_frameState_0() { return &___frameState_0; }
	inline void set_frameState_0(FrameState_t40C5EF3D259514DAB23DEDCD72218A934C5645E0  value)
	{
		___frameState_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS60_0_T9B16CEDAB1D664130D52803188ADF3EF78ED78A3_H
#ifndef VUFORIARENDERER_T76A570ACCA3476064DB729CCDAF59CF0BAB41081_H
#define VUFORIARENDERER_T76A570ACCA3476064DB729CCDAF59CF0BAB41081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer
struct  VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081  : public RuntimeObject
{
public:
	// Vuforia.VuforiaRenderer_VideoBGCfgData Vuforia.VuforiaRenderer::mVideoBGConfig
	VideoBGCfgData_t4FD9E3E825BD3AF119DFAE696F50C478336B1A9A  ___mVideoBGConfig_1;
	// System.Boolean Vuforia.VuforiaRenderer::mVideoBGConfigSet
	bool ___mVideoBGConfigSet_2;
	// UnityEngine.Texture Vuforia.VuforiaRenderer::mVideoBackgroundTexture
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___mVideoBackgroundTexture_3;
	// System.IntPtr Vuforia.VuforiaRenderer::mNativeRenderingCallback
	intptr_t ___mNativeRenderingCallback_4;
	// System.Boolean Vuforia.VuforiaRenderer::mBackgroundTextureHasChanged
	bool ___mBackgroundTextureHasChanged_5;
	// System.Func`1<System.Boolean> Vuforia.VuforiaRenderer::mLegacyRenderingCondition
	Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * ___mLegacyRenderingCondition_6;

public:
	inline static int32_t get_offset_of_mVideoBGConfig_1() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081, ___mVideoBGConfig_1)); }
	inline VideoBGCfgData_t4FD9E3E825BD3AF119DFAE696F50C478336B1A9A  get_mVideoBGConfig_1() const { return ___mVideoBGConfig_1; }
	inline VideoBGCfgData_t4FD9E3E825BD3AF119DFAE696F50C478336B1A9A * get_address_of_mVideoBGConfig_1() { return &___mVideoBGConfig_1; }
	inline void set_mVideoBGConfig_1(VideoBGCfgData_t4FD9E3E825BD3AF119DFAE696F50C478336B1A9A  value)
	{
		___mVideoBGConfig_1 = value;
	}

	inline static int32_t get_offset_of_mVideoBGConfigSet_2() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081, ___mVideoBGConfigSet_2)); }
	inline bool get_mVideoBGConfigSet_2() const { return ___mVideoBGConfigSet_2; }
	inline bool* get_address_of_mVideoBGConfigSet_2() { return &___mVideoBGConfigSet_2; }
	inline void set_mVideoBGConfigSet_2(bool value)
	{
		___mVideoBGConfigSet_2 = value;
	}

	inline static int32_t get_offset_of_mVideoBackgroundTexture_3() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081, ___mVideoBackgroundTexture_3)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_mVideoBackgroundTexture_3() const { return ___mVideoBackgroundTexture_3; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_mVideoBackgroundTexture_3() { return &___mVideoBackgroundTexture_3; }
	inline void set_mVideoBackgroundTexture_3(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___mVideoBackgroundTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoBackgroundTexture_3), value);
	}

	inline static int32_t get_offset_of_mNativeRenderingCallback_4() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081, ___mNativeRenderingCallback_4)); }
	inline intptr_t get_mNativeRenderingCallback_4() const { return ___mNativeRenderingCallback_4; }
	inline intptr_t* get_address_of_mNativeRenderingCallback_4() { return &___mNativeRenderingCallback_4; }
	inline void set_mNativeRenderingCallback_4(intptr_t value)
	{
		___mNativeRenderingCallback_4 = value;
	}

	inline static int32_t get_offset_of_mBackgroundTextureHasChanged_5() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081, ___mBackgroundTextureHasChanged_5)); }
	inline bool get_mBackgroundTextureHasChanged_5() const { return ___mBackgroundTextureHasChanged_5; }
	inline bool* get_address_of_mBackgroundTextureHasChanged_5() { return &___mBackgroundTextureHasChanged_5; }
	inline void set_mBackgroundTextureHasChanged_5(bool value)
	{
		___mBackgroundTextureHasChanged_5 = value;
	}

	inline static int32_t get_offset_of_mLegacyRenderingCondition_6() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081, ___mLegacyRenderingCondition_6)); }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * get_mLegacyRenderingCondition_6() const { return ___mLegacyRenderingCondition_6; }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 ** get_address_of_mLegacyRenderingCondition_6() { return &___mLegacyRenderingCondition_6; }
	inline void set_mLegacyRenderingCondition_6(Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * value)
	{
		___mLegacyRenderingCondition_6 = value;
		Il2CppCodeGenWriteBarrier((&___mLegacyRenderingCondition_6), value);
	}
};

struct VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081_StaticFields
{
public:
	// Vuforia.VuforiaRenderer Vuforia.VuforiaRenderer::sInstance
	VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081 * ___sInstance_0;

public:
	inline static int32_t get_offset_of_sInstance_0() { return static_cast<int32_t>(offsetof(VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081_StaticFields, ___sInstance_0)); }
	inline VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081 * get_sInstance_0() const { return ___sInstance_0; }
	inline VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081 ** get_address_of_sInstance_0() { return &___sInstance_0; }
	inline void set_sInstance_0(VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081 * value)
	{
		___sInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___sInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIARENDERER_T76A570ACCA3476064DB729CCDAF59CF0BAB41081_H
#ifndef VUFORIARUNTIME_T12D46DC6B127017866AA832739B2B22B0612DC1D_H
#define VUFORIARUNTIME_T12D46DC6B127017866AA832739B2B22B0612DC1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntime
struct  VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D  : public RuntimeObject
{
public:
	// System.Action`1<Vuforia.VuforiaUnity_InitError> Vuforia.VuforiaRuntime::mOnVuforiaInitError
	Action_1_tAA7EF8692AA276A9B2E6353FC26F374339933EFF * ___mOnVuforiaInitError_1;
	// System.Boolean Vuforia.VuforiaRuntime::mFailedToInitialize
	bool ___mFailedToInitialize_2;
	// Vuforia.VuforiaUnity_InitError Vuforia.VuforiaRuntime::mInitError
	int32_t ___mInitError_3;
	// Vuforia.VuforiaRuntime_InitState Vuforia.VuforiaRuntime::mInitState
	int32_t ___mInitState_4;
	// System.Int32 Vuforia.VuforiaRuntime::mTimesPermissionCheckFailed
	int32_t ___mTimesPermissionCheckFailed_5;
	// System.Boolean Vuforia.VuforiaRuntime::mInitThreadReturned
	bool ___mInitThreadReturned_6;
	// Vuforia.VuforiaUnity_InitError Vuforia.VuforiaRuntime::mReturnedError
	int32_t ___mReturnedError_7;
	// System.Boolean Vuforia.VuforiaRuntime::mAppIsQuitting
	bool ___mAppIsQuitting_10;

public:
	inline static int32_t get_offset_of_mOnVuforiaInitError_1() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D, ___mOnVuforiaInitError_1)); }
	inline Action_1_tAA7EF8692AA276A9B2E6353FC26F374339933EFF * get_mOnVuforiaInitError_1() const { return ___mOnVuforiaInitError_1; }
	inline Action_1_tAA7EF8692AA276A9B2E6353FC26F374339933EFF ** get_address_of_mOnVuforiaInitError_1() { return &___mOnVuforiaInitError_1; }
	inline void set_mOnVuforiaInitError_1(Action_1_tAA7EF8692AA276A9B2E6353FC26F374339933EFF * value)
	{
		___mOnVuforiaInitError_1 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuforiaInitError_1), value);
	}

	inline static int32_t get_offset_of_mFailedToInitialize_2() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D, ___mFailedToInitialize_2)); }
	inline bool get_mFailedToInitialize_2() const { return ___mFailedToInitialize_2; }
	inline bool* get_address_of_mFailedToInitialize_2() { return &___mFailedToInitialize_2; }
	inline void set_mFailedToInitialize_2(bool value)
	{
		___mFailedToInitialize_2 = value;
	}

	inline static int32_t get_offset_of_mInitError_3() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D, ___mInitError_3)); }
	inline int32_t get_mInitError_3() const { return ___mInitError_3; }
	inline int32_t* get_address_of_mInitError_3() { return &___mInitError_3; }
	inline void set_mInitError_3(int32_t value)
	{
		___mInitError_3 = value;
	}

	inline static int32_t get_offset_of_mInitState_4() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D, ___mInitState_4)); }
	inline int32_t get_mInitState_4() const { return ___mInitState_4; }
	inline int32_t* get_address_of_mInitState_4() { return &___mInitState_4; }
	inline void set_mInitState_4(int32_t value)
	{
		___mInitState_4 = value;
	}

	inline static int32_t get_offset_of_mTimesPermissionCheckFailed_5() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D, ___mTimesPermissionCheckFailed_5)); }
	inline int32_t get_mTimesPermissionCheckFailed_5() const { return ___mTimesPermissionCheckFailed_5; }
	inline int32_t* get_address_of_mTimesPermissionCheckFailed_5() { return &___mTimesPermissionCheckFailed_5; }
	inline void set_mTimesPermissionCheckFailed_5(int32_t value)
	{
		___mTimesPermissionCheckFailed_5 = value;
	}

	inline static int32_t get_offset_of_mInitThreadReturned_6() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D, ___mInitThreadReturned_6)); }
	inline bool get_mInitThreadReturned_6() const { return ___mInitThreadReturned_6; }
	inline bool* get_address_of_mInitThreadReturned_6() { return &___mInitThreadReturned_6; }
	inline void set_mInitThreadReturned_6(bool value)
	{
		___mInitThreadReturned_6 = value;
	}

	inline static int32_t get_offset_of_mReturnedError_7() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D, ___mReturnedError_7)); }
	inline int32_t get_mReturnedError_7() const { return ___mReturnedError_7; }
	inline int32_t* get_address_of_mReturnedError_7() { return &___mReturnedError_7; }
	inline void set_mReturnedError_7(int32_t value)
	{
		___mReturnedError_7 = value;
	}

	inline static int32_t get_offset_of_mAppIsQuitting_10() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D, ___mAppIsQuitting_10)); }
	inline bool get_mAppIsQuitting_10() const { return ___mAppIsQuitting_10; }
	inline bool* get_address_of_mAppIsQuitting_10() { return &___mAppIsQuitting_10; }
	inline void set_mAppIsQuitting_10(bool value)
	{
		___mAppIsQuitting_10 = value;
	}
};

struct VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D_StaticFields
{
public:
	// Vuforia.VuforiaRuntime Vuforia.VuforiaRuntime::mInstance
	VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D * ___mInstance_8;
	// System.Object Vuforia.VuforiaRuntime::mPadlock
	RuntimeObject * ___mPadlock_9;

public:
	inline static int32_t get_offset_of_mInstance_8() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D_StaticFields, ___mInstance_8)); }
	inline VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D * get_mInstance_8() const { return ___mInstance_8; }
	inline VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D ** get_address_of_mInstance_8() { return &___mInstance_8; }
	inline void set_mInstance_8(VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D * value)
	{
		___mInstance_8 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_8), value);
	}

	inline static int32_t get_offset_of_mPadlock_9() { return static_cast<int32_t>(offsetof(VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D_StaticFields, ___mPadlock_9)); }
	inline RuntimeObject * get_mPadlock_9() const { return ___mPadlock_9; }
	inline RuntimeObject ** get_address_of_mPadlock_9() { return &___mPadlock_9; }
	inline void set_mPadlock_9(RuntimeObject * value)
	{
		___mPadlock_9 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIARUNTIME_T12D46DC6B127017866AA832739B2B22B0612DC1D_H
#ifndef VUFORIARUNTIMEUTILITIES_T2BBA4B702A9982A803DE4C93AEF9591B12744DA7_H
#define VUFORIARUNTIMEUTILITIES_T2BBA4B702A9982A803DE4C93AEF9591B12744DA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntimeUtilities
struct  VuforiaRuntimeUtilities_t2BBA4B702A9982A803DE4C93AEF9591B12744DA7  : public RuntimeObject
{
public:

public:
};

struct VuforiaRuntimeUtilities_t2BBA4B702A9982A803DE4C93AEF9591B12744DA7_StaticFields
{
public:
	// Vuforia.VuforiaRuntimeUtilities_InitializableBool Vuforia.VuforiaRuntimeUtilities::sWebCamUsed
	int32_t ___sWebCamUsed_0;
	// Vuforia.VuforiaRuntimeUtilities_InitializableBool Vuforia.VuforiaRuntimeUtilities::sNativePluginSupport
	int32_t ___sNativePluginSupport_1;

public:
	inline static int32_t get_offset_of_sWebCamUsed_0() { return static_cast<int32_t>(offsetof(VuforiaRuntimeUtilities_t2BBA4B702A9982A803DE4C93AEF9591B12744DA7_StaticFields, ___sWebCamUsed_0)); }
	inline int32_t get_sWebCamUsed_0() const { return ___sWebCamUsed_0; }
	inline int32_t* get_address_of_sWebCamUsed_0() { return &___sWebCamUsed_0; }
	inline void set_sWebCamUsed_0(int32_t value)
	{
		___sWebCamUsed_0 = value;
	}

	inline static int32_t get_offset_of_sNativePluginSupport_1() { return static_cast<int32_t>(offsetof(VuforiaRuntimeUtilities_t2BBA4B702A9982A803DE4C93AEF9591B12744DA7_StaticFields, ___sNativePluginSupport_1)); }
	inline int32_t get_sNativePluginSupport_1() const { return ___sNativePluginSupport_1; }
	inline int32_t* get_address_of_sNativePluginSupport_1() { return &___sNativePluginSupport_1; }
	inline void set_sNativePluginSupport_1(int32_t value)
	{
		___sNativePluginSupport_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIARUNTIMEUTILITIES_T2BBA4B702A9982A803DE4C93AEF9591B12744DA7_H
#ifndef GLOBALVARS_TEB729934896F3AE63A63EBE07D7FBD183A0B22E6_H
#define GLOBALVARS_TEB729934896F3AE63A63EBE07D7FBD183A0B22E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRuntimeUtilities_GlobalVars
struct  GlobalVars_tEB729934896F3AE63A63EBE07D7FBD183A0B22E6  : public RuntimeObject
{
public:

public:
};

struct GlobalVars_tEB729934896F3AE63A63EBE07D7FBD183A0B22E6_StaticFields
{
public:
	// System.String Vuforia.VuforiaRuntimeUtilities_GlobalVars::GLTF_ASSET_LOCATION
	String_t* ___GLTF_ASSET_LOCATION_6;

public:
	inline static int32_t get_offset_of_GLTF_ASSET_LOCATION_6() { return static_cast<int32_t>(offsetof(GlobalVars_tEB729934896F3AE63A63EBE07D7FBD183A0B22E6_StaticFields, ___GLTF_ASSET_LOCATION_6)); }
	inline String_t* get_GLTF_ASSET_LOCATION_6() const { return ___GLTF_ASSET_LOCATION_6; }
	inline String_t** get_address_of_GLTF_ASSET_LOCATION_6() { return &___GLTF_ASSET_LOCATION_6; }
	inline void set_GLTF_ASSET_LOCATION_6(String_t* value)
	{
		___GLTF_ASSET_LOCATION_6 = value;
		Il2CppCodeGenWriteBarrier((&___GLTF_ASSET_LOCATION_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLOBALVARS_TEB729934896F3AE63A63EBE07D7FBD183A0B22E6_H
#ifndef WEBCAM_TB1292A564B1702E8D7B230601B4F55317B1AD0C2_H
#define WEBCAM_TB1292A564B1702E8D7B230601B4F55317B1AD0C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCam
struct  WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2  : public RuntimeObject
{
public:
	// UnityEngine.Camera[] Vuforia.WebCam::mARCameras
	CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9* ___mARCameras_0;
	// System.Int32[] Vuforia.WebCam::mOriginalCameraCullMask
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___mOriginalCameraCullMask_1;
	// Vuforia.IWebCamTexAdaptor Vuforia.WebCam::mWebCamTexture
	RuntimeObject* ___mWebCamTexture_2;
	// Vuforia.CameraDevice_VideoModeData Vuforia.WebCam::mVideoModeData
	VideoModeData_t7BC59F654FEA6A276ACE52D08AA89A8438698C3D  ___mVideoModeData_3;
	// Vuforia.TextureRenderer Vuforia.WebCam::mTextureRenderer
	TextureRenderer_t033DF107DC5C8ADC8C63277D48BA615920542D22 * ___mTextureRenderer_4;
	// UnityEngine.Texture2D Vuforia.WebCam::mBufferReadTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___mBufferReadTexture_5;
	// UnityEngine.Rect Vuforia.WebCam::mReadPixelsRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___mReadPixelsRect_6;
	// Vuforia.WebCamProfile_ProfileData Vuforia.WebCam::mWebCamProfile
	ProfileData_t7A423949645BADBD3002DCB2C13AD2BB7B6C2F9E  ___mWebCamProfile_7;
	// System.Boolean Vuforia.WebCam::mIsDirty
	bool ___mIsDirty_8;
	// System.Int32 Vuforia.WebCam::mLastFrameIdx
	int32_t ___mLastFrameIdx_9;
	// System.Int32 Vuforia.WebCam::mRenderTextureLayer
	int32_t ___mRenderTextureLayer_10;
	// System.Boolean Vuforia.WebCam::mWebcamPlaying
	bool ___mWebcamPlaying_11;
	// System.Boolean Vuforia.WebCam::<IsTextureSizeAvailable>k__BackingField
	bool ___U3CIsTextureSizeAvailableU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_mARCameras_0() { return static_cast<int32_t>(offsetof(WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2, ___mARCameras_0)); }
	inline CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9* get_mARCameras_0() const { return ___mARCameras_0; }
	inline CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9** get_address_of_mARCameras_0() { return &___mARCameras_0; }
	inline void set_mARCameras_0(CameraU5BU5D_t2A1957E88FB79357C12B87941970D776D30E90F9* value)
	{
		___mARCameras_0 = value;
		Il2CppCodeGenWriteBarrier((&___mARCameras_0), value);
	}

	inline static int32_t get_offset_of_mOriginalCameraCullMask_1() { return static_cast<int32_t>(offsetof(WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2, ___mOriginalCameraCullMask_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_mOriginalCameraCullMask_1() const { return ___mOriginalCameraCullMask_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_mOriginalCameraCullMask_1() { return &___mOriginalCameraCullMask_1; }
	inline void set_mOriginalCameraCullMask_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___mOriginalCameraCullMask_1 = value;
		Il2CppCodeGenWriteBarrier((&___mOriginalCameraCullMask_1), value);
	}

	inline static int32_t get_offset_of_mWebCamTexture_2() { return static_cast<int32_t>(offsetof(WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2, ___mWebCamTexture_2)); }
	inline RuntimeObject* get_mWebCamTexture_2() const { return ___mWebCamTexture_2; }
	inline RuntimeObject** get_address_of_mWebCamTexture_2() { return &___mWebCamTexture_2; }
	inline void set_mWebCamTexture_2(RuntimeObject* value)
	{
		___mWebCamTexture_2 = value;
		Il2CppCodeGenWriteBarrier((&___mWebCamTexture_2), value);
	}

	inline static int32_t get_offset_of_mVideoModeData_3() { return static_cast<int32_t>(offsetof(WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2, ___mVideoModeData_3)); }
	inline VideoModeData_t7BC59F654FEA6A276ACE52D08AA89A8438698C3D  get_mVideoModeData_3() const { return ___mVideoModeData_3; }
	inline VideoModeData_t7BC59F654FEA6A276ACE52D08AA89A8438698C3D * get_address_of_mVideoModeData_3() { return &___mVideoModeData_3; }
	inline void set_mVideoModeData_3(VideoModeData_t7BC59F654FEA6A276ACE52D08AA89A8438698C3D  value)
	{
		___mVideoModeData_3 = value;
	}

	inline static int32_t get_offset_of_mTextureRenderer_4() { return static_cast<int32_t>(offsetof(WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2, ___mTextureRenderer_4)); }
	inline TextureRenderer_t033DF107DC5C8ADC8C63277D48BA615920542D22 * get_mTextureRenderer_4() const { return ___mTextureRenderer_4; }
	inline TextureRenderer_t033DF107DC5C8ADC8C63277D48BA615920542D22 ** get_address_of_mTextureRenderer_4() { return &___mTextureRenderer_4; }
	inline void set_mTextureRenderer_4(TextureRenderer_t033DF107DC5C8ADC8C63277D48BA615920542D22 * value)
	{
		___mTextureRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTextureRenderer_4), value);
	}

	inline static int32_t get_offset_of_mBufferReadTexture_5() { return static_cast<int32_t>(offsetof(WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2, ___mBufferReadTexture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_mBufferReadTexture_5() const { return ___mBufferReadTexture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_mBufferReadTexture_5() { return &___mBufferReadTexture_5; }
	inline void set_mBufferReadTexture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___mBufferReadTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___mBufferReadTexture_5), value);
	}

	inline static int32_t get_offset_of_mReadPixelsRect_6() { return static_cast<int32_t>(offsetof(WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2, ___mReadPixelsRect_6)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_mReadPixelsRect_6() const { return ___mReadPixelsRect_6; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_mReadPixelsRect_6() { return &___mReadPixelsRect_6; }
	inline void set_mReadPixelsRect_6(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___mReadPixelsRect_6 = value;
	}

	inline static int32_t get_offset_of_mWebCamProfile_7() { return static_cast<int32_t>(offsetof(WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2, ___mWebCamProfile_7)); }
	inline ProfileData_t7A423949645BADBD3002DCB2C13AD2BB7B6C2F9E  get_mWebCamProfile_7() const { return ___mWebCamProfile_7; }
	inline ProfileData_t7A423949645BADBD3002DCB2C13AD2BB7B6C2F9E * get_address_of_mWebCamProfile_7() { return &___mWebCamProfile_7; }
	inline void set_mWebCamProfile_7(ProfileData_t7A423949645BADBD3002DCB2C13AD2BB7B6C2F9E  value)
	{
		___mWebCamProfile_7 = value;
	}

	inline static int32_t get_offset_of_mIsDirty_8() { return static_cast<int32_t>(offsetof(WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2, ___mIsDirty_8)); }
	inline bool get_mIsDirty_8() const { return ___mIsDirty_8; }
	inline bool* get_address_of_mIsDirty_8() { return &___mIsDirty_8; }
	inline void set_mIsDirty_8(bool value)
	{
		___mIsDirty_8 = value;
	}

	inline static int32_t get_offset_of_mLastFrameIdx_9() { return static_cast<int32_t>(offsetof(WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2, ___mLastFrameIdx_9)); }
	inline int32_t get_mLastFrameIdx_9() const { return ___mLastFrameIdx_9; }
	inline int32_t* get_address_of_mLastFrameIdx_9() { return &___mLastFrameIdx_9; }
	inline void set_mLastFrameIdx_9(int32_t value)
	{
		___mLastFrameIdx_9 = value;
	}

	inline static int32_t get_offset_of_mRenderTextureLayer_10() { return static_cast<int32_t>(offsetof(WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2, ___mRenderTextureLayer_10)); }
	inline int32_t get_mRenderTextureLayer_10() const { return ___mRenderTextureLayer_10; }
	inline int32_t* get_address_of_mRenderTextureLayer_10() { return &___mRenderTextureLayer_10; }
	inline void set_mRenderTextureLayer_10(int32_t value)
	{
		___mRenderTextureLayer_10 = value;
	}

	inline static int32_t get_offset_of_mWebcamPlaying_11() { return static_cast<int32_t>(offsetof(WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2, ___mWebcamPlaying_11)); }
	inline bool get_mWebcamPlaying_11() const { return ___mWebcamPlaying_11; }
	inline bool* get_address_of_mWebcamPlaying_11() { return &___mWebcamPlaying_11; }
	inline void set_mWebcamPlaying_11(bool value)
	{
		___mWebcamPlaying_11 = value;
	}

	inline static int32_t get_offset_of_U3CIsTextureSizeAvailableU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2, ___U3CIsTextureSizeAvailableU3Ek__BackingField_12)); }
	inline bool get_U3CIsTextureSizeAvailableU3Ek__BackingField_12() const { return ___U3CIsTextureSizeAvailableU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CIsTextureSizeAvailableU3Ek__BackingField_12() { return &___U3CIsTextureSizeAvailableU3Ek__BackingField_12; }
	inline void set_U3CIsTextureSizeAvailableU3Ek__BackingField_12(bool value)
	{
		___U3CIsTextureSizeAvailableU3Ek__BackingField_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCAM_TB1292A564B1702E8D7B230601B4F55317B1AD0C2_H
#ifndef PROFILECOLLECTION_TEF0A645B4A489A704B776140BF024B5740DB97E0_H
#define PROFILECOLLECTION_TEF0A645B4A489A704B776140BF024B5740DB97E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamProfile_ProfileCollection
struct  ProfileCollection_tEF0A645B4A489A704B776140BF024B5740DB97E0 
{
public:
	// Vuforia.WebCamProfile_ProfileData Vuforia.WebCamProfile_ProfileCollection::DefaultProfile
	ProfileData_t7A423949645BADBD3002DCB2C13AD2BB7B6C2F9E  ___DefaultProfile_0;
	// System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile_ProfileData> Vuforia.WebCamProfile_ProfileCollection::Profiles
	Dictionary_2_t46FB20A88A3801A02559059E86A26B5B78D0A86B * ___Profiles_1;

public:
	inline static int32_t get_offset_of_DefaultProfile_0() { return static_cast<int32_t>(offsetof(ProfileCollection_tEF0A645B4A489A704B776140BF024B5740DB97E0, ___DefaultProfile_0)); }
	inline ProfileData_t7A423949645BADBD3002DCB2C13AD2BB7B6C2F9E  get_DefaultProfile_0() const { return ___DefaultProfile_0; }
	inline ProfileData_t7A423949645BADBD3002DCB2C13AD2BB7B6C2F9E * get_address_of_DefaultProfile_0() { return &___DefaultProfile_0; }
	inline void set_DefaultProfile_0(ProfileData_t7A423949645BADBD3002DCB2C13AD2BB7B6C2F9E  value)
	{
		___DefaultProfile_0 = value;
	}

	inline static int32_t get_offset_of_Profiles_1() { return static_cast<int32_t>(offsetof(ProfileCollection_tEF0A645B4A489A704B776140BF024B5740DB97E0, ___Profiles_1)); }
	inline Dictionary_2_t46FB20A88A3801A02559059E86A26B5B78D0A86B * get_Profiles_1() const { return ___Profiles_1; }
	inline Dictionary_2_t46FB20A88A3801A02559059E86A26B5B78D0A86B ** get_address_of_Profiles_1() { return &___Profiles_1; }
	inline void set_Profiles_1(Dictionary_2_t46FB20A88A3801A02559059E86A26B5B78D0A86B * value)
	{
		___Profiles_1 = value;
		Il2CppCodeGenWriteBarrier((&___Profiles_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Vuforia.WebCamProfile/ProfileCollection
struct ProfileCollection_tEF0A645B4A489A704B776140BF024B5740DB97E0_marshaled_pinvoke
{
	ProfileData_t7A423949645BADBD3002DCB2C13AD2BB7B6C2F9E  ___DefaultProfile_0;
	Dictionary_2_t46FB20A88A3801A02559059E86A26B5B78D0A86B * ___Profiles_1;
};
// Native definition for COM marshalling of Vuforia.WebCamProfile/ProfileCollection
struct ProfileCollection_tEF0A645B4A489A704B776140BF024B5740DB97E0_marshaled_com
{
	ProfileData_t7A423949645BADBD3002DCB2C13AD2BB7B6C2F9E  ___DefaultProfile_0;
	Dictionary_2_t46FB20A88A3801A02559059E86A26B5B78D0A86B * ___Profiles_1;
};
#endif // PROFILECOLLECTION_TEF0A645B4A489A704B776140BF024B5740DB97E0_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef VUFORIACONFIGURATION_T389C464955859AB411F51B7950ACBAA6DCAFCD82_H
#define VUFORIACONFIGURATION_T389C464955859AB411F51B7950ACBAA6DCAFCD82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaConfiguration
struct  VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Vuforia.VuforiaConfiguration_GenericVuforiaConfiguration Vuforia.VuforiaConfiguration::vuforia
	GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064 * ___vuforia_6;
	// Vuforia.VuforiaConfiguration_DigitalEyewearConfiguration Vuforia.VuforiaConfiguration::digitalEyewear
	DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18 * ___digitalEyewear_7;
	// Vuforia.VuforiaConfiguration_DatabaseConfiguration Vuforia.VuforiaConfiguration::database
	DatabaseConfiguration_t0B1E7EF9676AD0191B8AFA87BFD43EB9F141928A * ___database_8;
	// Vuforia.VuforiaConfiguration_VideoBackgroundConfiguration Vuforia.VuforiaConfiguration::videoBackground
	VideoBackgroundConfiguration_tCC24E374B966B79D018C14F6807A6DDA47302F17 * ___videoBackground_9;
	// Vuforia.VuforiaConfiguration_DeviceTrackerConfiguration Vuforia.VuforiaConfiguration::deviceTracker
	DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F * ___deviceTracker_10;
	// Vuforia.VuforiaConfiguration_SmartTerrainConfiguration Vuforia.VuforiaConfiguration::smartTerrain
	SmartTerrainConfiguration_t6104CF56BC1AFE13ED509BB01CBE90E7E64372C7 * ___smartTerrain_11;
	// Vuforia.VuforiaConfiguration_WebCamConfiguration Vuforia.VuforiaConfiguration::webcam
	WebCamConfiguration_t0A5BD19A86D3AA8B68FB79C1EA8A5855A6C9F321 * ___webcam_12;

public:
	inline static int32_t get_offset_of_vuforia_6() { return static_cast<int32_t>(offsetof(VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82, ___vuforia_6)); }
	inline GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064 * get_vuforia_6() const { return ___vuforia_6; }
	inline GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064 ** get_address_of_vuforia_6() { return &___vuforia_6; }
	inline void set_vuforia_6(GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064 * value)
	{
		___vuforia_6 = value;
		Il2CppCodeGenWriteBarrier((&___vuforia_6), value);
	}

	inline static int32_t get_offset_of_digitalEyewear_7() { return static_cast<int32_t>(offsetof(VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82, ___digitalEyewear_7)); }
	inline DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18 * get_digitalEyewear_7() const { return ___digitalEyewear_7; }
	inline DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18 ** get_address_of_digitalEyewear_7() { return &___digitalEyewear_7; }
	inline void set_digitalEyewear_7(DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18 * value)
	{
		___digitalEyewear_7 = value;
		Il2CppCodeGenWriteBarrier((&___digitalEyewear_7), value);
	}

	inline static int32_t get_offset_of_database_8() { return static_cast<int32_t>(offsetof(VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82, ___database_8)); }
	inline DatabaseConfiguration_t0B1E7EF9676AD0191B8AFA87BFD43EB9F141928A * get_database_8() const { return ___database_8; }
	inline DatabaseConfiguration_t0B1E7EF9676AD0191B8AFA87BFD43EB9F141928A ** get_address_of_database_8() { return &___database_8; }
	inline void set_database_8(DatabaseConfiguration_t0B1E7EF9676AD0191B8AFA87BFD43EB9F141928A * value)
	{
		___database_8 = value;
		Il2CppCodeGenWriteBarrier((&___database_8), value);
	}

	inline static int32_t get_offset_of_videoBackground_9() { return static_cast<int32_t>(offsetof(VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82, ___videoBackground_9)); }
	inline VideoBackgroundConfiguration_tCC24E374B966B79D018C14F6807A6DDA47302F17 * get_videoBackground_9() const { return ___videoBackground_9; }
	inline VideoBackgroundConfiguration_tCC24E374B966B79D018C14F6807A6DDA47302F17 ** get_address_of_videoBackground_9() { return &___videoBackground_9; }
	inline void set_videoBackground_9(VideoBackgroundConfiguration_tCC24E374B966B79D018C14F6807A6DDA47302F17 * value)
	{
		___videoBackground_9 = value;
		Il2CppCodeGenWriteBarrier((&___videoBackground_9), value);
	}

	inline static int32_t get_offset_of_deviceTracker_10() { return static_cast<int32_t>(offsetof(VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82, ___deviceTracker_10)); }
	inline DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F * get_deviceTracker_10() const { return ___deviceTracker_10; }
	inline DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F ** get_address_of_deviceTracker_10() { return &___deviceTracker_10; }
	inline void set_deviceTracker_10(DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F * value)
	{
		___deviceTracker_10 = value;
		Il2CppCodeGenWriteBarrier((&___deviceTracker_10), value);
	}

	inline static int32_t get_offset_of_smartTerrain_11() { return static_cast<int32_t>(offsetof(VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82, ___smartTerrain_11)); }
	inline SmartTerrainConfiguration_t6104CF56BC1AFE13ED509BB01CBE90E7E64372C7 * get_smartTerrain_11() const { return ___smartTerrain_11; }
	inline SmartTerrainConfiguration_t6104CF56BC1AFE13ED509BB01CBE90E7E64372C7 ** get_address_of_smartTerrain_11() { return &___smartTerrain_11; }
	inline void set_smartTerrain_11(SmartTerrainConfiguration_t6104CF56BC1AFE13ED509BB01CBE90E7E64372C7 * value)
	{
		___smartTerrain_11 = value;
		Il2CppCodeGenWriteBarrier((&___smartTerrain_11), value);
	}

	inline static int32_t get_offset_of_webcam_12() { return static_cast<int32_t>(offsetof(VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82, ___webcam_12)); }
	inline WebCamConfiguration_t0A5BD19A86D3AA8B68FB79C1EA8A5855A6C9F321 * get_webcam_12() const { return ___webcam_12; }
	inline WebCamConfiguration_t0A5BD19A86D3AA8B68FB79C1EA8A5855A6C9F321 ** get_address_of_webcam_12() { return &___webcam_12; }
	inline void set_webcam_12(WebCamConfiguration_t0A5BD19A86D3AA8B68FB79C1EA8A5855A6C9F321 * value)
	{
		___webcam_12 = value;
		Il2CppCodeGenWriteBarrier((&___webcam_12), value);
	}
};

struct VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82_StaticFields
{
public:
	// Vuforia.VuforiaConfiguration Vuforia.VuforiaConfiguration::mInstance
	VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82 * ___mInstance_4;
	// System.Object Vuforia.VuforiaConfiguration::mPadlock
	RuntimeObject * ___mPadlock_5;

public:
	inline static int32_t get_offset_of_mInstance_4() { return static_cast<int32_t>(offsetof(VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82_StaticFields, ___mInstance_4)); }
	inline VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82 * get_mInstance_4() const { return ___mInstance_4; }
	inline VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82 ** get_address_of_mInstance_4() { return &___mInstance_4; }
	inline void set_mInstance_4(VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82 * value)
	{
		___mInstance_4 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_4), value);
	}

	inline static int32_t get_offset_of_mPadlock_5() { return static_cast<int32_t>(offsetof(VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82_StaticFields, ___mPadlock_5)); }
	inline RuntimeObject * get_mPadlock_5() const { return ___mPadlock_5; }
	inline RuntimeObject ** get_address_of_mPadlock_5() { return &___mPadlock_5; }
	inline void set_mPadlock_5(RuntimeObject * value)
	{
		___mPadlock_5 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIACONFIGURATION_T389C464955859AB411F51B7950ACBAA6DCAFCD82_H
#ifndef WEBCAMPROFILE_T43FF6EEF7ED810F8F46D5B0C9DE0CE01257B0D8B_H
#define WEBCAMPROFILE_T43FF6EEF7ED810F8F46D5B0C9DE0CE01257B0D8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.WebCamProfile
struct  WebCamProfile_t43FF6EEF7ED810F8F46D5B0C9DE0CE01257B0D8B  : public RuntimeObject
{
public:
	// Vuforia.WebCamProfile_ProfileCollection Vuforia.WebCamProfile::mProfileCollection
	ProfileCollection_tEF0A645B4A489A704B776140BF024B5740DB97E0  ___mProfileCollection_0;

public:
	inline static int32_t get_offset_of_mProfileCollection_0() { return static_cast<int32_t>(offsetof(WebCamProfile_t43FF6EEF7ED810F8F46D5B0C9DE0CE01257B0D8B, ___mProfileCollection_0)); }
	inline ProfileCollection_tEF0A645B4A489A704B776140BF024B5740DB97E0  get_mProfileCollection_0() const { return ___mProfileCollection_0; }
	inline ProfileCollection_tEF0A645B4A489A704B776140BF024B5740DB97E0 * get_address_of_mProfileCollection_0() { return &___mProfileCollection_0; }
	inline void set_mProfileCollection_0(ProfileCollection_tEF0A645B4A489A704B776140BF024B5740DB97E0  value)
	{
		___mProfileCollection_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBCAMPROFILE_T43FF6EEF7ED810F8F46D5B0C9DE0CE01257B0D8B_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef VUFORIAMONOBEHAVIOUR_T806C61E721B78928AF6266F3AF838FA2CB56AB5D_H
#define VUFORIAMONOBEHAVIOUR_T806C61E721B78928AF6266F3AF838FA2CB56AB5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VuforiaMonoBehaviour
struct  VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMONOBEHAVIOUR_T806C61E721B78928AF6266F3AF838FA2CB56AB5D_H
#ifndef TRACKABLEBEHAVIOUR_T579D75AAFEF7B2D69F4B68931D5A58074E80A7E4_H
#define TRACKABLEBEHAVIOUR_T579D75AAFEF7B2D69F4B68931D5A58074E80A7E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour
struct  TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// System.Double Vuforia.TrackableBehaviour::<TimeStamp>k__BackingField
	double ___U3CTimeStampU3Ek__BackingField_4;
	// System.String Vuforia.TrackableBehaviour::mTrackableName
	String_t* ___mTrackableName_5;
	// System.Boolean Vuforia.TrackableBehaviour::mPreserveChildSize
	bool ___mPreserveChildSize_6;
	// System.Boolean Vuforia.TrackableBehaviour::mInitializedInEditor
	bool ___mInitializedInEditor_7;
	// UnityEngine.Vector3 Vuforia.TrackableBehaviour::mPreviousScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mPreviousScale_8;
	// Vuforia.TrackableBehaviour_Status Vuforia.TrackableBehaviour::mStatus
	int32_t ___mStatus_9;
	// Vuforia.TrackableBehaviour_StatusInfo Vuforia.TrackableBehaviour::mStatusInfo
	int32_t ___mStatusInfo_10;
	// Vuforia.Trackable Vuforia.TrackableBehaviour::mTrackable
	RuntimeObject* ___mTrackable_11;
	// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler> Vuforia.TrackableBehaviour::mTrackableEventHandlers
	List_1_tE4338C7F7D33C78CB75B44EB5CCCA0152E97497B * ___mTrackableEventHandlers_12;

public:
	inline static int32_t get_offset_of_U3CTimeStampU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___U3CTimeStampU3Ek__BackingField_4)); }
	inline double get_U3CTimeStampU3Ek__BackingField_4() const { return ___U3CTimeStampU3Ek__BackingField_4; }
	inline double* get_address_of_U3CTimeStampU3Ek__BackingField_4() { return &___U3CTimeStampU3Ek__BackingField_4; }
	inline void set_U3CTimeStampU3Ek__BackingField_4(double value)
	{
		___U3CTimeStampU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_mTrackableName_5() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mTrackableName_5)); }
	inline String_t* get_mTrackableName_5() const { return ___mTrackableName_5; }
	inline String_t** get_address_of_mTrackableName_5() { return &___mTrackableName_5; }
	inline void set_mTrackableName_5(String_t* value)
	{
		___mTrackableName_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableName_5), value);
	}

	inline static int32_t get_offset_of_mPreserveChildSize_6() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mPreserveChildSize_6)); }
	inline bool get_mPreserveChildSize_6() const { return ___mPreserveChildSize_6; }
	inline bool* get_address_of_mPreserveChildSize_6() { return &___mPreserveChildSize_6; }
	inline void set_mPreserveChildSize_6(bool value)
	{
		___mPreserveChildSize_6 = value;
	}

	inline static int32_t get_offset_of_mInitializedInEditor_7() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mInitializedInEditor_7)); }
	inline bool get_mInitializedInEditor_7() const { return ___mInitializedInEditor_7; }
	inline bool* get_address_of_mInitializedInEditor_7() { return &___mInitializedInEditor_7; }
	inline void set_mInitializedInEditor_7(bool value)
	{
		___mInitializedInEditor_7 = value;
	}

	inline static int32_t get_offset_of_mPreviousScale_8() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mPreviousScale_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mPreviousScale_8() const { return ___mPreviousScale_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mPreviousScale_8() { return &___mPreviousScale_8; }
	inline void set_mPreviousScale_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mPreviousScale_8 = value;
	}

	inline static int32_t get_offset_of_mStatus_9() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mStatus_9)); }
	inline int32_t get_mStatus_9() const { return ___mStatus_9; }
	inline int32_t* get_address_of_mStatus_9() { return &___mStatus_9; }
	inline void set_mStatus_9(int32_t value)
	{
		___mStatus_9 = value;
	}

	inline static int32_t get_offset_of_mStatusInfo_10() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mStatusInfo_10)); }
	inline int32_t get_mStatusInfo_10() const { return ___mStatusInfo_10; }
	inline int32_t* get_address_of_mStatusInfo_10() { return &___mStatusInfo_10; }
	inline void set_mStatusInfo_10(int32_t value)
	{
		___mStatusInfo_10 = value;
	}

	inline static int32_t get_offset_of_mTrackable_11() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mTrackable_11)); }
	inline RuntimeObject* get_mTrackable_11() const { return ___mTrackable_11; }
	inline RuntimeObject** get_address_of_mTrackable_11() { return &___mTrackable_11; }
	inline void set_mTrackable_11(RuntimeObject* value)
	{
		___mTrackable_11 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackable_11), value);
	}

	inline static int32_t get_offset_of_mTrackableEventHandlers_12() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4, ___mTrackableEventHandlers_12)); }
	inline List_1_tE4338C7F7D33C78CB75B44EB5CCCA0152E97497B * get_mTrackableEventHandlers_12() const { return ___mTrackableEventHandlers_12; }
	inline List_1_tE4338C7F7D33C78CB75B44EB5CCCA0152E97497B ** get_address_of_mTrackableEventHandlers_12() { return &___mTrackableEventHandlers_12; }
	inline void set_mTrackableEventHandlers_12(List_1_tE4338C7F7D33C78CB75B44EB5CCCA0152E97497B * value)
	{
		___mTrackableEventHandlers_12 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableEventHandlers_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEBEHAVIOUR_T579D75AAFEF7B2D69F4B68931D5A58074E80A7E4_H
#ifndef VUFORIABEHAVIOUR_T9E688F16A822A56C5BB1910EF9B91448A9165BC2_H
#define VUFORIABEHAVIOUR_T9E688F16A822A56C5BB1910EF9B91448A9165BC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaBehaviour
struct  VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// Vuforia.VuforiaARController_WorldCenterMode Vuforia.VuforiaBehaviour::mWorldCenterMode
	int32_t ___mWorldCenterMode_5;
	// Vuforia.TrackableBehaviour Vuforia.VuforiaBehaviour::mWorldCenter
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * ___mWorldCenter_6;
	// System.Boolean Vuforia.VuforiaBehaviour::mAppIsQuitting
	bool ___mAppIsQuitting_7;
	// System.Action Vuforia.VuforiaBehaviour::AwakeEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___AwakeEvent_10;
	// System.Action Vuforia.VuforiaBehaviour::OnEnableEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnEnableEvent_11;
	// System.Action Vuforia.VuforiaBehaviour::StartEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___StartEvent_12;
	// System.Action Vuforia.VuforiaBehaviour::UpdateEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___UpdateEvent_13;
	// System.Action Vuforia.VuforiaBehaviour::OnLevelWasLoadedEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnLevelWasLoadedEvent_14;
	// System.Action`1<System.Boolean> Vuforia.VuforiaBehaviour::OnApplicationPauseEvent
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___OnApplicationPauseEvent_15;
	// System.Action Vuforia.VuforiaBehaviour::OnDisableEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnDisableEvent_16;
	// System.Action Vuforia.VuforiaBehaviour::OnDestroyEvent
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnDestroyEvent_17;

public:
	inline static int32_t get_offset_of_mWorldCenterMode_5() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2, ___mWorldCenterMode_5)); }
	inline int32_t get_mWorldCenterMode_5() const { return ___mWorldCenterMode_5; }
	inline int32_t* get_address_of_mWorldCenterMode_5() { return &___mWorldCenterMode_5; }
	inline void set_mWorldCenterMode_5(int32_t value)
	{
		___mWorldCenterMode_5 = value;
	}

	inline static int32_t get_offset_of_mWorldCenter_6() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2, ___mWorldCenter_6)); }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * get_mWorldCenter_6() const { return ___mWorldCenter_6; }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 ** get_address_of_mWorldCenter_6() { return &___mWorldCenter_6; }
	inline void set_mWorldCenter_6(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * value)
	{
		___mWorldCenter_6 = value;
		Il2CppCodeGenWriteBarrier((&___mWorldCenter_6), value);
	}

	inline static int32_t get_offset_of_mAppIsQuitting_7() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2, ___mAppIsQuitting_7)); }
	inline bool get_mAppIsQuitting_7() const { return ___mAppIsQuitting_7; }
	inline bool* get_address_of_mAppIsQuitting_7() { return &___mAppIsQuitting_7; }
	inline void set_mAppIsQuitting_7(bool value)
	{
		___mAppIsQuitting_7 = value;
	}

	inline static int32_t get_offset_of_AwakeEvent_10() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2, ___AwakeEvent_10)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_AwakeEvent_10() const { return ___AwakeEvent_10; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_AwakeEvent_10() { return &___AwakeEvent_10; }
	inline void set_AwakeEvent_10(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___AwakeEvent_10 = value;
		Il2CppCodeGenWriteBarrier((&___AwakeEvent_10), value);
	}

	inline static int32_t get_offset_of_OnEnableEvent_11() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2, ___OnEnableEvent_11)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnEnableEvent_11() const { return ___OnEnableEvent_11; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnEnableEvent_11() { return &___OnEnableEvent_11; }
	inline void set_OnEnableEvent_11(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnEnableEvent_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnEnableEvent_11), value);
	}

	inline static int32_t get_offset_of_StartEvent_12() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2, ___StartEvent_12)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_StartEvent_12() const { return ___StartEvent_12; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_StartEvent_12() { return &___StartEvent_12; }
	inline void set_StartEvent_12(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___StartEvent_12 = value;
		Il2CppCodeGenWriteBarrier((&___StartEvent_12), value);
	}

	inline static int32_t get_offset_of_UpdateEvent_13() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2, ___UpdateEvent_13)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_UpdateEvent_13() const { return ___UpdateEvent_13; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_UpdateEvent_13() { return &___UpdateEvent_13; }
	inline void set_UpdateEvent_13(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___UpdateEvent_13 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateEvent_13), value);
	}

	inline static int32_t get_offset_of_OnLevelWasLoadedEvent_14() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2, ___OnLevelWasLoadedEvent_14)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnLevelWasLoadedEvent_14() const { return ___OnLevelWasLoadedEvent_14; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnLevelWasLoadedEvent_14() { return &___OnLevelWasLoadedEvent_14; }
	inline void set_OnLevelWasLoadedEvent_14(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnLevelWasLoadedEvent_14 = value;
		Il2CppCodeGenWriteBarrier((&___OnLevelWasLoadedEvent_14), value);
	}

	inline static int32_t get_offset_of_OnApplicationPauseEvent_15() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2, ___OnApplicationPauseEvent_15)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_OnApplicationPauseEvent_15() const { return ___OnApplicationPauseEvent_15; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_OnApplicationPauseEvent_15() { return &___OnApplicationPauseEvent_15; }
	inline void set_OnApplicationPauseEvent_15(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___OnApplicationPauseEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___OnApplicationPauseEvent_15), value);
	}

	inline static int32_t get_offset_of_OnDisableEvent_16() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2, ___OnDisableEvent_16)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnDisableEvent_16() const { return ___OnDisableEvent_16; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnDisableEvent_16() { return &___OnDisableEvent_16; }
	inline void set_OnDisableEvent_16(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnDisableEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___OnDisableEvent_16), value);
	}

	inline static int32_t get_offset_of_OnDestroyEvent_17() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2, ___OnDestroyEvent_17)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnDestroyEvent_17() const { return ___OnDestroyEvent_17; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnDestroyEvent_17() { return &___OnDestroyEvent_17; }
	inline void set_OnDestroyEvent_17(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnDestroyEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___OnDestroyEvent_17), value);
	}
};

struct VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2_StaticFields
{
public:
	// Vuforia.VuforiaBehaviour Vuforia.VuforiaBehaviour::mVuforiaBehaviour
	VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2 * ___mVuforiaBehaviour_4;
	// System.Action`1<Vuforia.VuforiaBehaviour> Vuforia.VuforiaBehaviour::BehaviourCreated
	Action_1_t2C77A04A3D5781CE2D25ADCA64743C98A7E534F9 * ___BehaviourCreated_8;
	// System.Action`1<Vuforia.VuforiaBehaviour> Vuforia.VuforiaBehaviour::BehaviourDestroyed
	Action_1_t2C77A04A3D5781CE2D25ADCA64743C98A7E534F9 * ___BehaviourDestroyed_9;

public:
	inline static int32_t get_offset_of_mVuforiaBehaviour_4() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2_StaticFields, ___mVuforiaBehaviour_4)); }
	inline VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2 * get_mVuforiaBehaviour_4() const { return ___mVuforiaBehaviour_4; }
	inline VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2 ** get_address_of_mVuforiaBehaviour_4() { return &___mVuforiaBehaviour_4; }
	inline void set_mVuforiaBehaviour_4(VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2 * value)
	{
		___mVuforiaBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_4), value);
	}

	inline static int32_t get_offset_of_BehaviourCreated_8() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2_StaticFields, ___BehaviourCreated_8)); }
	inline Action_1_t2C77A04A3D5781CE2D25ADCA64743C98A7E534F9 * get_BehaviourCreated_8() const { return ___BehaviourCreated_8; }
	inline Action_1_t2C77A04A3D5781CE2D25ADCA64743C98A7E534F9 ** get_address_of_BehaviourCreated_8() { return &___BehaviourCreated_8; }
	inline void set_BehaviourCreated_8(Action_1_t2C77A04A3D5781CE2D25ADCA64743C98A7E534F9 * value)
	{
		___BehaviourCreated_8 = value;
		Il2CppCodeGenWriteBarrier((&___BehaviourCreated_8), value);
	}

	inline static int32_t get_offset_of_BehaviourDestroyed_9() { return static_cast<int32_t>(offsetof(VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2_StaticFields, ___BehaviourDestroyed_9)); }
	inline Action_1_t2C77A04A3D5781CE2D25ADCA64743C98A7E534F9 * get_BehaviourDestroyed_9() const { return ___BehaviourDestroyed_9; }
	inline Action_1_t2C77A04A3D5781CE2D25ADCA64743C98A7E534F9 ** get_address_of_BehaviourDestroyed_9() { return &___BehaviourDestroyed_9; }
	inline void set_BehaviourDestroyed_9(Action_1_t2C77A04A3D5781CE2D25ADCA64743C98A7E534F9 * value)
	{
		___BehaviourDestroyed_9 = value;
		Il2CppCodeGenWriteBarrier((&___BehaviourDestroyed_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIABEHAVIOUR_T9E688F16A822A56C5BB1910EF9B91448A9165BC2_H
#ifndef DATASETTRACKABLEBEHAVIOUR_T84B7BB3C959046F38CC73E423800BD2F8859E706_H
#define DATASETTRACKABLEBEHAVIOUR_T84B7BB3C959046F38CC73E423800BD2F8859E706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DataSetTrackableBehaviour
struct  DataSetTrackableBehaviour_t84B7BB3C959046F38CC73E423800BD2F8859E706  : public TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4
{
public:
	// System.String Vuforia.DataSetTrackableBehaviour::mDataSetPath
	String_t* ___mDataSetPath_13;

public:
	inline static int32_t get_offset_of_mDataSetPath_13() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t84B7BB3C959046F38CC73E423800BD2F8859E706, ___mDataSetPath_13)); }
	inline String_t* get_mDataSetPath_13() const { return ___mDataSetPath_13; }
	inline String_t** get_address_of_mDataSetPath_13() { return &___mDataSetPath_13; }
	inline void set_mDataSetPath_13(String_t* value)
	{
		___mDataSetPath_13 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSetPath_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASETTRACKABLEBEHAVIOUR_T84B7BB3C959046F38CC73E423800BD2F8859E706_H
#ifndef CYLINDERTARGETBEHAVIOUR_T5D0B9267830B26006BE1410C8C8080D78FA840F5_H
#define CYLINDERTARGETBEHAVIOUR_T5D0B9267830B26006BE1410C8C8080D78FA840F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CylinderTargetBehaviour
struct  CylinderTargetBehaviour_t5D0B9267830B26006BE1410C8C8080D78FA840F5  : public DataSetTrackableBehaviour_t84B7BB3C959046F38CC73E423800BD2F8859E706
{
public:
	// Vuforia.CylinderTarget Vuforia.CylinderTargetBehaviour::mCylinderTarget
	RuntimeObject* ___mCylinderTarget_14;
	// System.Single Vuforia.CylinderTargetBehaviour::mTopDiameterRatio
	float ___mTopDiameterRatio_15;
	// System.Single Vuforia.CylinderTargetBehaviour::mBottomDiameterRatio
	float ___mBottomDiameterRatio_16;
	// System.Single Vuforia.CylinderTargetBehaviour::mSideLength
	float ___mSideLength_17;
	// System.Single Vuforia.CylinderTargetBehaviour::mTopDiameter
	float ___mTopDiameter_18;
	// System.Single Vuforia.CylinderTargetBehaviour::mBottomDiameter
	float ___mBottomDiameter_19;
	// System.Int32 Vuforia.CylinderTargetBehaviour::mFrameIndex
	int32_t ___mFrameIndex_20;
	// System.Int32 Vuforia.CylinderTargetBehaviour::mUpdateFrameIndex
	int32_t ___mUpdateFrameIndex_21;
	// System.Single Vuforia.CylinderTargetBehaviour::mFutureScale
	float ___mFutureScale_22;
	// System.Single Vuforia.CylinderTargetBehaviour::mLastTransformScale
	float ___mLastTransformScale_23;

public:
	inline static int32_t get_offset_of_mCylinderTarget_14() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_t5D0B9267830B26006BE1410C8C8080D78FA840F5, ___mCylinderTarget_14)); }
	inline RuntimeObject* get_mCylinderTarget_14() const { return ___mCylinderTarget_14; }
	inline RuntimeObject** get_address_of_mCylinderTarget_14() { return &___mCylinderTarget_14; }
	inline void set_mCylinderTarget_14(RuntimeObject* value)
	{
		___mCylinderTarget_14 = value;
		Il2CppCodeGenWriteBarrier((&___mCylinderTarget_14), value);
	}

	inline static int32_t get_offset_of_mTopDiameterRatio_15() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_t5D0B9267830B26006BE1410C8C8080D78FA840F5, ___mTopDiameterRatio_15)); }
	inline float get_mTopDiameterRatio_15() const { return ___mTopDiameterRatio_15; }
	inline float* get_address_of_mTopDiameterRatio_15() { return &___mTopDiameterRatio_15; }
	inline void set_mTopDiameterRatio_15(float value)
	{
		___mTopDiameterRatio_15 = value;
	}

	inline static int32_t get_offset_of_mBottomDiameterRatio_16() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_t5D0B9267830B26006BE1410C8C8080D78FA840F5, ___mBottomDiameterRatio_16)); }
	inline float get_mBottomDiameterRatio_16() const { return ___mBottomDiameterRatio_16; }
	inline float* get_address_of_mBottomDiameterRatio_16() { return &___mBottomDiameterRatio_16; }
	inline void set_mBottomDiameterRatio_16(float value)
	{
		___mBottomDiameterRatio_16 = value;
	}

	inline static int32_t get_offset_of_mSideLength_17() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_t5D0B9267830B26006BE1410C8C8080D78FA840F5, ___mSideLength_17)); }
	inline float get_mSideLength_17() const { return ___mSideLength_17; }
	inline float* get_address_of_mSideLength_17() { return &___mSideLength_17; }
	inline void set_mSideLength_17(float value)
	{
		___mSideLength_17 = value;
	}

	inline static int32_t get_offset_of_mTopDiameter_18() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_t5D0B9267830B26006BE1410C8C8080D78FA840F5, ___mTopDiameter_18)); }
	inline float get_mTopDiameter_18() const { return ___mTopDiameter_18; }
	inline float* get_address_of_mTopDiameter_18() { return &___mTopDiameter_18; }
	inline void set_mTopDiameter_18(float value)
	{
		___mTopDiameter_18 = value;
	}

	inline static int32_t get_offset_of_mBottomDiameter_19() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_t5D0B9267830B26006BE1410C8C8080D78FA840F5, ___mBottomDiameter_19)); }
	inline float get_mBottomDiameter_19() const { return ___mBottomDiameter_19; }
	inline float* get_address_of_mBottomDiameter_19() { return &___mBottomDiameter_19; }
	inline void set_mBottomDiameter_19(float value)
	{
		___mBottomDiameter_19 = value;
	}

	inline static int32_t get_offset_of_mFrameIndex_20() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_t5D0B9267830B26006BE1410C8C8080D78FA840F5, ___mFrameIndex_20)); }
	inline int32_t get_mFrameIndex_20() const { return ___mFrameIndex_20; }
	inline int32_t* get_address_of_mFrameIndex_20() { return &___mFrameIndex_20; }
	inline void set_mFrameIndex_20(int32_t value)
	{
		___mFrameIndex_20 = value;
	}

	inline static int32_t get_offset_of_mUpdateFrameIndex_21() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_t5D0B9267830B26006BE1410C8C8080D78FA840F5, ___mUpdateFrameIndex_21)); }
	inline int32_t get_mUpdateFrameIndex_21() const { return ___mUpdateFrameIndex_21; }
	inline int32_t* get_address_of_mUpdateFrameIndex_21() { return &___mUpdateFrameIndex_21; }
	inline void set_mUpdateFrameIndex_21(int32_t value)
	{
		___mUpdateFrameIndex_21 = value;
	}

	inline static int32_t get_offset_of_mFutureScale_22() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_t5D0B9267830B26006BE1410C8C8080D78FA840F5, ___mFutureScale_22)); }
	inline float get_mFutureScale_22() const { return ___mFutureScale_22; }
	inline float* get_address_of_mFutureScale_22() { return &___mFutureScale_22; }
	inline void set_mFutureScale_22(float value)
	{
		___mFutureScale_22 = value;
	}

	inline static int32_t get_offset_of_mLastTransformScale_23() { return static_cast<int32_t>(offsetof(CylinderTargetBehaviour_t5D0B9267830B26006BE1410C8C8080D78FA840F5, ___mLastTransformScale_23)); }
	inline float get_mLastTransformScale_23() const { return ___mLastTransformScale_23; }
	inline float* get_address_of_mLastTransformScale_23() { return &___mLastTransformScale_23; }
	inline void set_mLastTransformScale_23(float value)
	{
		___mLastTransformScale_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CYLINDERTARGETBEHAVIOUR_T5D0B9267830B26006BE1410C8C8080D78FA840F5_H
#ifndef IMAGETARGETBEHAVIOUR_T2014110FECB3CAB6142743A36CA3F50A91E97540_H
#define IMAGETARGETBEHAVIOUR_T2014110FECB3CAB6142743A36CA3F50A91E97540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ImageTargetBehaviour
struct  ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540  : public DataSetTrackableBehaviour_t84B7BB3C959046F38CC73E423800BD2F8859E706
{
public:
	// System.Single Vuforia.ImageTargetBehaviour::mAspectRatio
	float ___mAspectRatio_14;
	// Vuforia.ImageTargetType Vuforia.ImageTargetBehaviour::mImageTargetType
	int32_t ___mImageTargetType_15;
	// System.Single Vuforia.ImageTargetBehaviour::mWidth
	float ___mWidth_16;
	// System.Single Vuforia.ImageTargetBehaviour::mHeight
	float ___mHeight_17;
	// Vuforia.ImageTarget Vuforia.ImageTargetBehaviour::mImageTarget
	RuntimeObject* ___mImageTarget_18;
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButtonBehaviour> Vuforia.ImageTargetBehaviour::mVirtualButtonBehaviours
	Dictionary_2_t49D49E0D72539C160B6F736C64DBC729238B3481 * ___mVirtualButtonBehaviours_19;
	// System.Single Vuforia.ImageTargetBehaviour::mLastTransformScale
	float ___mLastTransformScale_20;
	// UnityEngine.Vector2 Vuforia.ImageTargetBehaviour::mLastSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___mLastSize_21;

public:
	inline static int32_t get_offset_of_mAspectRatio_14() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540, ___mAspectRatio_14)); }
	inline float get_mAspectRatio_14() const { return ___mAspectRatio_14; }
	inline float* get_address_of_mAspectRatio_14() { return &___mAspectRatio_14; }
	inline void set_mAspectRatio_14(float value)
	{
		___mAspectRatio_14 = value;
	}

	inline static int32_t get_offset_of_mImageTargetType_15() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540, ___mImageTargetType_15)); }
	inline int32_t get_mImageTargetType_15() const { return ___mImageTargetType_15; }
	inline int32_t* get_address_of_mImageTargetType_15() { return &___mImageTargetType_15; }
	inline void set_mImageTargetType_15(int32_t value)
	{
		___mImageTargetType_15 = value;
	}

	inline static int32_t get_offset_of_mWidth_16() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540, ___mWidth_16)); }
	inline float get_mWidth_16() const { return ___mWidth_16; }
	inline float* get_address_of_mWidth_16() { return &___mWidth_16; }
	inline void set_mWidth_16(float value)
	{
		___mWidth_16 = value;
	}

	inline static int32_t get_offset_of_mHeight_17() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540, ___mHeight_17)); }
	inline float get_mHeight_17() const { return ___mHeight_17; }
	inline float* get_address_of_mHeight_17() { return &___mHeight_17; }
	inline void set_mHeight_17(float value)
	{
		___mHeight_17 = value;
	}

	inline static int32_t get_offset_of_mImageTarget_18() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540, ___mImageTarget_18)); }
	inline RuntimeObject* get_mImageTarget_18() const { return ___mImageTarget_18; }
	inline RuntimeObject** get_address_of_mImageTarget_18() { return &___mImageTarget_18; }
	inline void set_mImageTarget_18(RuntimeObject* value)
	{
		___mImageTarget_18 = value;
		Il2CppCodeGenWriteBarrier((&___mImageTarget_18), value);
	}

	inline static int32_t get_offset_of_mVirtualButtonBehaviours_19() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540, ___mVirtualButtonBehaviours_19)); }
	inline Dictionary_2_t49D49E0D72539C160B6F736C64DBC729238B3481 * get_mVirtualButtonBehaviours_19() const { return ___mVirtualButtonBehaviours_19; }
	inline Dictionary_2_t49D49E0D72539C160B6F736C64DBC729238B3481 ** get_address_of_mVirtualButtonBehaviours_19() { return &___mVirtualButtonBehaviours_19; }
	inline void set_mVirtualButtonBehaviours_19(Dictionary_2_t49D49E0D72539C160B6F736C64DBC729238B3481 * value)
	{
		___mVirtualButtonBehaviours_19 = value;
		Il2CppCodeGenWriteBarrier((&___mVirtualButtonBehaviours_19), value);
	}

	inline static int32_t get_offset_of_mLastTransformScale_20() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540, ___mLastTransformScale_20)); }
	inline float get_mLastTransformScale_20() const { return ___mLastTransformScale_20; }
	inline float* get_address_of_mLastTransformScale_20() { return &___mLastTransformScale_20; }
	inline void set_mLastTransformScale_20(float value)
	{
		___mLastTransformScale_20 = value;
	}

	inline static int32_t get_offset_of_mLastSize_21() { return static_cast<int32_t>(offsetof(ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540, ___mLastSize_21)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_mLastSize_21() const { return ___mLastSize_21; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_mLastSize_21() { return &___mLastSize_21; }
	inline void set_mLastSize_21(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___mLastSize_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGETARGETBEHAVIOUR_T2014110FECB3CAB6142743A36CA3F50A91E97540_H
#ifndef MULTITARGETBEHAVIOUR_TF3CD88859ACA6C07A2A61DFFE6FC6D64FA2353A9_H
#define MULTITARGETBEHAVIOUR_TF3CD88859ACA6C07A2A61DFFE6FC6D64FA2353A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MultiTargetBehaviour
struct  MultiTargetBehaviour_tF3CD88859ACA6C07A2A61DFFE6FC6D64FA2353A9  : public DataSetTrackableBehaviour_t84B7BB3C959046F38CC73E423800BD2F8859E706
{
public:
	// Vuforia.MultiTarget Vuforia.MultiTargetBehaviour::mMultiTarget
	RuntimeObject* ___mMultiTarget_14;

public:
	inline static int32_t get_offset_of_mMultiTarget_14() { return static_cast<int32_t>(offsetof(MultiTargetBehaviour_tF3CD88859ACA6C07A2A61DFFE6FC6D64FA2353A9, ___mMultiTarget_14)); }
	inline RuntimeObject* get_mMultiTarget_14() const { return ___mMultiTarget_14; }
	inline RuntimeObject** get_address_of_mMultiTarget_14() { return &___mMultiTarget_14; }
	inline void set_mMultiTarget_14(RuntimeObject* value)
	{
		___mMultiTarget_14 = value;
		Il2CppCodeGenWriteBarrier((&___mMultiTarget_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTITARGETBEHAVIOUR_TF3CD88859ACA6C07A2A61DFFE6FC6D64FA2353A9_H
#ifndef VUMARKBEHAVIOUR_T639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4_H
#define VUMARKBEHAVIOUR_T639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuMarkBehaviour
struct  VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4  : public DataSetTrackableBehaviour_t84B7BB3C959046F38CC73E423800BD2F8859E706
{
public:
	// System.Single Vuforia.VuMarkBehaviour::mAspectRatio
	float ___mAspectRatio_14;
	// System.Single Vuforia.VuMarkBehaviour::mWidth
	float ___mWidth_15;
	// System.Single Vuforia.VuMarkBehaviour::mHeight
	float ___mHeight_16;
	// System.String Vuforia.VuMarkBehaviour::mPreviewImage
	String_t* ___mPreviewImage_17;
	// Vuforia.InstanceIdType Vuforia.VuMarkBehaviour::mIdType
	int32_t ___mIdType_18;
	// System.Int32 Vuforia.VuMarkBehaviour::mIdLength
	int32_t ___mIdLength_19;
	// UnityEngine.Rect Vuforia.VuMarkBehaviour::mBoundingBox
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___mBoundingBox_20;
	// UnityEngine.Vector2 Vuforia.VuMarkBehaviour::mOrigin
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___mOrigin_21;
	// System.Boolean Vuforia.VuMarkBehaviour::mTrackingFromRuntimeAppearance
	bool ___mTrackingFromRuntimeAppearance_22;
	// Vuforia.VuMarkTemplate Vuforia.VuMarkBehaviour::mVuMarkTemplate
	RuntimeObject* ___mVuMarkTemplate_23;
	// Vuforia.VuMarkTarget Vuforia.VuMarkBehaviour::mVuMarkTarget
	RuntimeObject* ___mVuMarkTarget_24;
	// System.Int32 Vuforia.VuMarkBehaviour::mVuMarkResultId
	int32_t ___mVuMarkResultId_25;
	// System.Action Vuforia.VuMarkBehaviour::mOnTargetAssigned
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___mOnTargetAssigned_26;
	// System.Action Vuforia.VuMarkBehaviour::mOnTargetLost
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___mOnTargetLost_27;
	// System.Single Vuforia.VuMarkBehaviour::mLastTransformScale
	float ___mLastTransformScale_28;
	// UnityEngine.Vector2 Vuforia.VuMarkBehaviour::mLastSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___mLastSize_29;

public:
	inline static int32_t get_offset_of_mAspectRatio_14() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4, ___mAspectRatio_14)); }
	inline float get_mAspectRatio_14() const { return ___mAspectRatio_14; }
	inline float* get_address_of_mAspectRatio_14() { return &___mAspectRatio_14; }
	inline void set_mAspectRatio_14(float value)
	{
		___mAspectRatio_14 = value;
	}

	inline static int32_t get_offset_of_mWidth_15() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4, ___mWidth_15)); }
	inline float get_mWidth_15() const { return ___mWidth_15; }
	inline float* get_address_of_mWidth_15() { return &___mWidth_15; }
	inline void set_mWidth_15(float value)
	{
		___mWidth_15 = value;
	}

	inline static int32_t get_offset_of_mHeight_16() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4, ___mHeight_16)); }
	inline float get_mHeight_16() const { return ___mHeight_16; }
	inline float* get_address_of_mHeight_16() { return &___mHeight_16; }
	inline void set_mHeight_16(float value)
	{
		___mHeight_16 = value;
	}

	inline static int32_t get_offset_of_mPreviewImage_17() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4, ___mPreviewImage_17)); }
	inline String_t* get_mPreviewImage_17() const { return ___mPreviewImage_17; }
	inline String_t** get_address_of_mPreviewImage_17() { return &___mPreviewImage_17; }
	inline void set_mPreviewImage_17(String_t* value)
	{
		___mPreviewImage_17 = value;
		Il2CppCodeGenWriteBarrier((&___mPreviewImage_17), value);
	}

	inline static int32_t get_offset_of_mIdType_18() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4, ___mIdType_18)); }
	inline int32_t get_mIdType_18() const { return ___mIdType_18; }
	inline int32_t* get_address_of_mIdType_18() { return &___mIdType_18; }
	inline void set_mIdType_18(int32_t value)
	{
		___mIdType_18 = value;
	}

	inline static int32_t get_offset_of_mIdLength_19() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4, ___mIdLength_19)); }
	inline int32_t get_mIdLength_19() const { return ___mIdLength_19; }
	inline int32_t* get_address_of_mIdLength_19() { return &___mIdLength_19; }
	inline void set_mIdLength_19(int32_t value)
	{
		___mIdLength_19 = value;
	}

	inline static int32_t get_offset_of_mBoundingBox_20() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4, ___mBoundingBox_20)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_mBoundingBox_20() const { return ___mBoundingBox_20; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_mBoundingBox_20() { return &___mBoundingBox_20; }
	inline void set_mBoundingBox_20(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___mBoundingBox_20 = value;
	}

	inline static int32_t get_offset_of_mOrigin_21() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4, ___mOrigin_21)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_mOrigin_21() const { return ___mOrigin_21; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_mOrigin_21() { return &___mOrigin_21; }
	inline void set_mOrigin_21(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___mOrigin_21 = value;
	}

	inline static int32_t get_offset_of_mTrackingFromRuntimeAppearance_22() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4, ___mTrackingFromRuntimeAppearance_22)); }
	inline bool get_mTrackingFromRuntimeAppearance_22() const { return ___mTrackingFromRuntimeAppearance_22; }
	inline bool* get_address_of_mTrackingFromRuntimeAppearance_22() { return &___mTrackingFromRuntimeAppearance_22; }
	inline void set_mTrackingFromRuntimeAppearance_22(bool value)
	{
		___mTrackingFromRuntimeAppearance_22 = value;
	}

	inline static int32_t get_offset_of_mVuMarkTemplate_23() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4, ___mVuMarkTemplate_23)); }
	inline RuntimeObject* get_mVuMarkTemplate_23() const { return ___mVuMarkTemplate_23; }
	inline RuntimeObject** get_address_of_mVuMarkTemplate_23() { return &___mVuMarkTemplate_23; }
	inline void set_mVuMarkTemplate_23(RuntimeObject* value)
	{
		___mVuMarkTemplate_23 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkTemplate_23), value);
	}

	inline static int32_t get_offset_of_mVuMarkTarget_24() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4, ___mVuMarkTarget_24)); }
	inline RuntimeObject* get_mVuMarkTarget_24() const { return ___mVuMarkTarget_24; }
	inline RuntimeObject** get_address_of_mVuMarkTarget_24() { return &___mVuMarkTarget_24; }
	inline void set_mVuMarkTarget_24(RuntimeObject* value)
	{
		___mVuMarkTarget_24 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkTarget_24), value);
	}

	inline static int32_t get_offset_of_mVuMarkResultId_25() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4, ___mVuMarkResultId_25)); }
	inline int32_t get_mVuMarkResultId_25() const { return ___mVuMarkResultId_25; }
	inline int32_t* get_address_of_mVuMarkResultId_25() { return &___mVuMarkResultId_25; }
	inline void set_mVuMarkResultId_25(int32_t value)
	{
		___mVuMarkResultId_25 = value;
	}

	inline static int32_t get_offset_of_mOnTargetAssigned_26() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4, ___mOnTargetAssigned_26)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_mOnTargetAssigned_26() const { return ___mOnTargetAssigned_26; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_mOnTargetAssigned_26() { return &___mOnTargetAssigned_26; }
	inline void set_mOnTargetAssigned_26(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___mOnTargetAssigned_26 = value;
		Il2CppCodeGenWriteBarrier((&___mOnTargetAssigned_26), value);
	}

	inline static int32_t get_offset_of_mOnTargetLost_27() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4, ___mOnTargetLost_27)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_mOnTargetLost_27() const { return ___mOnTargetLost_27; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_mOnTargetLost_27() { return &___mOnTargetLost_27; }
	inline void set_mOnTargetLost_27(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___mOnTargetLost_27 = value;
		Il2CppCodeGenWriteBarrier((&___mOnTargetLost_27), value);
	}

	inline static int32_t get_offset_of_mLastTransformScale_28() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4, ___mLastTransformScale_28)); }
	inline float get_mLastTransformScale_28() const { return ___mLastTransformScale_28; }
	inline float* get_address_of_mLastTransformScale_28() { return &___mLastTransformScale_28; }
	inline void set_mLastTransformScale_28(float value)
	{
		___mLastTransformScale_28 = value;
	}

	inline static int32_t get_offset_of_mLastSize_29() { return static_cast<int32_t>(offsetof(VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4, ___mLastSize_29)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_mLastSize_29() const { return ___mLastSize_29; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_mLastSize_29() { return &___mLastSize_29; }
	inline void set_mLastSize_29(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___mLastSize_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKBEHAVIOUR_T639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (U3CU3Ec_tB435526A5CF5263ED816F1A1E6F72ED3E7168DB8), -1, sizeof(U3CU3Ec_tB435526A5CF5263ED816F1A1E6F72ED3E7168DB8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2600[3] = 
{
	U3CU3Ec_tB435526A5CF5263ED816F1A1E6F72ED3E7168DB8_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tB435526A5CF5263ED816F1A1E6F72ED3E7168DB8_StaticFields::get_offset_of_U3CU3E9__1_0_1(),
	U3CU3Ec_tB435526A5CF5263ED816F1A1E6F72ED3E7168DB8_StaticFields::get_offset_of_U3CU3E9__1_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { sizeof (LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2601[9] = 
{
	LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C::get_offset_of_mInitialized_0(),
	LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C::get_offset_of_mEnabled_1(),
	LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C::get_offset_of_mUpdated_2(),
	LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C::get_offset_of_mCachedPositionalOffset_3(),
	LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C::get_offset_of_mCachedRotationalOffset_4(),
	LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C::get_offset_of_mCachedTrackableResultDataArray_5(),
	LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C::get_offset_of_mCachedVuMarkResultDataArray_6(),
	LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C::get_offset_of_mCachedOriginTrackableID_7(),
	LateLatchingManager_t989F0737102DC3958150786EDFBA8BBE6C63663C::get_offset_of_mCachedFrameIndex_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (VRDeviceController_tDFB2A6DE022256FD695E024050B1F3A07B97D044), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2602[2] = 
{
	VRDeviceController_tDFB2A6DE022256FD695E024050B1F3A07B97D044::get_offset_of_mHeadPoseDisablingClasses_0(),
	VRDeviceController_tDFB2A6DE022256FD695E024050B1F3A07B97D044::get_offset_of_mHeadPosesEnabledOnce_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { sizeof (VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2), -1, sizeof(VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2603[14] = 
{
	VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2_StaticFields::get_offset_of_mVuforiaBehaviour_4(),
	VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2::get_offset_of_mWorldCenterMode_5(),
	VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2::get_offset_of_mWorldCenter_6(),
	VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2::get_offset_of_mAppIsQuitting_7(),
	VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2_StaticFields::get_offset_of_BehaviourCreated_8(),
	VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2_StaticFields::get_offset_of_BehaviourDestroyed_9(),
	VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2::get_offset_of_AwakeEvent_10(),
	VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2::get_offset_of_OnEnableEvent_11(),
	VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2::get_offset_of_StartEvent_12(),
	VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2::get_offset_of_UpdateEvent_13(),
	VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2::get_offset_of_OnLevelWasLoadedEvent_14(),
	VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2::get_offset_of_OnApplicationPauseEvent_15(),
	VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2::get_offset_of_OnDisableEvent_16(),
	VuforiaBehaviour_t9E688F16A822A56C5BB1910EF9B91448A9165BC2::get_offset_of_OnDestroyEvent_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { sizeof (VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82), -1, sizeof(VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2604[9] = 
{
	VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82_StaticFields::get_offset_of_mInstance_4(),
	VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82_StaticFields::get_offset_of_mPadlock_5(),
	VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82::get_offset_of_vuforia_6(),
	VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82::get_offset_of_digitalEyewear_7(),
	VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82::get_offset_of_database_8(),
	VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82::get_offset_of_videoBackground_9(),
	VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82::get_offset_of_deviceTracker_10(),
	VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82::get_offset_of_smartTerrain_11(),
	VuforiaConfiguration_t389C464955859AB411F51B7950ACBAA6DCAFCD82::get_offset_of_webcam_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2605[11] = 
{
	0,
	GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064::get_offset_of_vuforiaLicenseKey_1(),
	GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064::get_offset_of_ufoLicenseKey_2(),
	GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064::get_offset_of_delayedInitialization_3(),
	GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064::get_offset_of_cameraDeviceModeSetting_4(),
	GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064::get_offset_of_maxSimultaneousImageTargets_5(),
	GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064::get_offset_of_maxSimultaneousObjectTargets_6(),
	GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064::get_offset_of_useDelayedLoadingObjectTargets_7(),
	GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064::get_offset_of_modelTargetRecoWhileExtendedTracked_8(),
	GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064::get_offset_of_version_9(),
	GenericVuforiaConfiguration_t6FBB0036347CB878A938375817103A20CDD59064::get_offset_of_eulaAcceptedVersions_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2606[9] = 
{
	DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18::get_offset_of_cameraOffset_0(),
	DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18::get_offset_of_distortionRenderingLayer_1(),
	DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18::get_offset_of_eyewearType_2(),
	DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18::get_offset_of_stereoFramework_3(),
	DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18::get_offset_of_seeThroughConfiguration_4(),
	DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18::get_offset_of_viewerName_5(),
	DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18::get_offset_of_viewerManufacturer_6(),
	DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18::get_offset_of_useCustomViewer_7(),
	DigitalEyewearConfiguration_t1BA464AEDA9C847084FCF4F1CA3C92719D655B18::get_offset_of_customViewer_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (DatabaseConfiguration_t0B1E7EF9676AD0191B8AFA87BFD43EB9F141928A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2607[1] = 
{
	DatabaseConfiguration_t0B1E7EF9676AD0191B8AFA87BFD43EB9F141928A::get_offset_of_disableModelExtraction_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (VideoBackgroundConfiguration_tCC24E374B966B79D018C14F6807A6DDA47302F17), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2608[5] = 
{
	VideoBackgroundConfiguration_tCC24E374B966B79D018C14F6807A6DDA47302F17::get_offset_of_clippingMode_0(),
	VideoBackgroundConfiguration_tCC24E374B966B79D018C14F6807A6DDA47302F17::get_offset_of_numDivisions_1(),
	VideoBackgroundConfiguration_tCC24E374B966B79D018C14F6807A6DDA47302F17::get_offset_of_videoBackgroundShader_2(),
	VideoBackgroundConfiguration_tCC24E374B966B79D018C14F6807A6DDA47302F17::get_offset_of_matteShader_3(),
	VideoBackgroundConfiguration_tCC24E374B966B79D018C14F6807A6DDA47302F17::get_offset_of_videoBackgroundEnabled_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (TrackerConfiguration_t15942AA3CED7034F69729C6FC20AFCD7C078670A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2609[2] = 
{
	TrackerConfiguration_t15942AA3CED7034F69729C6FC20AFCD7C078670A::get_offset_of_autoInitTracker_0(),
	TrackerConfiguration_t15942AA3CED7034F69729C6FC20AFCD7C078670A::get_offset_of_autoStartTracker_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2610[6] = 
{
	DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F::get_offset_of_arcoreRequirement_2(),
	DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F::get_offset_of_trackingMode_3(),
	DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F::get_offset_of_posePrediction_4(),
	DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F::get_offset_of_modelCorrectionMode_5(),
	DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F::get_offset_of_modelTransformEnabled_6(),
	DeviceTrackerConfiguration_tC11D2DA49200D3693731D6AFF3F793E4315D1E3F::get_offset_of_modelTransform_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (ARCoreRequirement_tE27B0C1428A8BB5AD741CC60732FEB1F09AC9155)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2611[4] = 
{
	ARCoreRequirement_tE27B0C1428A8BB5AD741CC60732FEB1F09AC9155::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (SmartTerrainConfiguration_t6104CF56BC1AFE13ED509BB01CBE90E7E64372C7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (WebCamConfiguration_t0A5BD19A86D3AA8B68FB79C1EA8A5855A6C9F321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2613[3] = 
{
	WebCamConfiguration_t0A5BD19A86D3AA8B68FB79C1EA8A5855A6C9F321::get_offset_of_deviceNameSetInEditor_0(),
	WebCamConfiguration_t0A5BD19A86D3AA8B68FB79C1EA8A5855A6C9F321::get_offset_of_turnOffWebCam_1(),
	WebCamConfiguration_t0A5BD19A86D3AA8B68FB79C1EA8A5855A6C9F321::get_offset_of_renderTextureLayer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D), -1, sizeof(VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2614[11] = 
{
	0,
	VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D::get_offset_of_mOnVuforiaInitError_1(),
	VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D::get_offset_of_mFailedToInitialize_2(),
	VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D::get_offset_of_mInitError_3(),
	VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D::get_offset_of_mInitState_4(),
	VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D::get_offset_of_mTimesPermissionCheckFailed_5(),
	VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D::get_offset_of_mInitThreadReturned_6(),
	VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D::get_offset_of_mReturnedError_7(),
	VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D_StaticFields::get_offset_of_mInstance_8(),
	VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D_StaticFields::get_offset_of_mPadlock_9(),
	VuforiaRuntime_t12D46DC6B127017866AA832739B2B22B0612DC1D::get_offset_of_mAppIsQuitting_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { sizeof (InitState_t0072A2BD2C69378E4575A8148A3DB0974451CD70)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2615[5] = 
{
	InitState_t0072A2BD2C69378E4575A8148A3DB0974451CD70::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2616[16] = 
{
	VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4::get_offset_of_mAspectRatio_14(),
	VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4::get_offset_of_mWidth_15(),
	VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4::get_offset_of_mHeight_16(),
	VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4::get_offset_of_mPreviewImage_17(),
	VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4::get_offset_of_mIdType_18(),
	VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4::get_offset_of_mIdLength_19(),
	VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4::get_offset_of_mBoundingBox_20(),
	VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4::get_offset_of_mOrigin_21(),
	VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4::get_offset_of_mTrackingFromRuntimeAppearance_22(),
	VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4::get_offset_of_mVuMarkTemplate_23(),
	VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4::get_offset_of_mVuMarkTarget_24(),
	VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4::get_offset_of_mVuMarkResultId_25(),
	VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4::get_offset_of_mOnTargetAssigned_26(),
	VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4::get_offset_of_mOnTargetLost_27(),
	VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4::get_offset_of_mLastTransformScale_28(),
	VuMarkBehaviour_t639ADC64791886C7F8D65CD035E0F0E4C6E2FAC4::get_offset_of_mLastSize_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (VuMarkManager_tD0288834C61D3F733AD8E4E98FE9C5DED6AC56F9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2617[6] = 
{
	VuMarkManager_tD0288834C61D3F733AD8E4E98FE9C5DED6AC56F9::get_offset_of_mBehaviours_0(),
	VuMarkManager_tD0288834C61D3F733AD8E4E98FE9C5DED6AC56F9::get_offset_of_mActiveVuMarkTargets_1(),
	VuMarkManager_tD0288834C61D3F733AD8E4E98FE9C5DED6AC56F9::get_offset_of_mDestroyedBehaviours_2(),
	VuMarkManager_tD0288834C61D3F733AD8E4E98FE9C5DED6AC56F9::get_offset_of_mOnVuMarkDetected_3(),
	VuMarkManager_tD0288834C61D3F733AD8E4E98FE9C5DED6AC56F9::get_offset_of_mOnVuMarkLost_4(),
	VuMarkManager_tD0288834C61D3F733AD8E4E98FE9C5DED6AC56F9::get_offset_of_mOnVuMarkBehaviourDetected_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (InstanceIdType_t2B8C4E1066D9FFD1C8BAA369E303860476BABF0D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2618[4] = 
{
	InstanceIdType_t2B8C4E1066D9FFD1C8BAA369E303860476BABF0D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (CylinderTargetBehaviour_t5D0B9267830B26006BE1410C8C8080D78FA840F5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2624[10] = 
{
	CylinderTargetBehaviour_t5D0B9267830B26006BE1410C8C8080D78FA840F5::get_offset_of_mCylinderTarget_14(),
	CylinderTargetBehaviour_t5D0B9267830B26006BE1410C8C8080D78FA840F5::get_offset_of_mTopDiameterRatio_15(),
	CylinderTargetBehaviour_t5D0B9267830B26006BE1410C8C8080D78FA840F5::get_offset_of_mBottomDiameterRatio_16(),
	CylinderTargetBehaviour_t5D0B9267830B26006BE1410C8C8080D78FA840F5::get_offset_of_mSideLength_17(),
	CylinderTargetBehaviour_t5D0B9267830B26006BE1410C8C8080D78FA840F5::get_offset_of_mTopDiameter_18(),
	CylinderTargetBehaviour_t5D0B9267830B26006BE1410C8C8080D78FA840F5::get_offset_of_mBottomDiameter_19(),
	CylinderTargetBehaviour_t5D0B9267830B26006BE1410C8C8080D78FA840F5::get_offset_of_mFrameIndex_20(),
	CylinderTargetBehaviour_t5D0B9267830B26006BE1410C8C8080D78FA840F5::get_offset_of_mUpdateFrameIndex_21(),
	CylinderTargetBehaviour_t5D0B9267830B26006BE1410C8C8080D78FA840F5::get_offset_of_mFutureScale_22(),
	CylinderTargetBehaviour_t5D0B9267830B26006BE1410C8C8080D78FA840F5::get_offset_of_mLastTransformScale_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2625[5] = 
{
	DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644::get_offset_of_mDataSetPtr_0(),
	DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644::get_offset_of_mPath_1(),
	DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644::get_offset_of_mStorageType_2(),
	DataSet_t9E9CB4ECE4C6C5C7B2FD9597B83FBAB7C55A4644::get_offset_of_mTrackablesDict_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (StorageType_t42B154A18B4AF9C010AA731DB709FC5EABF5D1D8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2626[4] = 
{
	StorageType_t42B154A18B4AF9C010AA731DB709FC5EABF5D1D8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (DatabaseLoadARController_t46BD7BB216821464A9EB9C66E30CC19A31FA1C7A), -1, sizeof(DatabaseLoadARController_t46BD7BB216821464A9EB9C66E30CC19A31FA1C7A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2627[7] = 
{
	DatabaseLoadARController_t46BD7BB216821464A9EB9C66E30CC19A31FA1C7A::get_offset_of_mDatasetsLoaded_1(),
	DatabaseLoadARController_t46BD7BB216821464A9EB9C66E30CC19A31FA1C7A::get_offset_of_mExternalDatasetRoots_2(),
	DatabaseLoadARController_t46BD7BB216821464A9EB9C66E30CC19A31FA1C7A::get_offset_of_mDataSetsToLoad_3(),
	DatabaseLoadARController_t46BD7BB216821464A9EB9C66E30CC19A31FA1C7A::get_offset_of_mDataSetsToNOTLoad_4(),
	DatabaseLoadARController_t46BD7BB216821464A9EB9C66E30CC19A31FA1C7A::get_offset_of_mDataSetsToActivate_5(),
	DatabaseLoadARController_t46BD7BB216821464A9EB9C66E30CC19A31FA1C7A_StaticFields::get_offset_of_mInstance_6(),
	DatabaseLoadARController_t46BD7BB216821464A9EB9C66E30CC19A31FA1C7A_StaticFields::get_offset_of_mPadlock_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (DataSetTrackableBehaviour_t84B7BB3C959046F38CC73E423800BD2F8859E706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2628[1] = 
{
	DataSetTrackableBehaviour_t84B7BB3C959046F38CC73E423800BD2F8859E706::get_offset_of_mDataSetPath_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (ObjectTargetData_tD6160795783492DD1322A2E4B1842086FD50EF00)+ sizeof (RuntimeObject), sizeof(ObjectTargetData_tD6160795783492DD1322A2E4B1842086FD50EF00 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2629[2] = 
{
	ObjectTargetData_tD6160795783492DD1322A2E4B1842086FD50EF00::get_offset_of_id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectTargetData_tD6160795783492DD1322A2E4B1842086FD50EF00::get_offset_of_size_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9)+ sizeof (RuntimeObject), sizeof(RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2631[4] = 
{
	RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9::get_offset_of_leftTopX_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9::get_offset_of_leftTopY_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9::get_offset_of_rightBottomX_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RectangleData_tA9B66ACDEB48962ADACFB8D18129ED016D732BE9::get_offset_of_rightBottomY_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (RectangleIntData_t3E3BC17A4EC0DBA1E1206EB4E9BE76FC2D012553)+ sizeof (RuntimeObject), sizeof(RectangleIntData_t3E3BC17A4EC0DBA1E1206EB4E9BE76FC2D012553 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2632[4] = 
{
	RectangleIntData_t3E3BC17A4EC0DBA1E1206EB4E9BE76FC2D012553::get_offset_of_leftTopX_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RectangleIntData_t3E3BC17A4EC0DBA1E1206EB4E9BE76FC2D012553::get_offset_of_leftTopY_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RectangleIntData_t3E3BC17A4EC0DBA1E1206EB4E9BE76FC2D012553::get_offset_of_rightBottomX_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RectangleIntData_t3E3BC17A4EC0DBA1E1206EB4E9BE76FC2D012553::get_offset_of_rightBottomY_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (OrientedBoundingBox_t0698A68A263D40AA4048260B1DAD47A15EEB598B)+ sizeof (RuntimeObject), sizeof(OrientedBoundingBox_t0698A68A263D40AA4048260B1DAD47A15EEB598B ), 0, 0 };
extern const int32_t g_FieldOffsetTable2633[3] = 
{
	OrientedBoundingBox_t0698A68A263D40AA4048260B1DAD47A15EEB598B::get_offset_of_U3CCenterU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OrientedBoundingBox_t0698A68A263D40AA4048260B1DAD47A15EEB598B::get_offset_of_U3CHalfExtentsU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OrientedBoundingBox_t0698A68A263D40AA4048260B1DAD47A15EEB598B::get_offset_of_U3CRotationU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (OrientedBoundingBox3D_t2239DBD535FBC2F84F49502D23761C0630CC93B9)+ sizeof (RuntimeObject), sizeof(OrientedBoundingBox3D_t2239DBD535FBC2F84F49502D23761C0630CC93B9 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2634[3] = 
{
	OrientedBoundingBox3D_t2239DBD535FBC2F84F49502D23761C0630CC93B9::get_offset_of_U3CCenterU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OrientedBoundingBox3D_t2239DBD535FBC2F84F49502D23761C0630CC93B9::get_offset_of_U3CHalfExtentsU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OrientedBoundingBox3D_t2239DBD535FBC2F84F49502D23761C0630CC93B9::get_offset_of_U3CRotationYU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (CloudRecoImageTargetImpl_tE3588C83AF4E12EC1A5CC46A20581E58DB56D797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2637[1] = 
{
	CloudRecoImageTargetImpl_tE3588C83AF4E12EC1A5CC46A20581E58DB56D797::get_offset_of_mSize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (CylinderTargetImpl_t5DF553BFC311E3E8DBF13776A39F11C50F39BFA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2638[4] = 
{
	CylinderTargetImpl_t5DF553BFC311E3E8DBF13776A39F11C50F39BFA5::get_offset_of_mSideLength_4(),
	CylinderTargetImpl_t5DF553BFC311E3E8DBF13776A39F11C50F39BFA5::get_offset_of_mTopDiameter_5(),
	CylinderTargetImpl_t5DF553BFC311E3E8DBF13776A39F11C50F39BFA5::get_offset_of_mBottomDiameter_6(),
	CylinderTargetImpl_t5DF553BFC311E3E8DBF13776A39F11C50F39BFA5::get_offset_of_mCustomTargetSize_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (ImageTargetType_t3A1F68B5507799B9B73A9806FA199BF2F3A819D6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2639[4] = 
{
	ImageTargetType_t3A1F68B5507799B9B73A9806FA199BF2F3A819D6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (ImageTargetData_t5C57B09E08494327B42B9DDE3E2D0F1433E0A198)+ sizeof (RuntimeObject), sizeof(ImageTargetData_t5C57B09E08494327B42B9DDE3E2D0F1433E0A198 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2640[2] = 
{
	ImageTargetData_t5C57B09E08494327B42B9DDE3E2D0F1433E0A198::get_offset_of_id_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ImageTargetData_t5C57B09E08494327B42B9DDE3E2D0F1433E0A198::get_offset_of_size_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (ImageTargetBuilder_t5BA66A134696E24A591FA066087BAABE66F00755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2642[2] = 
{
	ImageTargetBuilder_t5BA66A134696E24A591FA066087BAABE66F00755::get_offset_of_mTrackableSource_0(),
	ImageTargetBuilder_t5BA66A134696E24A591FA066087BAABE66F00755::get_offset_of_mIsScanning_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (FrameQuality_tEA002835E0F464BB9506076B691826DAAFEB87FB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2643[5] = 
{
	FrameQuality_tEA002835E0F464BB9506076B691826DAAFEB87FB::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { sizeof (ImageTargetImpl_t5DF2A3C71EC32EFB7A5D80571C65827C436BE0FC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2644[2] = 
{
	ImageTargetImpl_t5DF2A3C71EC32EFB7A5D80571C65827C436BE0FC::get_offset_of_mImageTargetType_4(),
	ImageTargetImpl_t5DF2A3C71EC32EFB7A5D80571C65827C436BE0FC::get_offset_of_mVirtualButtons_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { sizeof (MultiTargetImpl_t7EC94DEAA083EF8826B31B4700CADDACA4B1643A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (MultiSetTargetSize_t4D4F4D573C37F7A936EB268A4576CDC0602E22E3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2647[2] = 
{
	MultiSetTargetSize_t4D4F4D573C37F7A936EB268A4576CDC0602E22E3::get_offset_of_mDataSetPtr_1(),
	MultiSetTargetSize_t4D4F4D573C37F7A936EB268A4576CDC0602E22E3::get_offset_of_mName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { sizeof (DefaultWebCamTexAdaptor_t76E5C24E8EBA6DCA906BF5C289D7C19115FEFF8D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2648[5] = 
{
	DefaultWebCamTexAdaptor_t76E5C24E8EBA6DCA906BF5C289D7C19115FEFF8D::get_offset_of_mTexture_0(),
	DefaultWebCamTexAdaptor_t76E5C24E8EBA6DCA906BF5C289D7C19115FEFF8D::get_offset_of_mPseudoPlaying_1(),
	DefaultWebCamTexAdaptor_t76E5C24E8EBA6DCA906BF5C289D7C19115FEFF8D::get_offset_of_mMsBetweenFrames_2(),
	DefaultWebCamTexAdaptor_t76E5C24E8EBA6DCA906BF5C289D7C19115FEFF8D::get_offset_of_mLastFrame_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (PlayModeEditorUtility_t4C4971B1A8CE64AD2D21007CB4ABDAABD2D2951A), -1, sizeof(PlayModeEditorUtility_t4C4971B1A8CE64AD2D21007CB4ABDAABD2D2951A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2649[1] = 
{
	PlayModeEditorUtility_t4C4971B1A8CE64AD2D21007CB4ABDAABD2D2951A_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (NullPlayModeEditorUtility_t06E84C91A80EB0DD0D7E5DBA780DC16CA2FBBE89), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (PremiumObjectFactory_t1C221706BF440DC37E00859C8D0851C34773AF79), -1, sizeof(PremiumObjectFactory_t1C221706BF440DC37E00859C8D0851C34773AF79_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2651[1] = 
{
	PremiumObjectFactory_t1C221706BF440DC37E00859C8D0851C34773AF79_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (NullPremiumObjectFactory_tCCF6718E86AE7E65FB25DDF81A0C4E75A2767032), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (TypeMapping_tD7716603EBE8D435AD26ABAEC1C1BFD88D70E5AC), -1, sizeof(TypeMapping_tD7716603EBE8D435AD26ABAEC1C1BFD88D70E5AC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2653[1] = 
{
	TypeMapping_tD7716603EBE8D435AD26ABAEC1C1BFD88D70E5AC_StaticFields::get_offset_of_sTypes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (WebCamTexAdaptor_t048726FEC9AC07F2D1949310A33517A042E50F5F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2655[2] = 
{
	WebCamTexAdaptor_t048726FEC9AC07F2D1949310A33517A042E50F5F::get_offset_of_mWebCamTexture_0(),
	WebCamTexAdaptor_t048726FEC9AC07F2D1949310A33517A042E50F5F::get_offset_of_mCheckCameraPermissions_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (VuforiaNullWrapper_tB0EFEA2000D8D4037DF5330B0AE989A191875F03), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (VuforiaNativeWrapper_t2BC1D2FB459100031EE442E9607A94D6644CD2D2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (VuforiaWrapper_tDC7A7816CA0A902A65EAE1EEBA8A27D87F94C1FD), -1, sizeof(VuforiaWrapper_tDC7A7816CA0A902A65EAE1EEBA8A27D87F94C1FD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2659[2] = 
{
	VuforiaWrapper_tDC7A7816CA0A902A65EAE1EEBA8A27D87F94C1FD_StaticFields::get_offset_of_sWrapper_0(),
	VuforiaWrapper_tDC7A7816CA0A902A65EAE1EEBA8A27D87F94C1FD_StaticFields::get_offset_of_sCamIndependentWrapper_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2663[9] = 
{
	StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1::get_offset_of_mTrackableBehaviours_0(),
	StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1::get_offset_of_mAutomaticallyCreatedBehaviours_1(),
	StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1::get_offset_of_mBehavioursMarkedForDeletion_2(),
	StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1::get_offset_of_mActiveTrackableBehaviours_3(),
	StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1::get_offset_of_mVuMarkManager_4(),
	StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1::get_offset_of_mDeviceTrackingManager_5(),
	StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1::get_offset_of_mCameraPositioningHelper_6(),
	StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1::get_offset_of_mExtendedTrackingManager_7(),
	StateManager_t3EECC6F13CB23833CF4CE201E0BA0B51B5A546E1::get_offset_of_mIlluminationManager_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010), -1, sizeof(U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2664[10] = 
{
	U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_StaticFields::get_offset_of_U3CU3E9__31_0_1(),
	U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_StaticFields::get_offset_of_U3CU3E9__31_1_2(),
	U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_StaticFields::get_offset_of_U3CU3E9__31_2_3(),
	U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_StaticFields::get_offset_of_U3CU3E9__31_3_4(),
	U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_StaticFields::get_offset_of_U3CU3E9__32_0_5(),
	U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_StaticFields::get_offset_of_U3CU3E9__32_1_6(),
	U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_StaticFields::get_offset_of_U3CU3E9__32_2_7(),
	U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_StaticFields::get_offset_of_U3CU3E9__32_3_8(),
	U3CU3Ec_t5414AB3F07CB83B511D1AF6E2FEDE1566A9A9010_StaticFields::get_offset_of_U3CU3E9__32_4_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (TextureRenderer_t033DF107DC5C8ADC8C63277D48BA615920542D22), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2665[3] = 
{
	TextureRenderer_t033DF107DC5C8ADC8C63277D48BA615920542D22::get_offset_of_mTextureBufferCamera_0(),
	TextureRenderer_t033DF107DC5C8ADC8C63277D48BA615920542D22::get_offset_of_mTextureWidth_1(),
	TextureRenderer_t033DF107DC5C8ADC8C63277D48BA615920542D22::get_offset_of_mTextureHeight_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (TrackableImpl_t97E7D175DE38822AF55FC03FCA26FB62B7E290A2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2666[2] = 
{
	TrackableImpl_t97E7D175DE38822AF55FC03FCA26FB62B7E290A2::get_offset_of_U3CNameU3Ek__BackingField_0(),
	TrackableImpl_t97E7D175DE38822AF55FC03FCA26FB62B7E290A2::get_offset_of_U3CIDU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2667[13] = 
{
	WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2::get_offset_of_mARCameras_0(),
	WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2::get_offset_of_mOriginalCameraCullMask_1(),
	WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2::get_offset_of_mWebCamTexture_2(),
	WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2::get_offset_of_mVideoModeData_3(),
	WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2::get_offset_of_mTextureRenderer_4(),
	WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2::get_offset_of_mBufferReadTexture_5(),
	WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2::get_offset_of_mReadPixelsRect_6(),
	WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2::get_offset_of_mWebCamProfile_7(),
	WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2::get_offset_of_mIsDirty_8(),
	WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2::get_offset_of_mLastFrameIdx_9(),
	WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2::get_offset_of_mRenderTextureLayer_10(),
	WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2::get_offset_of_mWebcamPlaying_11(),
	WebCam_tB1292A564B1702E8D7B230601B4F55317B1AD0C2::get_offset_of_U3CIsTextureSizeAvailableU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (WebCamProfile_t43FF6EEF7ED810F8F46D5B0C9DE0CE01257B0D8B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2668[1] = 
{
	WebCamProfile_t43FF6EEF7ED810F8F46D5B0C9DE0CE01257B0D8B::get_offset_of_mProfileCollection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (ProfileData_t7A423949645BADBD3002DCB2C13AD2BB7B6C2F9E)+ sizeof (RuntimeObject), sizeof(ProfileData_t7A423949645BADBD3002DCB2C13AD2BB7B6C2F9E ), 0, 0 };
extern const int32_t g_FieldOffsetTable2669[3] = 
{
	ProfileData_t7A423949645BADBD3002DCB2C13AD2BB7B6C2F9E::get_offset_of_RequestedTextureSize_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProfileData_t7A423949645BADBD3002DCB2C13AD2BB7B6C2F9E::get_offset_of_ResampledTextureSize_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProfileData_t7A423949645BADBD3002DCB2C13AD2BB7B6C2F9E::get_offset_of_RequestedFPS_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (ProfileCollection_tEF0A645B4A489A704B776140BF024B5740DB97E0)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2670[2] = 
{
	ProfileCollection_tEF0A645B4A489A704B776140BF024B5740DB97E0::get_offset_of_DefaultProfile_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProfileCollection_tEF0A645B4A489A704B776140BF024B5740DB97E0::get_offset_of_Profiles_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2673[8] = 
{
	ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540::get_offset_of_mAspectRatio_14(),
	ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540::get_offset_of_mImageTargetType_15(),
	ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540::get_offset_of_mWidth_16(),
	ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540::get_offset_of_mHeight_17(),
	ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540::get_offset_of_mImageTarget_18(),
	ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540::get_offset_of_mVirtualButtonBehaviours_19(),
	ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540::get_offset_of_mLastTransformScale_20(),
	ImageTargetBehaviour_t2014110FECB3CAB6142743A36CA3F50A91E97540::get_offset_of_mLastSize_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2674[4] = 
{
	ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E::get_offset_of_mActiveDataSets_1(),
	ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E::get_offset_of_mDataSets_2(),
	ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E::get_offset_of_mTargetFinders_3(),
	ObjectTracker_tC5DE67DCDB38A86F77C3462ECB63C4453A1D8B3E::get_offset_of_mImageTargetBuilder_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (MultiTargetBehaviour_tF3CD88859ACA6C07A2A61DFFE6FC6D64FA2353A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2676[1] = 
{
	MultiTargetBehaviour_tF3CD88859ACA6C07A2A61DFFE6FC6D64FA2353A9::get_offset_of_mMultiTarget_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (VuforiaUnity_t0642B0D82DCCD1F058C8133A227319530273E77F), -1, sizeof(VuforiaUnity_t0642B0D82DCCD1F058C8133A227319530273E77F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2677[4] = 
{
	VuforiaUnity_t0642B0D82DCCD1F058C8133A227319530273E77F_StaticFields::get_offset_of_mHoloLensApiAbstraction_0(),
	0,
	VuforiaUnity_t0642B0D82DCCD1F058C8133A227319530273E77F_StaticFields::get_offset_of_mRendererDirty_2(),
	VuforiaUnity_t0642B0D82DCCD1F058C8133A227319530273E77F_StaticFields::get_offset_of_mWrapperType_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (InitError_t486F7D53F5B0B7943D4E22BE2D32F4913D4A0431)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2678[13] = 
{
	InitError_t486F7D53F5B0B7943D4E22BE2D32F4913D4A0431::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (VuforiaHint_t5E4E8E78687FAEB9311A944CD4051169EF4A5A6D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2679[6] = 
{
	VuforiaHint_t5E4E8E78687FAEB9311A944CD4051169EF4A5A6D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (StorageType_t6847D699566F42A8C42777B30537BA0C0521A4D3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2680[4] = 
{
	StorageType_t6847D699566F42A8C42777B30537BA0C0521A4D3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876), -1, sizeof(VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2681[31] = 
{
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mCameraDeviceModeSetting_1(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mMaxSimultaneousImageTargets_2(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mMaxSimultaneousObjectTargets_3(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mUseDelayedLoadingObjectTargets_4(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mModelTargetRecoWhileExtendedTracked_5(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mWorldCenterMode_6(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mWorldCenter_7(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mVideoBgEventHandlers_8(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mOnBeforeVuforiaTrackersInitialized_9(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mOnVuforiaInitialized_10(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mOnVuforiaStarted_11(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mOnVuforiaDeinitialized_12(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mOnTrackablesUpdated_13(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mRenderOnUpdate_14(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mOnPause_15(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mPaused_16(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mOnBackgroundTextureChanged_17(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mStartHasBeenInvoked_18(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mHasStarted_19(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mCameraConfiguration_20(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mEyewearBehaviour_21(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mCheckStopCamera_22(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mClearMaterial_23(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mMetalRendering_24(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mHasStartedOnce_25(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mWasEnabledBeforePause_26(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mObjectTrackerWasActiveBeforePause_27(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mObjectTrackerWasActiveBeforeDisabling_28(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876::get_offset_of_mLastUpdatedFrame_29(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876_StaticFields::get_offset_of_mInstance_30(),
	VuforiaARController_t7732FFB77105A2F5BBEA40E3CECC5C15DA624876_StaticFields::get_offset_of_mPadlock_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (WorldCenterMode_t53E1430BD989A54F75332A2DA9D61C93545897E6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2682[4] = 
{
	WorldCenterMode_t53E1430BD989A54F75332A2DA9D61C93545897E6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (VuforiaMacros_tED85BB64AE7B78C234CDEEF78193EBFD4369CE2D)+ sizeof (RuntimeObject), sizeof(VuforiaMacros_tED85BB64AE7B78C234CDEEF78193EBFD4369CE2D ), 0, 0 };
extern const int32_t g_FieldOffsetTable2683[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7), -1, sizeof(VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2684[24] = 
{
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7_StaticFields::get_offset_of_sInstance_0(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mWorldCenterMode_1(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mWorldCenter_2(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mVuMarkWorldCenter_3(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mARCameraTransform_4(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mCentralAnchorPoint_5(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mTrackableResultDataArray_6(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mVuMarkDataArray_7(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mVuMarkResultDataArray_8(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mWCTrackableFoundQueue_9(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mCameraImages_10(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mImageData_11(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mInjectedFrameIdx_12(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mLastProcessedFrameStatePtr_13(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mInitialized_14(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mPaused_15(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mFrameState_16(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mAutoRotationState_17(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mLastFrameIdx_18(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mIsSeeThroughDevice_19(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mLateLatchingManager_20(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mCameraCalibrationComparer_21(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mPosePositionalOffset_22(),
	VuforiaManager_t56F310BC8B59799D4A44824C42168D133AE095F7::get_offset_of_mPoseRotationalOffset_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F)+ sizeof (RuntimeObject), sizeof(TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F ), 0, 0 };
extern const int32_t g_FieldOffsetTable2685[2] = 
{
	TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F::get_offset_of_TrackableId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TrackableIdPair_t0ED43F39A2E02ABE4CC16B7E1EAB75CA03F3443F::get_offset_of_ResultId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (AutoRotationState_t7763EA1A2607ECEF69E2D2438C471FE93451E508)+ sizeof (RuntimeObject), sizeof(AutoRotationState_t7763EA1A2607ECEF69E2D2438C471FE93451E508_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2686[5] = 
{
	AutoRotationState_t7763EA1A2607ECEF69E2D2438C471FE93451E508::get_offset_of_setOnPause_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AutoRotationState_t7763EA1A2607ECEF69E2D2438C471FE93451E508::get_offset_of_autorotateToPortrait_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AutoRotationState_t7763EA1A2607ECEF69E2D2438C471FE93451E508::get_offset_of_autorotateToPortraitUpsideDown_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AutoRotationState_t7763EA1A2607ECEF69E2D2438C471FE93451E508::get_offset_of_autorotateToLandscapeLeft_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AutoRotationState_t7763EA1A2607ECEF69E2D2438C471FE93451E508::get_offset_of_autorotateToLandscapeRight_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (U3CU3Ec__DisplayClass60_0_t9B16CEDAB1D664130D52803188ADF3EF78ED78A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2687[1] = 
{
	U3CU3Ec__DisplayClass60_0_t9B16CEDAB1D664130D52803188ADF3EF78ED78A3::get_offset_of_frameState_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (U3CU3Ec__DisplayClass71_0_tF16DC2DDDAFB2FC93B740291F3BF21ECA28C9174), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2688[1] = 
{
	U3CU3Ec__DisplayClass71_0_tF16DC2DDDAFB2FC93B740291F3BF21ECA28C9174::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081), -1, sizeof(VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2689[7] = 
{
	VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081_StaticFields::get_offset_of_sInstance_0(),
	VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081::get_offset_of_mVideoBGConfig_1(),
	VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081::get_offset_of_mVideoBGConfigSet_2(),
	VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081::get_offset_of_mVideoBackgroundTexture_3(),
	VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081::get_offset_of_mNativeRenderingCallback_4(),
	VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081::get_offset_of_mBackgroundTextureHasChanged_5(),
	VuforiaRenderer_t76A570ACCA3476064DB729CCDAF59CF0BAB41081::get_offset_of_mLegacyRenderingCondition_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (FpsHint_t53FC6839AC85BCC4C8696D147820A30A5C8A5276)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2690[5] = 
{
	FpsHint_t53FC6839AC85BCC4C8696D147820A30A5C8A5276::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (VideoBGCfgData_t4FD9E3E825BD3AF119DFAE696F50C478336B1A9A)+ sizeof (RuntimeObject), sizeof(VideoBGCfgData_t4FD9E3E825BD3AF119DFAE696F50C478336B1A9A ), 0, 0 };
extern const int32_t g_FieldOffsetTable2691[2] = 
{
	VideoBGCfgData_t4FD9E3E825BD3AF119DFAE696F50C478336B1A9A::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VideoBGCfgData_t4FD9E3E825BD3AF119DFAE696F50C478336B1A9A::get_offset_of_size_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5)+ sizeof (RuntimeObject), sizeof(Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2692[2] = 
{
	Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vec2I_t42F341F398E3117176C37CAAAF2D29943EA9FCF5::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (VideoTextureInfo_tE91303BB8B6C3E4C49CB1A717AE3ECEB2175D36D)+ sizeof (RuntimeObject), sizeof(VideoTextureInfo_tE91303BB8B6C3E4C49CB1A717AE3ECEB2175D36D ), 0, 0 };
extern const int32_t g_FieldOffsetTable2693[2] = 
{
	VideoTextureInfo_tE91303BB8B6C3E4C49CB1A717AE3ECEB2175D36D::get_offset_of_textureSize_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VideoTextureInfo_tE91303BB8B6C3E4C49CB1A717AE3ECEB2175D36D::get_offset_of_imageSize_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (RendererAPI_tC4AFE17C6FEA6EFD2328230E48CF33F7884AAAB1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2694[5] = 
{
	RendererAPI_tC4AFE17C6FEA6EFD2328230E48CF33F7884AAAB1::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (RenderEvent_t855A5B140DD41FF1AF8179D914AEA4D2B1B88DA7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2695[8] = 
{
	RenderEvent_t855A5B140DD41FF1AF8179D914AEA4D2B1B88DA7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609), -1, sizeof(U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2696[2] = 
{
	U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t0D9CA9C8F7A76A99A794019E01671BDCA16F9609_StaticFields::get_offset_of_U3CU3E9__18_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (VuforiaRuntimeUtilities_t2BBA4B702A9982A803DE4C93AEF9591B12744DA7), -1, sizeof(VuforiaRuntimeUtilities_t2BBA4B702A9982A803DE4C93AEF9591B12744DA7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2697[2] = 
{
	VuforiaRuntimeUtilities_t2BBA4B702A9982A803DE4C93AEF9591B12744DA7_StaticFields::get_offset_of_sWebCamUsed_0(),
	VuforiaRuntimeUtilities_t2BBA4B702A9982A803DE4C93AEF9591B12744DA7_StaticFields::get_offset_of_sNativePluginSupport_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (InitializableBool_t6C711D5999687FC6E8CAF38208EC8E185BF1FC22)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2698[4] = 
{
	InitializableBool_t6C711D5999687FC6E8CAF38208EC8E185BF1FC22::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (GlobalVars_tEB729934896F3AE63A63EBE07D7FBD183A0B22E6), -1, sizeof(GlobalVars_tEB729934896F3AE63A63EBE07D7FBD183A0B22E6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2699[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	GlobalVars_tEB729934896F3AE63A63EBE07D7FBD183A0B22E6_StaticFields::get_offset_of_GLTF_ASSET_LOCATION_6(),
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
