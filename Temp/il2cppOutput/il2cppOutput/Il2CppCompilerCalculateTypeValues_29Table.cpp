﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// NoteBookUpperCase
struct NoteBookUpperCase_t2F0680F176BD4435E95119A593054ED5B6761347;
// System.Action`1<UnityEngine.Camera>
struct Action_1_t6D83B4F361CDBC0D6B559F8DA3A9646E2ED9561C;
// System.Action`1<UnityEngine.Camera[]>
struct Action_1_t66F98C2DDE752F9D83CB8FEBE8084E0ABB8C314A;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4;
// Vuforia.UnityCompiled.IUnityAndroidPermissions
struct IUnityAndroidPermissions_tC63F0B0237AED0CFC9589B54D40A234FF082562D;
// Vuforia.UnityCompiled.IUnityCompiledFacade
struct IUnityCompiledFacade_t0E3A7AE8F2EFBD9E4E214CB598761DDD267E70BE;
// Vuforia.UnityCompiled.IUnityRenderPipeline
struct IUnityRenderPipeline_tAD5FDC16A735AA177793B835843069369918FC34;




#ifndef U3CMODULEU3E_TBB5D6FDE6CBE0067E5DC774822D5054F01745F8C_H
#define U3CMODULEU3E_TBB5D6FDE6CBE0067E5DC774822D5054F01745F8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tBB5D6FDE6CBE0067E5DC774822D5054F01745F8C 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TBB5D6FDE6CBE0067E5DC774822D5054F01745F8C_H
#ifndef U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#define U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef RUNTIMEOPENSOURCEINITIALIZER_T81785D24983BCE4C95906F20541AD7A3D3D11862_H
#define RUNTIMEOPENSOURCEINITIALIZER_T81785D24983BCE4C95906F20541AD7A3D3D11862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UnityCompiled.RuntimeOpenSourceInitializer
struct  RuntimeOpenSourceInitializer_t81785D24983BCE4C95906F20541AD7A3D3D11862  : public RuntimeObject
{
public:

public:
};

struct RuntimeOpenSourceInitializer_t81785D24983BCE4C95906F20541AD7A3D3D11862_StaticFields
{
public:
	// Vuforia.UnityCompiled.IUnityCompiledFacade Vuforia.UnityCompiled.RuntimeOpenSourceInitializer::sFacade
	RuntimeObject* ___sFacade_0;

public:
	inline static int32_t get_offset_of_sFacade_0() { return static_cast<int32_t>(offsetof(RuntimeOpenSourceInitializer_t81785D24983BCE4C95906F20541AD7A3D3D11862_StaticFields, ___sFacade_0)); }
	inline RuntimeObject* get_sFacade_0() const { return ___sFacade_0; }
	inline RuntimeObject** get_address_of_sFacade_0() { return &___sFacade_0; }
	inline void set_sFacade_0(RuntimeObject* value)
	{
		___sFacade_0 = value;
		Il2CppCodeGenWriteBarrier((&___sFacade_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOPENSOURCEINITIALIZER_T81785D24983BCE4C95906F20541AD7A3D3D11862_H
#ifndef OPENSOURCEUNITYCOMPILEDFACADE_T8315EDA1848F9A4AA27FC5AEAA48F4097D1FF2A5_H
#define OPENSOURCEUNITYCOMPILEDFACADE_T8315EDA1848F9A4AA27FC5AEAA48F4097D1FF2A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UnityCompiled.RuntimeOpenSourceInitializer_OpenSourceUnityCompiledFacade
struct  OpenSourceUnityCompiledFacade_t8315EDA1848F9A4AA27FC5AEAA48F4097D1FF2A5  : public RuntimeObject
{
public:
	// Vuforia.UnityCompiled.IUnityRenderPipeline Vuforia.UnityCompiled.RuntimeOpenSourceInitializer_OpenSourceUnityCompiledFacade::mUnityRenderPipeline
	RuntimeObject* ___mUnityRenderPipeline_0;
	// Vuforia.UnityCompiled.IUnityAndroidPermissions Vuforia.UnityCompiled.RuntimeOpenSourceInitializer_OpenSourceUnityCompiledFacade::mUnityAndroidPermissions
	RuntimeObject* ___mUnityAndroidPermissions_1;

public:
	inline static int32_t get_offset_of_mUnityRenderPipeline_0() { return static_cast<int32_t>(offsetof(OpenSourceUnityCompiledFacade_t8315EDA1848F9A4AA27FC5AEAA48F4097D1FF2A5, ___mUnityRenderPipeline_0)); }
	inline RuntimeObject* get_mUnityRenderPipeline_0() const { return ___mUnityRenderPipeline_0; }
	inline RuntimeObject** get_address_of_mUnityRenderPipeline_0() { return &___mUnityRenderPipeline_0; }
	inline void set_mUnityRenderPipeline_0(RuntimeObject* value)
	{
		___mUnityRenderPipeline_0 = value;
		Il2CppCodeGenWriteBarrier((&___mUnityRenderPipeline_0), value);
	}

	inline static int32_t get_offset_of_mUnityAndroidPermissions_1() { return static_cast<int32_t>(offsetof(OpenSourceUnityCompiledFacade_t8315EDA1848F9A4AA27FC5AEAA48F4097D1FF2A5, ___mUnityAndroidPermissions_1)); }
	inline RuntimeObject* get_mUnityAndroidPermissions_1() const { return ___mUnityAndroidPermissions_1; }
	inline RuntimeObject** get_address_of_mUnityAndroidPermissions_1() { return &___mUnityAndroidPermissions_1; }
	inline void set_mUnityAndroidPermissions_1(RuntimeObject* value)
	{
		___mUnityAndroidPermissions_1 = value;
		Il2CppCodeGenWriteBarrier((&___mUnityAndroidPermissions_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENSOURCEUNITYCOMPILEDFACADE_T8315EDA1848F9A4AA27FC5AEAA48F4097D1FF2A5_H
#ifndef UNITYANDROIDPERMISSIONS_TA4CE60356A3424ECF5E1AB1F39264182BB49D7CF_H
#define UNITYANDROIDPERMISSIONS_TA4CE60356A3424ECF5E1AB1F39264182BB49D7CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UnityCompiled.RuntimeOpenSourceInitializer_UnityAndroidPermissions
struct  UnityAndroidPermissions_tA4CE60356A3424ECF5E1AB1F39264182BB49D7CF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYANDROIDPERMISSIONS_TA4CE60356A3424ECF5E1AB1F39264182BB49D7CF_H
#ifndef UNITYRENDERPIPELINE_T62C711062D224A17F999C0F58CDB6F6E641560A4_H
#define UNITYRENDERPIPELINE_T62C711062D224A17F999C0F58CDB6F6E641560A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UnityCompiled.RuntimeOpenSourceInitializer_UnityRenderPipeline
struct  UnityRenderPipeline_t62C711062D224A17F999C0F58CDB6F6E641560A4  : public RuntimeObject
{
public:
	// System.Action`1<UnityEngine.Camera[]> Vuforia.UnityCompiled.RuntimeOpenSourceInitializer_UnityRenderPipeline::BeginFrameRendering
	Action_1_t66F98C2DDE752F9D83CB8FEBE8084E0ABB8C314A * ___BeginFrameRendering_0;
	// System.Action`1<UnityEngine.Camera> Vuforia.UnityCompiled.RuntimeOpenSourceInitializer_UnityRenderPipeline::BeginCameraRendering
	Action_1_t6D83B4F361CDBC0D6B559F8DA3A9646E2ED9561C * ___BeginCameraRendering_1;

public:
	inline static int32_t get_offset_of_BeginFrameRendering_0() { return static_cast<int32_t>(offsetof(UnityRenderPipeline_t62C711062D224A17F999C0F58CDB6F6E641560A4, ___BeginFrameRendering_0)); }
	inline Action_1_t66F98C2DDE752F9D83CB8FEBE8084E0ABB8C314A * get_BeginFrameRendering_0() const { return ___BeginFrameRendering_0; }
	inline Action_1_t66F98C2DDE752F9D83CB8FEBE8084E0ABB8C314A ** get_address_of_BeginFrameRendering_0() { return &___BeginFrameRendering_0; }
	inline void set_BeginFrameRendering_0(Action_1_t66F98C2DDE752F9D83CB8FEBE8084E0ABB8C314A * value)
	{
		___BeginFrameRendering_0 = value;
		Il2CppCodeGenWriteBarrier((&___BeginFrameRendering_0), value);
	}

	inline static int32_t get_offset_of_BeginCameraRendering_1() { return static_cast<int32_t>(offsetof(UnityRenderPipeline_t62C711062D224A17F999C0F58CDB6F6E641560A4, ___BeginCameraRendering_1)); }
	inline Action_1_t6D83B4F361CDBC0D6B559F8DA3A9646E2ED9561C * get_BeginCameraRendering_1() const { return ___BeginCameraRendering_1; }
	inline Action_1_t6D83B4F361CDBC0D6B559F8DA3A9646E2ED9561C ** get_address_of_BeginCameraRendering_1() { return &___BeginCameraRendering_1; }
	inline void set_BeginCameraRendering_1(Action_1_t6D83B4F361CDBC0D6B559F8DA3A9646E2ED9561C * value)
	{
		___BeginCameraRendering_1 = value;
		Il2CppCodeGenWriteBarrier((&___BeginCameraRendering_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYRENDERPIPELINE_T62C711062D224A17F999C0F58CDB6F6E641560A4_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_T02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22_H
#define __STATICARRAYINITTYPESIZEU3D12_T02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12
struct  __StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_T02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22_H
#ifndef __STATICARRAYINITTYPESIZEU3D40_TB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D_H
#define __STATICARRAYINITTYPESIZEU3D40_TB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D40
struct  __StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D__padding[40];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D40_TB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2BDD5686E87BA22996C7749A9C7A7F86EB589380_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2BDD5686E87BA22996C7749A9C7A7F86EB589380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	__StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22  ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D40 <PrivateImplementationDetails>::9E6378168821DBABB7EE3D0154346480FAC8AEF1
	__StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D  ___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1;

public:
	inline static int32_t get_offset_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields, ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline __StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22  get_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline __StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22 * get_address_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(__StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22  value)
	{
		___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}

	inline static int32_t get_offset_of_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields, ___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1)); }
	inline __StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D  get_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1() const { return ___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline __StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D * get_address_of_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return &___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline void set_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1(__StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D  value)
	{
		___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2BDD5686E87BA22996C7749A9C7A7F86EB589380_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef STATUS_T9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092_H
#define STATUS_T9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour_Status
struct  Status_t9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour_Status::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Status_t9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T9B64F0BA3AD7E64C80B7CD10F61ECC24F20EC092_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef BOUNDINGBOXRENDERER_T687A36FDD509AC21A29D0CF70359619EFD251AFB_H
#define BOUNDINGBOXRENDERER_T687A36FDD509AC21A29D0CF70359619EFD251AFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BoundingBoxRenderer
struct  BoundingBoxRenderer_t687A36FDD509AC21A29D0CF70359619EFD251AFB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Material BoundingBoxRenderer::mLineMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___mLineMaterial_4;

public:
	inline static int32_t get_offset_of_mLineMaterial_4() { return static_cast<int32_t>(offsetof(BoundingBoxRenderer_t687A36FDD509AC21A29D0CF70359619EFD251AFB, ___mLineMaterial_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_mLineMaterial_4() const { return ___mLineMaterial_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_mLineMaterial_4() { return &___mLineMaterial_4; }
	inline void set_mLineMaterial_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___mLineMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___mLineMaterial_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOXRENDERER_T687A36FDD509AC21A29D0CF70359619EFD251AFB_H
#ifndef BUTTON1_ONCLICK_TAF7C0C72994FFD1B8026D8E3D01AB56CB86C0834_H
#define BUTTON1_ONCLICK_TAF7C0C72994FFD1B8026D8E3D01AB56CB86C0834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Button1_OnClick
struct  Button1_OnClick_tAF7C0C72994FFD1B8026D8E3D01AB56CB86C0834  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Button Button1_OnClick::btn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___btn_4;
	// UnityEngine.GameObject Button1_OnClick::children_gameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___children_gameObject_5;
	// NoteBookUpperCase Button1_OnClick::cases
	NoteBookUpperCase_t2F0680F176BD4435E95119A593054ED5B6761347 * ___cases_6;

public:
	inline static int32_t get_offset_of_btn_4() { return static_cast<int32_t>(offsetof(Button1_OnClick_tAF7C0C72994FFD1B8026D8E3D01AB56CB86C0834, ___btn_4)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_btn_4() const { return ___btn_4; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_btn_4() { return &___btn_4; }
	inline void set_btn_4(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___btn_4 = value;
		Il2CppCodeGenWriteBarrier((&___btn_4), value);
	}

	inline static int32_t get_offset_of_children_gameObject_5() { return static_cast<int32_t>(offsetof(Button1_OnClick_tAF7C0C72994FFD1B8026D8E3D01AB56CB86C0834, ___children_gameObject_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_children_gameObject_5() const { return ___children_gameObject_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_children_gameObject_5() { return &___children_gameObject_5; }
	inline void set_children_gameObject_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___children_gameObject_5 = value;
		Il2CppCodeGenWriteBarrier((&___children_gameObject_5), value);
	}

	inline static int32_t get_offset_of_cases_6() { return static_cast<int32_t>(offsetof(Button1_OnClick_tAF7C0C72994FFD1B8026D8E3D01AB56CB86C0834, ___cases_6)); }
	inline NoteBookUpperCase_t2F0680F176BD4435E95119A593054ED5B6761347 * get_cases_6() const { return ___cases_6; }
	inline NoteBookUpperCase_t2F0680F176BD4435E95119A593054ED5B6761347 ** get_address_of_cases_6() { return &___cases_6; }
	inline void set_cases_6(NoteBookUpperCase_t2F0680F176BD4435E95119A593054ED5B6761347 * value)
	{
		___cases_6 = value;
		Il2CppCodeGenWriteBarrier((&___cases_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUTTON1_ONCLICK_TAF7C0C72994FFD1B8026D8E3D01AB56CB86C0834_H
#ifndef DEFAULTMODELRECOEVENTHANDLER_TD8D52485390B4887E690E2CE623D20236D7E82CA_H
#define DEFAULTMODELRECOEVENTHANDLER_TD8D52485390B4887E690E2CE623D20236D7E82CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultModelRecoEventHandler
struct  DefaultModelRecoEventHandler_tD8D52485390B4887E690E2CE623D20236D7E82CA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text DefaultModelRecoEventHandler::ModelRecoErrorText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___ModelRecoErrorText_4;
	// System.Boolean DefaultModelRecoEventHandler::StopSearchWhenModelFound
	bool ___StopSearchWhenModelFound_5;
	// System.Boolean DefaultModelRecoEventHandler::StopSearchWhileTracking
	bool ___StopSearchWhileTracking_6;

public:
	inline static int32_t get_offset_of_ModelRecoErrorText_4() { return static_cast<int32_t>(offsetof(DefaultModelRecoEventHandler_tD8D52485390B4887E690E2CE623D20236D7E82CA, ___ModelRecoErrorText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_ModelRecoErrorText_4() const { return ___ModelRecoErrorText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_ModelRecoErrorText_4() { return &___ModelRecoErrorText_4; }
	inline void set_ModelRecoErrorText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___ModelRecoErrorText_4 = value;
		Il2CppCodeGenWriteBarrier((&___ModelRecoErrorText_4), value);
	}

	inline static int32_t get_offset_of_StopSearchWhenModelFound_5() { return static_cast<int32_t>(offsetof(DefaultModelRecoEventHandler_tD8D52485390B4887E690E2CE623D20236D7E82CA, ___StopSearchWhenModelFound_5)); }
	inline bool get_StopSearchWhenModelFound_5() const { return ___StopSearchWhenModelFound_5; }
	inline bool* get_address_of_StopSearchWhenModelFound_5() { return &___StopSearchWhenModelFound_5; }
	inline void set_StopSearchWhenModelFound_5(bool value)
	{
		___StopSearchWhenModelFound_5 = value;
	}

	inline static int32_t get_offset_of_StopSearchWhileTracking_6() { return static_cast<int32_t>(offsetof(DefaultModelRecoEventHandler_tD8D52485390B4887E690E2CE623D20236D7E82CA, ___StopSearchWhileTracking_6)); }
	inline bool get_StopSearchWhileTracking_6() const { return ___StopSearchWhileTracking_6; }
	inline bool* get_address_of_StopSearchWhileTracking_6() { return &___StopSearchWhileTracking_6; }
	inline void set_StopSearchWhileTracking_6(bool value)
	{
		___StopSearchWhileTracking_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTMODELRECOEVENTHANDLER_TD8D52485390B4887E690E2CE623D20236D7E82CA_H
#ifndef DEFAULTTRACKABLEEVENTHANDLER_T6997E0A19AC0FABC165FB7264F57DF2EDF4E8022_H
#define DEFAULTTRACKABLEEVENTHANDLER_T6997E0A19AC0FABC165FB7264F57DF2EDF4E8022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultTrackableEventHandler
struct  DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Vuforia.TrackableBehaviour DefaultTrackableEventHandler::mTrackableBehaviour
	TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * ___mTrackableBehaviour_4;
	// Vuforia.TrackableBehaviour_Status DefaultTrackableEventHandler::m_PreviousStatus
	int32_t ___m_PreviousStatus_5;
	// Vuforia.TrackableBehaviour_Status DefaultTrackableEventHandler::m_NewStatus
	int32_t ___m_NewStatus_6;

public:
	inline static int32_t get_offset_of_mTrackableBehaviour_4() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022, ___mTrackableBehaviour_4)); }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * get_mTrackableBehaviour_4() const { return ___mTrackableBehaviour_4; }
	inline TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 ** get_address_of_mTrackableBehaviour_4() { return &___mTrackableBehaviour_4; }
	inline void set_mTrackableBehaviour_4(TrackableBehaviour_t579D75AAFEF7B2D69F4B68931D5A58074E80A7E4 * value)
	{
		___mTrackableBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableBehaviour_4), value);
	}

	inline static int32_t get_offset_of_m_PreviousStatus_5() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022, ___m_PreviousStatus_5)); }
	inline int32_t get_m_PreviousStatus_5() const { return ___m_PreviousStatus_5; }
	inline int32_t* get_address_of_m_PreviousStatus_5() { return &___m_PreviousStatus_5; }
	inline void set_m_PreviousStatus_5(int32_t value)
	{
		___m_PreviousStatus_5 = value;
	}

	inline static int32_t get_offset_of_m_NewStatus_6() { return static_cast<int32_t>(offsetof(DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022, ___m_NewStatus_6)); }
	inline int32_t get_m_NewStatus_6() const { return ___m_NewStatus_6; }
	inline int32_t* get_address_of_m_NewStatus_6() { return &___m_NewStatus_6; }
	inline void set_m_NewStatus_6(int32_t value)
	{
		___m_NewStatus_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTRACKABLEEVENTHANDLER_T6997E0A19AC0FABC165FB7264F57DF2EDF4E8022_H
#ifndef NOTEBOOK_T4EE5D0D869351884ECD1EBF258E5D2AA49526279_H
#define NOTEBOOK_T4EE5D0D869351884ECD1EBF258E5D2AA49526279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NoteBook
struct  NoteBook_t4EE5D0D869351884ECD1EBF258E5D2AA49526279  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject NoteBook::children_gameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___children_gameObject_4;
	// System.Boolean NoteBook::isOpen
	bool ___isOpen_5;
	// System.Boolean NoteBook::isClose
	bool ___isClose_6;
	// NoteBookUpperCase NoteBook::cases
	NoteBookUpperCase_t2F0680F176BD4435E95119A593054ED5B6761347 * ___cases_7;

public:
	inline static int32_t get_offset_of_children_gameObject_4() { return static_cast<int32_t>(offsetof(NoteBook_t4EE5D0D869351884ECD1EBF258E5D2AA49526279, ___children_gameObject_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_children_gameObject_4() const { return ___children_gameObject_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_children_gameObject_4() { return &___children_gameObject_4; }
	inline void set_children_gameObject_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___children_gameObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___children_gameObject_4), value);
	}

	inline static int32_t get_offset_of_isOpen_5() { return static_cast<int32_t>(offsetof(NoteBook_t4EE5D0D869351884ECD1EBF258E5D2AA49526279, ___isOpen_5)); }
	inline bool get_isOpen_5() const { return ___isOpen_5; }
	inline bool* get_address_of_isOpen_5() { return &___isOpen_5; }
	inline void set_isOpen_5(bool value)
	{
		___isOpen_5 = value;
	}

	inline static int32_t get_offset_of_isClose_6() { return static_cast<int32_t>(offsetof(NoteBook_t4EE5D0D869351884ECD1EBF258E5D2AA49526279, ___isClose_6)); }
	inline bool get_isClose_6() const { return ___isClose_6; }
	inline bool* get_address_of_isClose_6() { return &___isClose_6; }
	inline void set_isClose_6(bool value)
	{
		___isClose_6 = value;
	}

	inline static int32_t get_offset_of_cases_7() { return static_cast<int32_t>(offsetof(NoteBook_t4EE5D0D869351884ECD1EBF258E5D2AA49526279, ___cases_7)); }
	inline NoteBookUpperCase_t2F0680F176BD4435E95119A593054ED5B6761347 * get_cases_7() const { return ___cases_7; }
	inline NoteBookUpperCase_t2F0680F176BD4435E95119A593054ED5B6761347 ** get_address_of_cases_7() { return &___cases_7; }
	inline void set_cases_7(NoteBookUpperCase_t2F0680F176BD4435E95119A593054ED5B6761347 * value)
	{
		___cases_7 = value;
		Il2CppCodeGenWriteBarrier((&___cases_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTEBOOK_T4EE5D0D869351884ECD1EBF258E5D2AA49526279_H
#ifndef NOTEBOOKUPPERCASE_T2F0680F176BD4435E95119A593054ED5B6761347_H
#define NOTEBOOKUPPERCASE_T2F0680F176BD4435E95119A593054ED5B6761347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NoteBookUpperCase
struct  NoteBookUpperCase_t2F0680F176BD4435E95119A593054ED5B6761347  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 NoteBookUpperCase::rotateAngle
	int32_t ___rotateAngle_4;
	// System.Int32 NoteBookUpperCase::rotateSpeed
	int32_t ___rotateSpeed_5;
	// System.Boolean NoteBookUpperCase::isOpen
	bool ___isOpen_6;
	// System.Boolean NoteBookUpperCase::isClose
	bool ___isClose_7;

public:
	inline static int32_t get_offset_of_rotateAngle_4() { return static_cast<int32_t>(offsetof(NoteBookUpperCase_t2F0680F176BD4435E95119A593054ED5B6761347, ___rotateAngle_4)); }
	inline int32_t get_rotateAngle_4() const { return ___rotateAngle_4; }
	inline int32_t* get_address_of_rotateAngle_4() { return &___rotateAngle_4; }
	inline void set_rotateAngle_4(int32_t value)
	{
		___rotateAngle_4 = value;
	}

	inline static int32_t get_offset_of_rotateSpeed_5() { return static_cast<int32_t>(offsetof(NoteBookUpperCase_t2F0680F176BD4435E95119A593054ED5B6761347, ___rotateSpeed_5)); }
	inline int32_t get_rotateSpeed_5() const { return ___rotateSpeed_5; }
	inline int32_t* get_address_of_rotateSpeed_5() { return &___rotateSpeed_5; }
	inline void set_rotateSpeed_5(int32_t value)
	{
		___rotateSpeed_5 = value;
	}

	inline static int32_t get_offset_of_isOpen_6() { return static_cast<int32_t>(offsetof(NoteBookUpperCase_t2F0680F176BD4435E95119A593054ED5B6761347, ___isOpen_6)); }
	inline bool get_isOpen_6() const { return ___isOpen_6; }
	inline bool* get_address_of_isOpen_6() { return &___isOpen_6; }
	inline void set_isOpen_6(bool value)
	{
		___isOpen_6 = value;
	}

	inline static int32_t get_offset_of_isClose_7() { return static_cast<int32_t>(offsetof(NoteBookUpperCase_t2F0680F176BD4435E95119A593054ED5B6761347, ___isClose_7)); }
	inline bool get_isClose_7() const { return ___isClose_7; }
	inline bool* get_address_of_isClose_7() { return &___isClose_7; }
	inline void set_isClose_7(bool value)
	{
		___isClose_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTEBOOKUPPERCASE_T2F0680F176BD4435E95119A593054ED5B6761347_H
#ifndef OPENBUTTON_ONCLICK_TF5AFB18C0F18BB186BD44AAC7F5F6C034CEB2459_H
#define OPENBUTTON_ONCLICK_TF5AFB18C0F18BB186BD44AAC7F5F6C034CEB2459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OpenButton_OnClick
struct  OpenButton_OnClick_tF5AFB18C0F18BB186BD44AAC7F5F6C034CEB2459  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Button OpenButton_OnClick::closeBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___closeBtn_4;
	// UnityEngine.UI.Button OpenButton_OnClick::openBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___openBtn_5;
	// UnityEngine.GameObject OpenButton_OnClick::children_gameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___children_gameObject_6;
	// NoteBookUpperCase OpenButton_OnClick::cases
	NoteBookUpperCase_t2F0680F176BD4435E95119A593054ED5B6761347 * ___cases_7;

public:
	inline static int32_t get_offset_of_closeBtn_4() { return static_cast<int32_t>(offsetof(OpenButton_OnClick_tF5AFB18C0F18BB186BD44AAC7F5F6C034CEB2459, ___closeBtn_4)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_closeBtn_4() const { return ___closeBtn_4; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_closeBtn_4() { return &___closeBtn_4; }
	inline void set_closeBtn_4(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___closeBtn_4 = value;
		Il2CppCodeGenWriteBarrier((&___closeBtn_4), value);
	}

	inline static int32_t get_offset_of_openBtn_5() { return static_cast<int32_t>(offsetof(OpenButton_OnClick_tF5AFB18C0F18BB186BD44AAC7F5F6C034CEB2459, ___openBtn_5)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_openBtn_5() const { return ___openBtn_5; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_openBtn_5() { return &___openBtn_5; }
	inline void set_openBtn_5(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___openBtn_5 = value;
		Il2CppCodeGenWriteBarrier((&___openBtn_5), value);
	}

	inline static int32_t get_offset_of_children_gameObject_6() { return static_cast<int32_t>(offsetof(OpenButton_OnClick_tF5AFB18C0F18BB186BD44AAC7F5F6C034CEB2459, ___children_gameObject_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_children_gameObject_6() const { return ___children_gameObject_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_children_gameObject_6() { return &___children_gameObject_6; }
	inline void set_children_gameObject_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___children_gameObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___children_gameObject_6), value);
	}

	inline static int32_t get_offset_of_cases_7() { return static_cast<int32_t>(offsetof(OpenButton_OnClick_tF5AFB18C0F18BB186BD44AAC7F5F6C034CEB2459, ___cases_7)); }
	inline NoteBookUpperCase_t2F0680F176BD4435E95119A593054ED5B6761347 * get_cases_7() const { return ___cases_7; }
	inline NoteBookUpperCase_t2F0680F176BD4435E95119A593054ED5B6761347 ** get_address_of_cases_7() { return &___cases_7; }
	inline void set_cases_7(NoteBookUpperCase_t2F0680F176BD4435E95119A593054ED5B6761347 * value)
	{
		___cases_7 = value;
		Il2CppCodeGenWriteBarrier((&___cases_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPENBUTTON_ONCLICK_TF5AFB18C0F18BB186BD44AAC7F5F6C034CEB2459_H
#ifndef VUFORIAMONOBEHAVIOUR_T806C61E721B78928AF6266F3AF838FA2CB56AB5D_H
#define VUFORIAMONOBEHAVIOUR_T806C61E721B78928AF6266F3AF838FA2CB56AB5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VuforiaMonoBehaviour
struct  VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAMONOBEHAVIOUR_T806C61E721B78928AF6266F3AF838FA2CB56AB5D_H
#ifndef DEFAULTINITIALIZATIONERRORHANDLER_T7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38_H
#define DEFAULTINITIALIZATIONERRORHANDLER_T7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DefaultInitializationErrorHandler
struct  DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38  : public VuforiaMonoBehaviour_t806C61E721B78928AF6266F3AF838FA2CB56AB5D
{
public:
	// System.String DefaultInitializationErrorHandler::mErrorText
	String_t* ___mErrorText_4;
	// System.Boolean DefaultInitializationErrorHandler::mErrorOccurred
	bool ___mErrorOccurred_5;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::bodyStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___bodyStyle_7;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::headerStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___headerStyle_8;
	// UnityEngine.GUIStyle DefaultInitializationErrorHandler::footerStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___footerStyle_9;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::bodyTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___bodyTexture_10;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::headerTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___headerTexture_11;
	// UnityEngine.Texture2D DefaultInitializationErrorHandler::footerTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___footerTexture_12;

public:
	inline static int32_t get_offset_of_mErrorText_4() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___mErrorText_4)); }
	inline String_t* get_mErrorText_4() const { return ___mErrorText_4; }
	inline String_t** get_address_of_mErrorText_4() { return &___mErrorText_4; }
	inline void set_mErrorText_4(String_t* value)
	{
		___mErrorText_4 = value;
		Il2CppCodeGenWriteBarrier((&___mErrorText_4), value);
	}

	inline static int32_t get_offset_of_mErrorOccurred_5() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___mErrorOccurred_5)); }
	inline bool get_mErrorOccurred_5() const { return ___mErrorOccurred_5; }
	inline bool* get_address_of_mErrorOccurred_5() { return &___mErrorOccurred_5; }
	inline void set_mErrorOccurred_5(bool value)
	{
		___mErrorOccurred_5 = value;
	}

	inline static int32_t get_offset_of_bodyStyle_7() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___bodyStyle_7)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_bodyStyle_7() const { return ___bodyStyle_7; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_bodyStyle_7() { return &___bodyStyle_7; }
	inline void set_bodyStyle_7(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___bodyStyle_7 = value;
		Il2CppCodeGenWriteBarrier((&___bodyStyle_7), value);
	}

	inline static int32_t get_offset_of_headerStyle_8() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___headerStyle_8)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_headerStyle_8() const { return ___headerStyle_8; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_headerStyle_8() { return &___headerStyle_8; }
	inline void set_headerStyle_8(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___headerStyle_8 = value;
		Il2CppCodeGenWriteBarrier((&___headerStyle_8), value);
	}

	inline static int32_t get_offset_of_footerStyle_9() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___footerStyle_9)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_footerStyle_9() const { return ___footerStyle_9; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_footerStyle_9() { return &___footerStyle_9; }
	inline void set_footerStyle_9(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___footerStyle_9 = value;
		Il2CppCodeGenWriteBarrier((&___footerStyle_9), value);
	}

	inline static int32_t get_offset_of_bodyTexture_10() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___bodyTexture_10)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_bodyTexture_10() const { return ___bodyTexture_10; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_bodyTexture_10() { return &___bodyTexture_10; }
	inline void set_bodyTexture_10(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___bodyTexture_10 = value;
		Il2CppCodeGenWriteBarrier((&___bodyTexture_10), value);
	}

	inline static int32_t get_offset_of_headerTexture_11() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___headerTexture_11)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_headerTexture_11() const { return ___headerTexture_11; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_headerTexture_11() { return &___headerTexture_11; }
	inline void set_headerTexture_11(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___headerTexture_11 = value;
		Il2CppCodeGenWriteBarrier((&___headerTexture_11), value);
	}

	inline static int32_t get_offset_of_footerTexture_12() { return static_cast<int32_t>(offsetof(DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38, ___footerTexture_12)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_footerTexture_12() const { return ___footerTexture_12; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_footerTexture_12() { return &___footerTexture_12; }
	inline void set_footerTexture_12(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___footerTexture_12 = value;
		Il2CppCodeGenWriteBarrier((&___footerTexture_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTINITIALIZATIONERRORHANDLER_T7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2900 = { sizeof (U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380), -1, sizeof(U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2900[2] = 
{
	U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields::get_offset_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
	U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields::get_offset_of_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2901 = { sizeof (__StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2902 = { sizeof (__StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2903 = { sizeof (U3CModuleU3E_tBB5D6FDE6CBE0067E5DC774822D5054F01745F8C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2904 = { sizeof (BoundingBoxRenderer_t687A36FDD509AC21A29D0CF70359619EFD251AFB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2904[1] = 
{
	BoundingBoxRenderer_t687A36FDD509AC21A29D0CF70359619EFD251AFB::get_offset_of_mLineMaterial_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2905 = { sizeof (DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2905[9] = 
{
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_mErrorText_4(),
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_mErrorOccurred_5(),
	0,
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_bodyStyle_7(),
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_headerStyle_8(),
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_footerStyle_9(),
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_bodyTexture_10(),
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_headerTexture_11(),
	DefaultInitializationErrorHandler_t7C0EE05C3D7BF5C7A141BC322D2F0B950B101D38::get_offset_of_footerTexture_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2906 = { sizeof (DefaultModelRecoEventHandler_tD8D52485390B4887E690E2CE623D20236D7E82CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2906[3] = 
{
	DefaultModelRecoEventHandler_tD8D52485390B4887E690E2CE623D20236D7E82CA::get_offset_of_ModelRecoErrorText_4(),
	DefaultModelRecoEventHandler_tD8D52485390B4887E690E2CE623D20236D7E82CA::get_offset_of_StopSearchWhenModelFound_5(),
	DefaultModelRecoEventHandler_tD8D52485390B4887E690E2CE623D20236D7E82CA::get_offset_of_StopSearchWhileTracking_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2907 = { sizeof (DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2907[3] = 
{
	DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022::get_offset_of_mTrackableBehaviour_4(),
	DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022::get_offset_of_m_PreviousStatus_5(),
	DefaultTrackableEventHandler_t6997E0A19AC0FABC165FB7264F57DF2EDF4E8022::get_offset_of_m_NewStatus_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2908 = { sizeof (RuntimeOpenSourceInitializer_t81785D24983BCE4C95906F20541AD7A3D3D11862), -1, sizeof(RuntimeOpenSourceInitializer_t81785D24983BCE4C95906F20541AD7A3D3D11862_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2908[1] = 
{
	RuntimeOpenSourceInitializer_t81785D24983BCE4C95906F20541AD7A3D3D11862_StaticFields::get_offset_of_sFacade_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2909 = { sizeof (OpenSourceUnityCompiledFacade_t8315EDA1848F9A4AA27FC5AEAA48F4097D1FF2A5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2909[2] = 
{
	OpenSourceUnityCompiledFacade_t8315EDA1848F9A4AA27FC5AEAA48F4097D1FF2A5::get_offset_of_mUnityRenderPipeline_0(),
	OpenSourceUnityCompiledFacade_t8315EDA1848F9A4AA27FC5AEAA48F4097D1FF2A5::get_offset_of_mUnityAndroidPermissions_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2910 = { sizeof (UnityRenderPipeline_t62C711062D224A17F999C0F58CDB6F6E641560A4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2910[2] = 
{
	UnityRenderPipeline_t62C711062D224A17F999C0F58CDB6F6E641560A4::get_offset_of_BeginFrameRendering_0(),
	UnityRenderPipeline_t62C711062D224A17F999C0F58CDB6F6E641560A4::get_offset_of_BeginCameraRendering_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2911 = { sizeof (UnityAndroidPermissions_tA4CE60356A3424ECF5E1AB1F39264182BB49D7CF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2912 = { sizeof (U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2913 = { sizeof (Button1_OnClick_tAF7C0C72994FFD1B8026D8E3D01AB56CB86C0834), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2913[3] = 
{
	Button1_OnClick_tAF7C0C72994FFD1B8026D8E3D01AB56CB86C0834::get_offset_of_btn_4(),
	Button1_OnClick_tAF7C0C72994FFD1B8026D8E3D01AB56CB86C0834::get_offset_of_children_gameObject_5(),
	Button1_OnClick_tAF7C0C72994FFD1B8026D8E3D01AB56CB86C0834::get_offset_of_cases_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2914 = { sizeof (NoteBook_t4EE5D0D869351884ECD1EBF258E5D2AA49526279), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2914[4] = 
{
	NoteBook_t4EE5D0D869351884ECD1EBF258E5D2AA49526279::get_offset_of_children_gameObject_4(),
	NoteBook_t4EE5D0D869351884ECD1EBF258E5D2AA49526279::get_offset_of_isOpen_5(),
	NoteBook_t4EE5D0D869351884ECD1EBF258E5D2AA49526279::get_offset_of_isClose_6(),
	NoteBook_t4EE5D0D869351884ECD1EBF258E5D2AA49526279::get_offset_of_cases_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2915 = { sizeof (NoteBookUpperCase_t2F0680F176BD4435E95119A593054ED5B6761347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2915[4] = 
{
	NoteBookUpperCase_t2F0680F176BD4435E95119A593054ED5B6761347::get_offset_of_rotateAngle_4(),
	NoteBookUpperCase_t2F0680F176BD4435E95119A593054ED5B6761347::get_offset_of_rotateSpeed_5(),
	NoteBookUpperCase_t2F0680F176BD4435E95119A593054ED5B6761347::get_offset_of_isOpen_6(),
	NoteBookUpperCase_t2F0680F176BD4435E95119A593054ED5B6761347::get_offset_of_isClose_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2916 = { sizeof (OpenButton_OnClick_tF5AFB18C0F18BB186BD44AAC7F5F6C034CEB2459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2916[4] = 
{
	OpenButton_OnClick_tF5AFB18C0F18BB186BD44AAC7F5F6C034CEB2459::get_offset_of_closeBtn_4(),
	OpenButton_OnClick_tF5AFB18C0F18BB186BD44AAC7F5F6C034CEB2459::get_offset_of_openBtn_5(),
	OpenButton_OnClick_tF5AFB18C0F18BB186BD44AAC7F5F6C034CEB2459::get_offset_of_children_gameObject_6(),
	OpenButton_OnClick_tF5AFB18C0F18BB186BD44AAC7F5F6C034CEB2459::get_offset_of_cases_7(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
