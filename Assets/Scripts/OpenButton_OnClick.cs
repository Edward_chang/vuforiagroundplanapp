﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenButton_OnClick : MonoBehaviour
{
    public Button closeBtn;
    public Button openBtn;
    GameObject children_gameObject;
    public NoteBookUpperCase cases = null;
    // Start is called before the first frame update
    void Start()
    {
        Button mbutton1 = openBtn.GetComponent<Button>();
        mbutton1.onClick.AddListener(openCase);

        Button mbutton2 = closeBtn.GetComponent<Button>();
        mbutton2.onClick.AddListener(closeCase);
    }

    public void openCase()
    {
        children_gameObject = GameObject.Find("Bone.001");
        if (children_gameObject != null)
        {
            cases = children_gameObject.GetComponent<NoteBookUpperCase>();
            //Debug.Log(">>Button1_OnClick####" + cases.isOpen);
            cases.isClose = false;
            cases.isOpen = true;
            // Debug.Log(">>Button1_OnClick ####" + cases.isClose);
        }
    }

    public void closeCase()
    {
        children_gameObject = GameObject.Find("Bone.001");
        if (children_gameObject != null)
        {
            cases = children_gameObject.GetComponent<NoteBookUpperCase>();
            //Debug.Log(">>Button1_OnClick####" + cases.isOpen);
            cases.isClose = true;
            cases.isOpen = false;
            // Debug.Log(">>Button1_OnClick ####" + cases.isClose);
        }
    }
}
