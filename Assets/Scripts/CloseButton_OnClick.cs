﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Button1_OnClick : MonoBehaviour

{
    public Button btn;
    GameObject children_gameObject;
    public NoteBookUpperCase cases = null;
    // Start is called before the first frame update
    void Start()
    {
        Button mbutton = btn.GetComponent<Button>();
        mbutton.onClick.AddListener(CloseCase);
    }

  
    public void CloseCase()
    {
        children_gameObject = GameObject.Find("Bone.001");
        if (children_gameObject != null)
        {
            cases = children_gameObject.GetComponent<NoteBookUpperCase>();
            //Debug.Log(">>Button1_OnClick####" + cases.isOpen);
            cases.isClose = true;
           // Debug.Log(">>Button1_OnClick ####" + cases.isClose);
        }
    }
}
