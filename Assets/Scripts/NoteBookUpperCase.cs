﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteBookUpperCase : MonoBehaviour
{

    public int rotateAngle = 90;                    //旋转角度
    public int rotateSpeed = 2;                 //旋转速度
    public bool isOpen = false;                    //打开排风门
    public bool isClose = false;                   //关闭进风门

    // Update is called once per frame
    void Update()
    {
        Debug.Log("QQUpdateQQQQ"+ isClose);
        if (isOpen)
        {
            RotateXOpen();

        }
        if (isClose)
        {
            RotateXClose();

        }
    }

    public void OpenEnterWindDoor()
    {
        isOpen = true;
        Debug.Log("1111");
    }

    public void CloseEnterWindDoor()
    {
        isClose = true;
        Debug.Log("222");
    }


    public void RotateXOpen()
    {
      
        Quaternion target = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, 0);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, target, rotateSpeed);
        Debug.Log("###RotateXOpen####" + transform.rotation.eulerAngles.z);

        if (transform.rotation.eulerAngles.z <= 0)
        {
                isOpen = !isOpen;
                Debug.Log(isOpen);
            }
    }

    public void RotateXClose()
    {
        Quaternion target = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, -90);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, target, rotateSpeed);
        Debug.Log("##RotateXClose#####" + transform.rotation.eulerAngles.z);
        if (transform.rotation.eulerAngles.z <= -90)
        {
            isClose = !isClose;
            Debug.Log(isClose);
        }

    }
}
